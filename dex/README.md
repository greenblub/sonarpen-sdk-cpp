## Instructions

classes.dex is pre-complied by a below small java class to listen some events from android framework which can not been implemented in jni c++

`
package com.greenbulb.sonarpen;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.session.MediaSession;
import android.os.Build;
import android.os.FileObserver;
import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class SonarPenListener extends BroadcastReceiver {
    @Override
    public native void onReceive(Context context, Intent intent);


    public static void showToast(Context context, String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public class SonarPenFileObserver extends FileObserver {
        public SonarPenFileObserver(String path) {
            super(path);
        }

        @Override
        public native void onEvent(int event, String path);
    }

    private class ProfileSyncReceiver extends BroadcastReceiver {
        @Override
        public native void onReceive(Context context, Intent intent);
    }

    public class SPOnKeyListener implements View.OnKeyListener {
        @Override
        public native boolean onKey(View v, int keyCode, KeyEvent event);
    }

    public class SPOnTouchListener implements View.OnTouchListener {
        @Override
        public native boolean onTouch(View v, MotionEvent event);
    }

    @SuppressLint("NewApi")
    public class SPMediaSessionCallback extends MediaSession.Callback {
        @Override
        public boolean onMediaButtonEvent(Intent mediaButtonIntent) {
            return true;
        }
    }
}

`