/*
                    GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

                            Preamble

  The GNU General Public License is a free, copyleft license for
software and other kinds of works.

  The licenses for most software and other practical works are designed
to take away your freedom to share and change the works.  By contrast,
the GNU General Public License is intended to guarantee your freedom to
share and change all versions of a program--to make sure it remains free
software for all its users.  We, the Free Software Foundation, use the
GNU General Public License for most of our software; it applies also to
any other work released this way by its authors.  You can apply it to
your programs, too.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
them if you wish), that you receive source code or can get it if you
want it, that you can change the software or use pieces of it in new
free programs, and that you know you can do these things.

  To protect your rights, we need to prevent others from denying you
these rights or asking you to surrender the rights.  Therefore, you have
certain responsibilities if you distribute copies of the software, or if
you modify it: responsibilities to respect the freedom of others.

  For example, if you distribute copies of such a program, whether
gratis or for a fee, you must pass on to the recipients the same
freedoms that you received.  You must make sure that they, too, receive
or can get the source code.  And you must show them these terms so they
know their rights.

  Developers that use the GNU GPL protect your rights with two steps:
(1) assert copyright on the software, and (2) offer you this License
giving you legal permission to copy, distribute and/or modify it.

  For the developers' and authors' protection, the GPL clearly explains
that there is no warranty for this free software.  For both users' and
authors' sake, the GPL requires that modified versions be marked as
changed, so that their problems will not be attributed erroneously to
authors of previous versions.

  Some devices are designed to deny users access to install or run
modified versions of the software inside them, although the manufacturer
can do so.  This is fundamentally incompatible with the aim of
protecting users' freedom to change the software.  The systematic
pattern of such abuse occurs in the area of products for individuals to
use, which is precisely where it is most unacceptable.  Therefore, we
have designed this version of the GPL to prohibit the practice for those
products.  If such problems arise substantially in other domains, we
stand ready to extend this provision to those domains in future versions
of the GPL, as needed to protect the freedom of users.

  Finally, every program is threatened constantly by software patents.
States should not allow patents to restrict development and use of
software on general-purpose computers, but in those that do, we wish to
avoid the special danger that patents applied to a free program could
make it effectively proprietary.  To prevent this, the GPL assures that
patents cannot be used to render the program non-free.

  The precise terms and conditions for copying, distribution and
modification follow.

                       TERMS AND CONDITIONS

  0. Definitions.

  "This License" refers to version 3 of the GNU General Public License.

  "Copyright" also means copyright-like laws that apply to other kinds of
works, such as semiconductor masks.

  "The Program" refers to any copyrightable work licensed under this
License.  Each licensee is addressed as "you".  "Licensees" and
"recipients" may be individuals or organizations.

  To "modify" a work means to copy from or adapt all or part of the work
in a fashion requiring copyright permission, other than the making of an
exact copy.  The resulting work is called a "modified version" of the
earlier work or a work "based on" the earlier work.

  A "covered work" means either the unmodified Program or a work based
on the Program.

  To "propagate" a work means to do anything with it that, without
permission, would make you directly or secondarily liable for
infringement under applicable copyright law, except executing it on a
computer or modifying a private copy.  Propagation includes copying,
distribution (with or without modification), making available to the
public, and in some countries other activities as well.

  To "convey" a work means any kind of propagation that enables other
parties to make or receive copies.  Mere interaction with a user through
a computer network, with no transfer of a copy, is not conveying.

  An interactive user interface displays "Appropriate Legal Notices"
to the extent that it includes a convenient and prominently visible
feature that (1) displays an appropriate copyright notice, and (2)
tells the user that there is no warranty for the work (except to the
extent that warranties are provided), that licensees may convey the
work under this License, and how to view a copy of this License.  If
the interface presents a list of user commands or options, such as a
menu, a prominent item in the list meets this criterion.

  1. Source Code.

  The "source code" for a work means the preferred form of the work
for making modifications to it.  "Object code" means any non-source
form of a work.

  A "Standard Interface" means an interface that either is an official
standard defined by a recognized standards body, or, in the case of
interfaces specified for a particular programming language, one that
is widely used among developers working in that language.

  The "System Libraries" of an executable work include anything, other
than the work as a whole, that (a) is included in the normal form of
packaging a Major Component, but which is not part of that Major
Component, and (b) serves only to enable use of the work with that
Major Component, or to implement a Standard Interface for which an
implementation is available to the public in source code form.  A
"Major Component", in this context, means a major essential component
(kernel, window system, and so on) of the specific operating system
(if any) on which the executable work runs, or a compiler used to
produce the work, or an object code interpreter used to run it.

  The "Corresponding Source" for a work in object code form means all
the source code needed to generate, install, and (for an executable
work) run the object code and to modify the work, including scripts to
control those activities.  However, it does not include the work's
System Libraries, or general-purpose tools or generally available free
programs which are used unmodified in performing those activities but
which are not part of the work.  For example, Corresponding Source
includes interface definition files associated with source files for
the work, and the source code for shared libraries and dynamically
linked subprograms that the work is specifically designed to require,
such as by intimate data communication or control flow between those
subprograms and other parts of the work.

  The Corresponding Source need not include anything that users
can regenerate automatically from other parts of the Corresponding
Source.

  The Corresponding Source for a work in source code form is that
same work.

  2. Basic Permissions.

  All rights granted under this License are granted for the term of
copyright on the Program, and are irrevocable provided the stated
conditions are met.  This License explicitly affirms your unlimited
permission to run the unmodified Program.  The output from running a
covered work is covered by this License only if the output, given its
content, constitutes a covered work.  This License acknowledges your
rights of fair use or other equivalent, as provided by copyright law.

  You may make, run and propagate covered works that you do not
convey, without conditions so long as your license otherwise remains
in force.  You may convey covered works to others for the sole purpose
of having them make modifications exclusively for you, or provide you
with facilities for running those works, provided that you comply with
the terms of this License in conveying all material for which you do
not control copyright.  Those thus making or running the covered works
for you must do so exclusively on your behalf, under your direction
and control, on terms that prohibit them from making any copies of
your copyrighted material outside their relationship with you.

  Conveying under any other circumstances is permitted solely under
the conditions stated below.  Sublicensing is not allowed; section 10
makes it unnecessary.

  3. Protecting Users' Legal Rights From Anti-Circumvention Law.

  No covered work shall be deemed part of an effective technological
measure under any applicable law fulfilling obligations under article
11 of the WIPO copyright treaty adopted on 20 December 1996, or
similar laws prohibiting or restricting circumvention of such
measures.

  When you convey a covered work, you waive any legal power to forbid
circumvention of technological measures to the extent such circumvention
is effected by exercising rights under this License with respect to
the covered work, and you disclaim any intention to limit operation or
modification of the work as a means of enforcing, against the work's
users, your or third parties' legal rights to forbid circumvention of
technological measures.

  4. Conveying Verbatim Copies.

  You may convey verbatim copies of the Program's source code as you
receive it, in any medium, provided that you conspicuously and
appropriately publish on each copy an appropriate copyright notice;
keep intact all notices stating that this License and any
non-permissive terms added in accord with section 7 apply to the code;
keep intact all notices of the absence of any warranty; and give all
recipients a copy of this License along with the Program.

  You may charge any price or no price for each copy that you convey,
and you may offer support or warranty protection for a fee.

  5. Conveying Modified Source Versions.

  You may convey a work based on the Program, or the modifications to
produce it from the Program, in the form of source code under the
terms of section 4, provided that you also meet all of these conditions:

    a) The work must carry prominent notices stating that you modified
    it, and giving a relevant date.

    b) The work must carry prominent notices stating that it is
    released under this License and any conditions added under section
    7.  This requirement modifies the requirement in section 4 to
    "keep intact all notices".

    c) You must license the entire work, as a whole, under this
    License to anyone who comes into possession of a copy.  This
    License will therefore apply, along with any applicable section 7
    additional terms, to the whole of the work, and all its parts,
    regardless of how they are packaged.  This License gives no
    permission to license the work in any other way, but it does not
    invalidate such permission if you have separately received it.

    d) If the work has interactive user interfaces, each must display
    Appropriate Legal Notices; however, if the Program has interactive
    interfaces that do not display Appropriate Legal Notices, your
    work need not make them do so.

  A compilation of a covered work with other separate and independent
works, which are not by their nature extensions of the covered work,
and which are not combined with it such as to form a larger program,
in or on a volume of a storage or distribution medium, is called an
"aggregate" if the compilation and its resulting copyright are not
used to limit the access or legal rights of the compilation's users
beyond what the individual works permit.  Inclusion of a covered work
in an aggregate does not cause this License to apply to the other
parts of the aggregate.

  6. Conveying Non-Source Forms.

  You may convey a covered work in object code form under the terms
of sections 4 and 5, provided that you also convey the
machine-readable Corresponding Source under the terms of this License,
in one of these ways:

    a) Convey the object code in, or embodied in, a physical product
    (including a physical distribution medium), accompanied by the
    Corresponding Source fixed on a durable physical medium
    customarily used for software interchange.

    b) Convey the object code in, or embodied in, a physical product
    (including a physical distribution medium), accompanied by a
    written offer, valid for at least three years and valid for as
    long as you offer spare parts or customer support for that product
    model, to give anyone who possesses the object code either (1) a
    copy of the Corresponding Source for all the software in the
    product that is covered by this License, on a durable physical
    medium customarily used for software interchange, for a price no
    more than your reasonable cost of physically performing this
    conveying of source, or (2) access to copy the
    Corresponding Source from a network server at no charge.

    c) Convey individual copies of the object code with a copy of the
    written offer to provide the Corresponding Source.  This
    alternative is allowed only occasionally and noncommercially, and
    only if you received the object code with such an offer, in accord
    with subsection 6b.

    d) Convey the object code by offering access from a designated
    place (gratis or for a charge), and offer equivalent access to the
    Corresponding Source in the same way through the same place at no
    further charge.  You need not require recipients to copy the
    Corresponding Source along with the object code.  If the place to
    copy the object code is a network server, the Corresponding Source
    may be on a different server (operated by you or a third party)
    that supports equivalent copying facilities, provided you maintain
    clear directions next to the object code saying where to find the
    Corresponding Source.  Regardless of what server hosts the
    Corresponding Source, you remain obligated to ensure that it is
    available for as long as needed to satisfy these requirements.

    e) Convey the object code using peer-to-peer transmission, provided
    you inform other peers where the object code and Corresponding
    Source of the work are being offered to the general public at no
    charge under subsection 6d.

  A separable portion of the object code, whose source code is excluded
from the Corresponding Source as a System Library, need not be
included in conveying the object code work.

  A "User Product" is either (1) a "consumer product", which means any
tangible personal property which is normally used for personal, family,
or household purposes, or (2) anything designed or sold for incorporation
into a dwelling.  In determining whether a product is a consumer product,
doubtful cases shall be resolved in favor of coverage.  For a particular
product received by a particular user, "normally used" refers to a
typical or common use of that class of product, regardless of the status
of the particular user or of the way in which the particular user
actually uses, or expects or is expected to use, the product.  A product
is a consumer product regardless of whether the product has substantial
commercial, industrial or non-consumer uses, unless such uses represent
the only significant mode of use of the product.

  "Installation Information" for a User Product means any methods,
procedures, authorization keys, or other information required to install
and execute modified versions of a covered work in that User Product from
a modified version of its Corresponding Source.  The information must
suffice to ensure that the continued functioning of the modified object
code is in no case prevented or interfered with solely because
modification has been made.

  If you convey an object code work under this section in, or with, or
specifically for use in, a User Product, and the conveying occurs as
part of a transaction in which the right of possession and use of the
User Product is transferred to the recipient in perpetuity or for a
fixed term (regardless of how the transaction is characterized), the
Corresponding Source conveyed under this section must be accompanied
by the Installation Information.  But this requirement does not apply
if neither you nor any third party retains the ability to install
modified object code on the User Product (for example, the work has
been installed in ROM).

  The requirement to provide Installation Information does not include a
requirement to continue to provide support service, warranty, or updates
for a work that has been modified or installed by the recipient, or for
the User Product in which it has been modified or installed.  Access to a
network may be denied when the modification itself materially and
adversely affects the operation of the network or violates the rules and
protocols for communication across the network.

  Corresponding Source conveyed, and Installation Information provided,
in accord with this section must be in a format that is publicly
documented (and with an implementation available to the public in
source code form), and must require no special password or key for
unpacking, reading or copying.

  7. Additional Terms.

  "Additional permissions" are terms that supplement the terms of this
License by making exceptions from one or more of its conditions.
Additional permissions that are applicable to the entire Program shall
be treated as though they were included in this License, to the extent
that they are valid under applicable law.  If additional permissions
apply only to part of the Program, that part may be used separately
under those permissions, but the entire Program remains governed by
this License without regard to the additional permissions.

  When you convey a copy of a covered work, you may at your option
remove any additional permissions from that copy, or from any part of
it.  (Additional permissions may be written to require their own
removal in certain cases when you modify the work.)  You may place
additional permissions on material, added by you to a covered work,
for which you have or can give appropriate copyright permission.

  Notwithstanding any other provision of this License, for material you
add to a covered work, you may (if authorized by the copyright holders of
that material) supplement the terms of this License with terms:

    a) Disclaiming warranty or limiting liability differently from the
    terms of sections 15 and 16 of this License; or

    b) Requiring preservation of specified reasonable legal notices or
    author attributions in that material or in the Appropriate Legal
    Notices displayed by works containing it; or

    c) Prohibiting misrepresentation of the origin of that material, or
    requiring that modified versions of such material be marked in
    reasonable ways as different from the original version; or

    d) Limiting the use for publicity purposes of names of licensors or
    authors of the material; or

    e) Declining to grant rights under trademark law for use of some
    trade names, trademarks, or service marks; or

    f) Requiring indemnification of licensors and authors of that
    material by anyone who conveys the material (or modified versions of
    it) with contractual assumptions of liability to the recipient, for
    any liability that these contractual assumptions directly impose on
    those licensors and authors.

  All other non-permissive additional terms are considered "further
restrictions" within the meaning of section 10.  If the Program as you
received it, or any part of it, contains a notice stating that it is
governed by this License along with a term that is a further
restriction, you may remove that term.  If a license document contains
a further restriction but permits relicensing or conveying under this
License, you may add to a covered work material governed by the terms
of that license document, provided that the further restriction does
not survive such relicensing or conveying.

  If you add terms to a covered work in accord with this section, you
must place, in the relevant source files, a statement of the
additional terms that apply to those files, or a notice indicating
where to find the applicable terms.

  Additional terms, permissive or non-permissive, may be stated in the
form of a separately written license, or stated as exceptions;
the above requirements apply either way.

  8. Termination.

  You may not propagate or modify a covered work except as expressly
provided under this License.  Any attempt otherwise to propagate or
modify it is void, and will automatically terminate your rights under
this License (including any patent licenses granted under the third
paragraph of section 11).

  However, if you cease all violation of this License, then your
license from a particular copyright holder is reinstated (a)
provisionally, unless and until the copyright holder explicitly and
finally terminates your license, and (b) permanently, if the copyright
holder fails to notify you of the violation by some reasonable means
prior to 60 days after the cessation.

  Moreover, your license from a particular copyright holder is
reinstated permanently if the copyright holder notifies you of the
violation by some reasonable means, this is the first time you have
received notice of violation of this License (for any work) from that
copyright holder, and you cure the violation prior to 30 days after
your receipt of the notice.

  Termination of your rights under this section does not terminate the
licenses of parties who have received copies or rights from you under
this License.  If your rights have been terminated and not permanently
reinstated, you do not qualify to receive new licenses for the same
material under section 10.

  9. Acceptance Not Required for Having Copies.

  You are not required to accept this License in order to receive or
run a copy of the Program.  Ancillary propagation of a covered work
occurring solely as a consequence of using peer-to-peer transmission
to receive a copy likewise does not require acceptance.  However,
nothing other than this License grants you permission to propagate or
modify any covered work.  These actions infringe copyright if you do
not accept this License.  Therefore, by modifying or propagating a
covered work, you indicate your acceptance of this License to do so.

  10. Automatic Licensing of Downstream Recipients.

  Each time you convey a covered work, the recipient automatically
receives a license from the original licensors, to run, modify and
propagate that work, subject to this License.  You are not responsible
for enforcing compliance by third parties with this License.

  An "entity transaction" is a transaction transferring control of an
organization, or substantially all assets of one, or subdividing an
organization, or merging organizations.  If propagation of a covered
work results from an entity transaction, each party to that
transaction who receives a copy of the work also receives whatever
licenses to the work the party's predecessor in interest had or could
give under the previous paragraph, plus a right to possession of the
Corresponding Source of the work from the predecessor in interest, if
the predecessor has it or can get it with reasonable efforts.

  You may not impose any further restrictions on the exercise of the
rights granted or affirmed under this License.  For example, you may
not impose a license fee, royalty, or other charge for exercise of
rights granted under this License, and you may not initiate litigation
(including a cross-claim or counterclaim in a lawsuit) alleging that
any patent claim is infringed by making, using, selling, offering for
sale, or importing the Program or any portion of it.

  11. Patents.

  A "contributor" is a copyright holder who authorizes use under this
License of the Program or a work on which the Program is based.  The
work thus licensed is called the contributor's "contributor version".

  A contributor's "essential patent claims" are all patent claims
owned or controlled by the contributor, whether already acquired or
hereafter acquired, that would be infringed by some manner, permitted
by this License, of making, using, or selling its contributor version,
but do not include claims that would be infringed only as a
consequence of further modification of the contributor version.  For
purposes of this definition, "control" includes the right to grant
patent sublicenses in a manner consistent with the requirements of
this License.

  Each contributor grants you a non-exclusive, worldwide, royalty-free
patent license under the contributor's essential patent claims, to
make, use, sell, offer for sale, import and otherwise run, modify and
propagate the contents of its contributor version.

  In the following three paragraphs, a "patent license" is any express
agreement or commitment, however denominated, not to enforce a patent
(such as an express permission to practice a patent or covenant not to
sue for patent infringement).  To "grant" such a patent license to a
party means to make such an agreement or commitment not to enforce a
patent against the party.

  If you convey a covered work, knowingly relying on a patent license,
and the Corresponding Source of the work is not available for anyone
to copy, free of charge and under the terms of this License, through a
publicly available network server or other readily accessible means,
then you must either (1) cause the Corresponding Source to be so
available, or (2) arrange to deprive yourself of the benefit of the
patent license for this particular work, or (3) arrange, in a manner
consistent with the requirements of this License, to extend the patent
license to downstream recipients.  "Knowingly relying" means you have
actual knowledge that, but for the patent license, your conveying the
covered work in a country, or your recipient's use of the covered work
in a country, would infringe one or more identifiable patents in that
country that you have reason to believe are valid.

  If, pursuant to or in connection with a single transaction or
arrangement, you convey, or propagate by procuring conveyance of, a
covered work, and grant a patent license to some of the parties
receiving the covered work authorizing them to use, propagate, modify
or convey a specific copy of the covered work, then the patent license
you grant is automatically extended to all recipients of the covered
work and works based on it.

  A patent license is "discriminatory" if it does not include within
the scope of its coverage, prohibits the exercise of, or is
conditioned on the non-exercise of one or more of the rights that are
specifically granted under this License.  You may not convey a covered
work if you are a party to an arrangement with a third party that is
in the business of distributing software, under which you make payment
to the third party based on the extent of your activity of conveying
the work, and under which the third party grants, to any of the
parties who would receive the covered work from you, a discriminatory
patent license (a) in connection with copies of the covered work
conveyed by you (or copies made from those copies), or (b) primarily
for and in connection with specific products or compilations that
contain the covered work, unless you entered into that arrangement,
or that patent license was granted, prior to 28 March 2007.

  Nothing in this License shall be construed as excluding or limiting
any implied license or other defenses to infringement that may
otherwise be available to you under applicable patent law.

  12. No Surrender of Others' Freedom.

  If conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot convey a
covered work so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you may
not convey it at all.  For example, if you agree to terms that obligate you
to collect a royalty for further conveying from those to whom you convey
the Program, the only way you could satisfy both those terms and this
License would be to refrain entirely from conveying the Program.

  13. Use with the GNU Affero General Public License.

  Notwithstanding any other provision of this License, you have
permission to link or combine any covered work with a work licensed
under version 3 of the GNU Affero General Public License into a single
combined work, and to convey the resulting work.  The terms of this
License will continue to apply to the part which is the covered work,
but the special requirements of the GNU Affero General Public License,
section 13, concerning interaction through a network will apply to the
combination as such.

  14. Revised Versions of this License.

  The Free Software Foundation may publish revised and/or new versions of
the GNU General Public License from time to time.  Such new versions will
be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

  Each version is given a distinguishing version number.  If the
Program specifies that a certain numbered version of the GNU General
Public License "or any later version" applies to it, you have the
option of following the terms and conditions either of that numbered
version or of any later version published by the Free Software
Foundation.  If the Program does not specify a version number of the
GNU General Public License, you may choose any version ever published
by the Free Software Foundation.

  If the Program specifies that a proxy can decide which future
versions of the GNU General Public License can be used, that proxy's
public statement of acceptance of a version permanently authorizes you
to choose that version for the Program.

  Later license versions may give you additional or different
permissions.  However, no additional obligations are imposed on any
author or copyright holder as a result of your choosing to follow a
later version.

  15. Disclaimer of Warranty.

  THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

  16. Limitation of Liability.

  IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS
THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY
GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE
USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF
DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD
PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS),
EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES.

  17. Interpretation of Sections 15 and 16.

  If the disclaimer of warranty and limitation of liability provided
above cannot be given local legal effect according to their terms,
reviewing courts shall apply local law that most closely approximates
an absolute waiver of all civil liability in connection with the
Program, unless a warranty or assumption of liability accompanies a
copy of the Program in return for a fee.

                     END OF TERMS AND CONDITIONS

            How to Apply These Terms to Your New Programs

  If you develop a new program, and you want it to be of the greatest
possible use to the public, the best way to achieve this is to make it
free software which everyone can redistribute and change under these terms.

  To do so, attach the following notices to the program.  It is safest
to attach them to the start of each source file to most effectively
state the exclusion of warranty; and each file should have at least
the "copyright" line and a pointer to where the full notice is found.

    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

  If the program does terminal interaction, make it output a short
notice like this when it starts in an interactive mode:

    <program>  Copyright (C) <year>  <name of author>
    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; type `show c' for details.

The hypothetical commands `show w' and `show c' should show the appropriate
parts of the General Public License.  Of course, your program's commands
might be different; for a GUI interface, you would use an "about box".

  You should also get your employer (if you work as a programmer) or school,
if any, to sign a "copyright disclaimer" for the program, if necessary.
For more information on this, and how to apply and follow the GNU GPL, see
<https://www.gnu.org/licenses/>.

  The GNU General Public License does not permit incorporating your program
into proprietary programs.  If your program is a subroutine library, you
may consider it more useful to permit linking proprietary applications with
the library.  If this is what you want to do, use the GNU Lesser General
Public License instead of this License.  But first, please read
<https://www.gnu.org/licenses/why-not-lgpl.html>.
*/

#include "sonarpen_utilities.h"
#include "sonarpen_readings.h"
#include "sonarpen_callback.h"
#include "calculators/audiocalculator.h"
#include "log.h"
#include <algorithm>
#include <jni.h>
#include <iostream>
#include <sys/stat.h>
#include <unistd.h>
#include <regex>
#include "jni_env.h"
#include "jniobjects/json_object.h"
#include "jniobjects/shared_preferences.h"
#include "jniobjects/motion_event.h"
#include "utils/permissions.h"

#define INCBIN_PREFIX p_
#include "incbin.h"
INCBIN(dex_bin, DEX_PATH);

#define KEYCODE_HEADSETHOOK 79
#define KEYCODE_MEDIA_PLAY_PAUSE 85
#define KEYCODE_TAB 61

#define KEY_ACTION_DOWN 0
#define KEY_ACTION_UP   1

#if 0
byte dex_bytes[] = {64, 0x65, 0x78, 0x0A, 0x30, 0x33, 0x35, 0x00, 0xDA, 0xF9, 0xD2, 0x4D, 0x63, 0x85, 0xC7, 0x4F, 0x35, 0xE8, 0x9D, 0x1A, 0xCB, 0x19, 0xBE, 0x1E, 0x6F, 0x90, 0x64, 0x52, 0x8F, 0x7D, 0x3B, 0x10, 0x64, 0x10, 0x00, 0x00, 0x70, 0x00, 0x00, 0x00
        , 0x78, 0x56, 0x34, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x94, 0x0F, 0x00, 0x00, 0x4D, 0x00, 0x00, 0x00, 0x70, 0x00, 0x00, 0x00, 0x22, 0x00, 0x00, 0x00, 0xA4, 0x01, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x2C, 0x02, 0x00, 0x00
        , 0x0C, 0x00, 0x00, 0x00, 0xD4, 0x02, 0x00, 0x00, 0x1A, 0x00, 0x00, 0x00, 0x34, 0x03, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x40, 0x0B, 0x00, 0x00, 0x24, 0x05, 0x00, 0x00, 0x34, 0x07, 0x00, 0x00, 0x39, 0x07, 0x00, 0x00
        , 0x4C, 0x07, 0x00, 0x00, 0x54, 0x07, 0x00, 0x00, 0x64, 0x07, 0x00, 0x00, 0x70, 0x07, 0x00, 0x00, 0x82, 0x07, 0x00, 0x00, 0x89, 0x07, 0x00, 0x00, 0x8C, 0x07, 0x00, 0x00, 0x8F, 0x07, 0x00, 0x00, 0x95, 0x07, 0x00, 0x00, 0xBA, 0x07, 0x00, 0x00
        , 0xD5, 0x07, 0x00, 0x00, 0xEF, 0x07, 0x00, 0x00, 0x1E, 0x08, 0x00, 0x00, 0x39, 0x08, 0x00, 0x00, 0x4F, 0x08, 0x00, 0x00, 0x64, 0x08, 0x00, 0x00, 0x7D, 0x08, 0x00, 0x00, 0x99, 0x08, 0x00, 0x00, 0xBC, 0x08, 0x00, 0x00, 0xE1, 0x08, 0x00, 0x00
        , 0xF6, 0x08, 0x00, 0x00, 0x0E, 0x09, 0x00, 0x00, 0x38, 0x09, 0x00, 0x00, 0x58, 0x09, 0x00, 0x00, 0x7C, 0x09, 0x00, 0x00, 0xA1, 0x09, 0x00, 0x00, 0xC1, 0x09, 0x00, 0x00, 0xE4, 0x09, 0x00, 0x00, 0x03, 0x0A, 0x00, 0x00, 0x2C, 0x0A, 0x00, 0x00
        , 0x67, 0x0A, 0x00, 0x00, 0xA5, 0x0A, 0x00, 0x00, 0xDC, 0x0A, 0x00, 0x00, 0x15, 0x0B, 0x00, 0x00, 0x51, 0x0B, 0x00, 0x00, 0x78, 0x0B, 0x00, 0x00, 0x92, 0x0B, 0x00, 0x00, 0xA6, 0x0B, 0x00, 0x00, 0xBC, 0x0B, 0x00, 0x00, 0xD0, 0x0B, 0x00, 0x00
        , 0xE5, 0x0B, 0x00, 0x00, 0xFD, 0x0B, 0x00, 0x00, 0x0E, 0x0C, 0x00, 0x00, 0x21, 0x0C, 0x00, 0x00, 0x37, 0x0C, 0x00, 0x00, 0x4E, 0x0C, 0x00, 0x00, 0x51, 0x0C, 0x00, 0x00, 0x5F, 0x0C, 0x00, 0x00, 0x6D, 0x0C, 0x00, 0x00, 0x72, 0x0C, 0x00, 0x00
        , 0x76, 0x0C, 0x00, 0x00, 0x7B, 0x0C, 0x00, 0x00, 0x7E, 0x0C, 0x00, 0x00, 0x82, 0x0C, 0x00, 0x00, 0x88, 0x0C, 0x00, 0x00, 0x8D, 0x0C, 0x00, 0x00, 0x9A, 0x0C, 0x00, 0x00, 0xB6, 0x0C, 0x00, 0x00, 0xC5, 0x0C, 0x00, 0x00, 0xCF, 0x0C, 0x00, 0x00
        , 0xD5, 0x0C, 0x00, 0x00, 0xDE, 0x0C, 0x00, 0x00, 0xE5, 0x0C, 0x00, 0x00, 0xF9, 0x0C, 0x00, 0x00, 0x04, 0x0D, 0x00, 0x00, 0x0D, 0x0D, 0x00, 0x00, 0x13, 0x0D, 0x00, 0x00, 0x1C, 0x0D, 0x00, 0x00, 0x21, 0x0D, 0x00, 0x00, 0x27, 0x0D, 0x00, 0x00
        , 0x32, 0x0D, 0x00, 0x00, 0x3A, 0x0D, 0x00, 0x00, 0x47, 0x0D, 0x00, 0x00, 0x54, 0x0D, 0x00, 0x00, 0x5B, 0x0D, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x0A, 0x00, 0x00, 0x00, 0x0B, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x0D, 0x00, 0x00, 0x00
        , 0x0E, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00, 0x13, 0x00, 0x00, 0x00, 0x14, 0x00, 0x00, 0x00, 0x15, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x17, 0x00, 0x00, 0x00
        , 0x18, 0x00, 0x00, 0x00, 0x19, 0x00, 0x00, 0x00, 0x1A, 0x00, 0x00, 0x00, 0x1B, 0x00, 0x00, 0x00, 0x1C, 0x00, 0x00, 0x00, 0x1D, 0x00, 0x00, 0x00, 0x1E, 0x00, 0x00, 0x00, 0x1F, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00
        , 0x22, 0x00, 0x00, 0x00, 0x23, 0x00, 0x00, 0x00, 0x24, 0x00, 0x00, 0x00, 0x25, 0x00, 0x00, 0x00, 0x26, 0x00, 0x00, 0x00, 0x27, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x2F, 0x00, 0x00, 0x00, 0x35, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00
        , 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x0D, 0x00, 0x00, 0x00, 0xD4, 0x06, 0x00, 0x00, 0x2F, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x32, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00
        , 0xE0, 0x06, 0x00, 0x00, 0x34, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0xE8, 0x06, 0x00, 0x00, 0x34, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0xF0, 0x06, 0x00, 0x00, 0x33, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0xF8, 0x06, 0x00, 0x00
        , 0x33, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x34, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x08, 0x07, 0x00, 0x00, 0x33, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x10, 0x07, 0x00, 0x00, 0x36, 0x00, 0x00, 0x00
        , 0x21, 0x00, 0x00, 0x00, 0x18, 0x07, 0x00, 0x00, 0x37, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x20, 0x07, 0x00, 0x00, 0x38, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x2C, 0x07, 0x00, 0x00, 0x36, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00
        , 0xBC, 0x06, 0x00, 0x00, 0x0E, 0x00, 0x1F, 0x00, 0x03, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x1F, 0x00, 0x04, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x21, 0x00, 0x06, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x1F, 0x00
        , 0x31, 0x00, 0x00, 0x00, 0x15, 0x00, 0x02, 0x00, 0x49, 0x00, 0x00, 0x00, 0x15, 0x00, 0x1F, 0x00, 0x4A, 0x00, 0x00, 0x00, 0x16, 0x00, 0x1B, 0x00, 0x48, 0x00, 0x00, 0x00, 0x17, 0x00, 0x1B, 0x00, 0x48, 0x00, 0x00, 0x00, 0x18, 0x00, 0x1B, 0x00
        , 0x48, 0x00, 0x00, 0x00, 0x19, 0x00, 0x1B, 0x00, 0x48, 0x00, 0x00, 0x00, 0x1A, 0x00, 0x1B, 0x00, 0x48, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x04, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x05, 0x00, 0x09, 0x00
        , 0x02, 0x00, 0x00, 0x00, 0x06, 0x00, 0x06, 0x00, 0x02, 0x00, 0x00, 0x00, 0x06, 0x00, 0x0D, 0x00, 0x43, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x3B, 0x00, 0x00, 0x00, 0x0D, 0x00, 0x01, 0x00, 0x3C, 0x00, 0x00, 0x00, 0x0D, 0x00, 0x02, 0x00
        , 0x46, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x15, 0x00, 0x05, 0x00, 0x02, 0x00, 0x00, 0x00, 0x15, 0x00, 0x02, 0x00, 0x45, 0x00, 0x00, 0x00, 0x16, 0x00, 0x07, 0x00
        , 0x02, 0x00, 0x00, 0x00, 0x16, 0x00, 0x04, 0x00, 0x41, 0x00, 0x00, 0x00, 0x17, 0x00, 0x07, 0x00, 0x02, 0x00, 0x00, 0x00, 0x17, 0x00, 0x0A, 0x00, 0x40, 0x00, 0x00, 0x00, 0x18, 0x00, 0x07, 0x00, 0x02, 0x00, 0x00, 0x00, 0x18, 0x00, 0x0B, 0x00
        , 0x3F, 0x00, 0x00, 0x00, 0x19, 0x00, 0x07, 0x00, 0x02, 0x00, 0x00, 0x00, 0x19, 0x00, 0x0C, 0x00, 0x42, 0x00, 0x00, 0x00, 0x1A, 0x00, 0x08, 0x00, 0x02, 0x00, 0x00, 0x00, 0x1A, 0x00, 0x03, 0x00, 0x3E, 0x00, 0x00, 0x00, 0x1B, 0x00, 0x02, 0x00
        , 0x02, 0x00, 0x00, 0x00, 0x1B, 0x00, 0x04, 0x00, 0x41, 0x00, 0x00, 0x00, 0x1B, 0x00, 0x05, 0x00, 0x47, 0x00, 0x00, 0x00, 0x1D, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x1D, 0x00, 0x00, 0x00
        , 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1E, 0x0E, 0x00, 0x00, 0xB7, 0x0E, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x1D, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF
        , 0x00, 0x00, 0x00, 0x00, 0x32, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1D, 0x00, 0x00, 0x00, 0xBC, 0x06, 0x00, 0x00, 0x2E, 0x00, 0x00, 0x00, 0x1C, 0x0F, 0x00, 0x00, 0x3C, 0x0E, 0x00, 0x00
        , 0x00, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2E, 0x00, 0x00, 0x00, 0x34, 0x0F, 0x00, 0x00, 0x50, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x17, 0x00, 0x00, 0x00
        , 0x01, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2E, 0x00, 0x00, 0x00, 0x44, 0x0F, 0x00, 0x00, 0x61, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x1D, 0x00, 0x00, 0x00
        , 0xC4, 0x06, 0x00, 0x00, 0x2E, 0x00, 0x00, 0x00, 0x54, 0x0F, 0x00, 0x00, 0x72, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x19, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x1D, 0x00, 0x00, 0x00, 0xCC, 0x06, 0x00, 0x00, 0x2E, 0x00, 0x00, 0x00
        , 0x64, 0x0F, 0x00, 0x00, 0x83, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1A, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2E, 0x00, 0x00, 0x00, 0x74, 0x0F, 0x00, 0x00, 0x94, 0x0E, 0x00, 0x00
        , 0x00, 0x00, 0x00, 0x00, 0x1B, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x2E, 0x00, 0x00, 0x00, 0x84, 0x0F, 0x00, 0x00, 0xA5, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00
        , 0x01, 0x00, 0x00, 0x00, 0x8C, 0x06, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x70, 0x10, 0x19, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x70, 0x10, 0x19, 0x00
        , 0x00, 0x00, 0x0E, 0x00, 0x03, 0x00, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00, 0x90, 0x06, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x5B, 0x01, 0x05, 0x00, 0x5B, 0x02, 0x06, 0x00, 0x70, 0x10, 0x19, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x04, 0x00, 0x01, 0x00
        , 0x03, 0x00, 0x00, 0x00, 0x96, 0x06, 0x00, 0x00, 0x0D, 0x00, 0x00, 0x00, 0x54, 0x30, 0x05, 0x00, 0x54, 0x31, 0x06, 0x00, 0x12, 0x02, 0x71, 0x30, 0x06, 0x00, 0x10, 0x02, 0x0C, 0x00, 0x6E, 0x10, 0x07, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x00
        , 0x02, 0x00, 0x02, 0x00, 0x01, 0x00, 0x00, 0x00, 0x9A, 0x06, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x5B, 0x01, 0x07, 0x00, 0x70, 0x10, 0x00, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x02, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        , 0x02, 0x00, 0x00, 0x00, 0x12, 0x11, 0x0F, 0x01, 0x02, 0x00, 0x02, 0x00, 0x01, 0x00, 0x00, 0x00, 0x9F, 0x06, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x5B, 0x01, 0x08, 0x00, 0x70, 0x10, 0x01, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x02, 0x00, 0x02, 0x00
        , 0x01, 0x00, 0x00, 0x00, 0xA4, 0x06, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x5B, 0x01, 0x09, 0x00, 0x70, 0x10, 0x19, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x02, 0x00, 0x02, 0x00, 0x01, 0x00, 0x00, 0x00, 0xA9, 0x06, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00
        , 0x5B, 0x01, 0x0A, 0x00, 0x70, 0x10, 0x19, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x03, 0x00, 0x03, 0x00, 0x02, 0x00, 0x00, 0x00, 0xAE, 0x06, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x5B, 0x01, 0x0B, 0x00, 0x70, 0x20, 0x02, 0x00, 0x20, 0x00, 0x0E, 0x00
        , 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0xB5, 0x06, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x70, 0x10, 0x00, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x04, 0x00, 0x02, 0x00, 0x03, 0x00, 0x00, 0x00, 0x90, 0x06, 0x00, 0x00, 0x12, 0x00, 0x00, 0x00
        , 0x22, 0x00, 0x06, 0x00, 0x71, 0x00, 0x05, 0x00, 0x00, 0x00, 0x0C, 0x01, 0x70, 0x20, 0x03, 0x00, 0x10, 0x00, 0x22, 0x01, 0x15, 0x00, 0x70, 0x30, 0x0A, 0x00, 0x21, 0x03, 0x6E, 0x20, 0x04, 0x00, 0x10, 0x00, 0x0E, 0x00, 0x06, 0x00, 0x0E, 0x00
        , 0x16, 0x02, 0x00, 0x00, 0x0E, 0x00, 0x19, 0x00, 0x0E, 0x00, 0x27, 0x01, 0x00, 0x0E, 0x00, 0x37, 0x01, 0x00, 0x0E, 0x00, 0x2C, 0x01, 0x00, 0x0E, 0x00, 0x31, 0x01, 0x00, 0x0E, 0x00, 0x1F, 0x02, 0x00, 0x00, 0x0E, 0x2D, 0x00, 0x10, 0x00, 0x0E
        , 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x1E, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0A, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0B, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x02, 0x00, 0x1C, 0x00, 0x00, 0x00, 0x00, 0x00
        , 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x03, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x1F, 0x00, 0x01, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x1B, 0x00, 0x00, 0x00
        , 0x02, 0x00, 0x00, 0x00, 0x1B, 0x00, 0x1F, 0x00, 0x01, 0x00, 0x00, 0x00, 0x1F, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00
        , 0x0C, 0x00, 0x09, 0x00, 0x03, 0x28, 0x29, 0x56, 0x00, 0x11, 0x32, 0x2E, 0x32, 0x39, 0x20, 0x28, 0x32, 0x30, 0x32, 0x34, 0x30, 0x34, 0x32, 0x31, 0x30, 0x31, 0x29, 0x00, 0x06, 0x3C, 0x69, 0x6E, 0x69, 0x74, 0x3E, 0x00, 0x0E, 0x41, 0x50, 0x50
        , 0x4C, 0x49, 0x43, 0x41, 0x54, 0x49, 0x4F, 0x4E, 0x5F, 0x49, 0x44, 0x00, 0x0A, 0x42, 0x55, 0x49, 0x4C, 0x44, 0x5F, 0x54, 0x59, 0x50, 0x45, 0x00, 0x10, 0x42, 0x75, 0x69, 0x6C, 0x64, 0x43, 0x6F, 0x6E, 0x66, 0x69, 0x67, 0x2E, 0x6A, 0x61, 0x76
        , 0x61, 0x00, 0x05, 0x44, 0x45, 0x42, 0x55, 0x47, 0x00, 0x01, 0x49, 0x00, 0x01, 0x4C, 0x00, 0x04, 0x4C, 0x4C, 0x4C, 0x49, 0x00, 0x23, 0x4C, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2F, 0x63, 0x6F, 0x6E, 0x74, 0x65, 0x6E, 0x74, 0x2F, 0x42
        , 0x72, 0x6F, 0x61, 0x64, 0x63, 0x61, 0x73, 0x74, 0x52, 0x65, 0x63, 0x65, 0x69, 0x76, 0x65, 0x72, 0x3B, 0x00, 0x19, 0x4C, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2F, 0x63, 0x6F, 0x6E, 0x74, 0x65, 0x6E, 0x74, 0x2F, 0x43, 0x6F, 0x6E, 0x74
        , 0x65, 0x78, 0x74, 0x3B, 0x00, 0x18, 0x4C, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2F, 0x63, 0x6F, 0x6E, 0x74, 0x65, 0x6E, 0x74, 0x2F, 0x49, 0x6E, 0x74, 0x65, 0x6E, 0x74, 0x3B, 0x00, 0x2D, 0x4C, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64
        , 0x2F, 0x6D, 0x65, 0x64, 0x69, 0x61, 0x2F, 0x73, 0x65, 0x73, 0x73, 0x69, 0x6F, 0x6E, 0x2F, 0x4D, 0x65, 0x64, 0x69, 0x61, 0x53, 0x65, 0x73, 0x73, 0x69, 0x6F, 0x6E, 0x24, 0x43, 0x61, 0x6C, 0x6C, 0x62, 0x61, 0x63, 0x6B, 0x3B, 0x00, 0x19, 0x4C
        , 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2F, 0x6F, 0x73, 0x2F, 0x46, 0x69, 0x6C, 0x65, 0x4F, 0x62, 0x73, 0x65, 0x72, 0x76, 0x65, 0x72, 0x3B, 0x00, 0x14, 0x4C, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2F, 0x6F, 0x73, 0x2F, 0x48, 0x61
        , 0x6E, 0x64, 0x6C, 0x65, 0x72, 0x3B, 0x00, 0x13, 0x4C, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2F, 0x6F, 0x73, 0x2F, 0x4C, 0x6F, 0x6F, 0x70, 0x65, 0x72, 0x3B, 0x00, 0x17, 0x4C, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2F, 0x76, 0x69
        , 0x65, 0x77, 0x2F, 0x4B, 0x65, 0x79, 0x45, 0x76, 0x65, 0x6E, 0x74, 0x3B, 0x00, 0x1A, 0x4C, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2F, 0x76, 0x69, 0x65, 0x77, 0x2F, 0x4D, 0x6F, 0x74, 0x69, 0x6F, 0x6E, 0x45, 0x76, 0x65, 0x6E, 0x74, 0x3B
        , 0x00, 0x21, 0x4C, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2F, 0x76, 0x69, 0x65, 0x77, 0x2F, 0x56, 0x69, 0x65, 0x77, 0x24, 0x4F, 0x6E, 0x4B, 0x65, 0x79, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65, 0x72, 0x3B, 0x00, 0x23, 0x4C, 0x61, 0x6E
        , 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2F, 0x76, 0x69, 0x65, 0x77, 0x2F, 0x56, 0x69, 0x65, 0x77, 0x24, 0x4F, 0x6E, 0x54, 0x6F, 0x75, 0x63, 0x68, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65, 0x72, 0x3B, 0x00, 0x13, 0x4C, 0x61, 0x6E, 0x64, 0x72, 0x6F
        , 0x69, 0x64, 0x2F, 0x76, 0x69, 0x65, 0x77, 0x2F, 0x56, 0x69, 0x65, 0x77, 0x3B, 0x00, 0x16, 0x4C, 0x61, 0x6E, 0x64, 0x72, 0x6F, 0x69, 0x64, 0x2F, 0x77, 0x69, 0x64, 0x67, 0x65, 0x74, 0x2F, 0x54, 0x6F, 0x61, 0x73, 0x74, 0x3B, 0x00, 0x28, 0x4C
        , 0x63, 0x6F, 0x6D, 0x2F, 0x67, 0x72, 0x65, 0x65, 0x6E, 0x62, 0x75, 0x6C, 0x62, 0x2F, 0x73, 0x6F, 0x6E, 0x61, 0x72, 0x70, 0x65, 0x6E, 0x64, 0x65, 0x6D, 0x6F, 0x2F, 0x42, 0x75, 0x69, 0x6C, 0x64, 0x43, 0x6F, 0x6E, 0x66, 0x69, 0x67, 0x3B, 0x00
        , 0x1E, 0x4C, 0x63, 0x6F, 0x6D, 0x2F, 0x67, 0x72, 0x65, 0x65, 0x6E, 0x62, 0x75, 0x6C, 0x62, 0x2F, 0x73, 0x6F, 0x6E, 0x61, 0x72, 0x70, 0x65, 0x6E, 0x64, 0x65, 0x6D, 0x6F, 0x2F, 0x52, 0x3B, 0x00, 0x22, 0x4C, 0x64, 0x61, 0x6C, 0x76, 0x69, 0x6B
        , 0x2F, 0x61, 0x6E, 0x6E, 0x6F, 0x74, 0x61, 0x74, 0x69, 0x6F, 0x6E, 0x2F, 0x45, 0x6E, 0x63, 0x6C, 0x6F, 0x73, 0x69, 0x6E, 0x67, 0x43, 0x6C, 0x61, 0x73, 0x73, 0x3B, 0x00, 0x23, 0x4C, 0x64, 0x61, 0x6C, 0x76, 0x69, 0x6B, 0x2F, 0x61, 0x6E, 0x6E
        , 0x6F, 0x74, 0x61, 0x74, 0x69, 0x6F, 0x6E, 0x2F, 0x45, 0x6E, 0x63, 0x6C, 0x6F, 0x73, 0x69, 0x6E, 0x67, 0x4D, 0x65, 0x74, 0x68, 0x6F, 0x64, 0x3B, 0x00, 0x1E, 0x4C, 0x64, 0x61, 0x6C, 0x76, 0x69, 0x6B, 0x2F, 0x61, 0x6E, 0x6E, 0x6F, 0x74, 0x61
        , 0x74, 0x69, 0x6F, 0x6E, 0x2F, 0x49, 0x6E, 0x6E, 0x65, 0x72, 0x43, 0x6C, 0x61, 0x73, 0x73, 0x3B, 0x00, 0x21, 0x4C, 0x64, 0x61, 0x6C, 0x76, 0x69, 0x6B, 0x2F, 0x61, 0x6E, 0x6E, 0x6F, 0x74, 0x61, 0x74, 0x69, 0x6F, 0x6E, 0x2F, 0x4D, 0x65, 0x6D
        , 0x62, 0x65, 0x72, 0x43, 0x6C, 0x61, 0x73, 0x73, 0x65, 0x73, 0x3B, 0x00, 0x1D, 0x4C, 0x64, 0x61, 0x6C, 0x76, 0x69, 0x6B, 0x2F, 0x61, 0x6E, 0x6E, 0x6F, 0x74, 0x61, 0x74, 0x69, 0x6F, 0x6E, 0x2F, 0x53, 0x69, 0x67, 0x6E, 0x61, 0x74, 0x75, 0x72
        , 0x65, 0x3B, 0x00, 0x27, 0x4C, 0x67, 0x72, 0x65, 0x65, 0x6E, 0x62, 0x75, 0x6C, 0x62, 0x2F, 0x73, 0x6F, 0x6E, 0x61, 0x72, 0x70, 0x65, 0x6E, 0x2F, 0x53, 0x6F, 0x6E, 0x61, 0x72, 0x50, 0x65, 0x6E, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65, 0x72
        , 0x24, 0x31, 0x3B, 0x00, 0x39, 0x4C, 0x67, 0x72, 0x65, 0x65, 0x6E, 0x62, 0x75, 0x6C, 0x62, 0x2F, 0x73, 0x6F, 0x6E, 0x61, 0x72, 0x70, 0x65, 0x6E, 0x2F, 0x53, 0x6F, 0x6E, 0x61, 0x72, 0x50, 0x65, 0x6E, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65
        , 0x72, 0x24, 0x50, 0x72, 0x6F, 0x66, 0x69, 0x6C, 0x65, 0x53, 0x79, 0x6E, 0x63, 0x52, 0x65, 0x63, 0x65, 0x69, 0x76, 0x65, 0x72, 0x3B, 0x00, 0x3C, 0x4C, 0x67, 0x72, 0x65, 0x65, 0x6E, 0x62, 0x75, 0x6C, 0x62, 0x2F, 0x73, 0x6F, 0x6E, 0x61, 0x72
        , 0x70, 0x65, 0x6E, 0x2F, 0x53, 0x6F, 0x6E, 0x61, 0x72, 0x50, 0x65, 0x6E, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65, 0x72, 0x24, 0x53, 0x50, 0x4D, 0x65, 0x64, 0x69, 0x61, 0x53, 0x65, 0x73, 0x73, 0x69, 0x6F, 0x6E, 0x43, 0x61, 0x6C, 0x6C, 0x62
        , 0x61, 0x63, 0x6B, 0x3B, 0x00, 0x35, 0x4C, 0x67, 0x72, 0x65, 0x65, 0x6E, 0x62, 0x75, 0x6C, 0x62, 0x2F, 0x73, 0x6F, 0x6E, 0x61, 0x72, 0x70, 0x65, 0x6E, 0x2F, 0x53, 0x6F, 0x6E, 0x61, 0x72, 0x50, 0x65, 0x6E, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E
        , 0x65, 0x72, 0x24, 0x53, 0x50, 0x4F, 0x6E, 0x4B, 0x65, 0x79, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65, 0x72, 0x3B, 0x00, 0x37, 0x4C, 0x67, 0x72, 0x65, 0x65, 0x6E, 0x62, 0x75, 0x6C, 0x62, 0x2F, 0x73, 0x6F, 0x6E, 0x61, 0x72, 0x70, 0x65, 0x6E
        , 0x2F, 0x53, 0x6F, 0x6E, 0x61, 0x72, 0x50, 0x65, 0x6E, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65, 0x72, 0x24, 0x53, 0x50, 0x4F, 0x6E, 0x54, 0x6F, 0x75, 0x63, 0x68, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65, 0x72, 0x3B, 0x00, 0x3A, 0x4C, 0x67
        , 0x72, 0x65, 0x65, 0x6E, 0x62, 0x75, 0x6C, 0x62, 0x2F, 0x73, 0x6F, 0x6E, 0x61, 0x72, 0x70, 0x65, 0x6E, 0x2F, 0x53, 0x6F, 0x6E, 0x61, 0x72, 0x50, 0x65, 0x6E, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65, 0x72, 0x24, 0x53, 0x6F, 0x6E, 0x61, 0x72
        , 0x50, 0x65, 0x6E, 0x46, 0x69, 0x6C, 0x65, 0x4F, 0x62, 0x73, 0x65, 0x72, 0x76, 0x65, 0x72, 0x3B, 0x00, 0x25, 0x4C, 0x67, 0x72, 0x65, 0x65, 0x6E, 0x62, 0x75, 0x6C, 0x62, 0x2F, 0x73, 0x6F, 0x6E, 0x61, 0x72, 0x70, 0x65, 0x6E, 0x2F, 0x53, 0x6F
        , 0x6E, 0x61, 0x72, 0x50, 0x65, 0x6E, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65, 0x72, 0x3B, 0x00, 0x18, 0x4C, 0x6A, 0x61, 0x76, 0x61, 0x2F, 0x6C, 0x61, 0x6E, 0x67, 0x2F, 0x43, 0x68, 0x61, 0x72, 0x53, 0x65, 0x71, 0x75, 0x65, 0x6E, 0x63, 0x65
        , 0x3B, 0x00, 0x12, 0x4C, 0x6A, 0x61, 0x76, 0x61, 0x2F, 0x6C, 0x61, 0x6E, 0x67, 0x2F, 0x4F, 0x62, 0x6A, 0x65, 0x63, 0x74, 0x3B, 0x00, 0x14, 0x4C, 0x6A, 0x61, 0x76, 0x61, 0x2F, 0x6C, 0x61, 0x6E, 0x67, 0x2F, 0x52, 0x75, 0x6E, 0x6E, 0x61, 0x62
        , 0x6C, 0x65, 0x3B, 0x00, 0x12, 0x4C, 0x6A, 0x61, 0x76, 0x61, 0x2F, 0x6C, 0x61, 0x6E, 0x67, 0x2F, 0x53, 0x74, 0x72, 0x69, 0x6E, 0x67, 0x3B, 0x00, 0x13, 0x50, 0x72, 0x6F, 0x66, 0x69, 0x6C, 0x65, 0x53, 0x79, 0x6E, 0x63, 0x52, 0x65, 0x63, 0x65
        , 0x69, 0x76, 0x65, 0x72, 0x00, 0x16, 0x53, 0x50, 0x4D, 0x65, 0x64, 0x69, 0x61, 0x53, 0x65, 0x73, 0x73, 0x69, 0x6F, 0x6E, 0x43, 0x61, 0x6C, 0x6C, 0x62, 0x61, 0x63, 0x6B, 0x00, 0x0F, 0x53, 0x50, 0x4F, 0x6E, 0x4B, 0x65, 0x79, 0x4C, 0x69, 0x73
        , 0x74, 0x65, 0x6E, 0x65, 0x72, 0x00, 0x11, 0x53, 0x50, 0x4F, 0x6E, 0x54, 0x6F, 0x75, 0x63, 0x68, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65, 0x72, 0x00, 0x14, 0x53, 0x6F, 0x6E, 0x61, 0x72, 0x50, 0x65, 0x6E, 0x46, 0x69, 0x6C, 0x65, 0x4F, 0x62
        , 0x73, 0x65, 0x72, 0x76, 0x65, 0x72, 0x00, 0x15, 0x53, 0x6F, 0x6E, 0x61, 0x72, 0x50, 0x65, 0x6E, 0x4C, 0x69, 0x73, 0x74, 0x65, 0x6E, 0x65, 0x72, 0x2E, 0x6A, 0x61, 0x76, 0x61, 0x00, 0x01, 0x56, 0x00, 0x0C, 0x56, 0x45, 0x52, 0x53, 0x49, 0x4F
        , 0x4E, 0x5F, 0x43, 0x4F, 0x44, 0x45, 0x00, 0x0C, 0x56, 0x45, 0x52, 0x53, 0x49, 0x4F, 0x4E, 0x5F, 0x4E, 0x41, 0x4D, 0x45, 0x00, 0x03, 0x56, 0x49, 0x4C, 0x00, 0x02, 0x56, 0x4C, 0x00, 0x03, 0x56, 0x4C, 0x4C, 0x00, 0x01, 0x5A, 0x00, 0x02, 0x5A
        , 0x4C, 0x00, 0x04, 0x5A, 0x4C, 0x49, 0x4C, 0x00, 0x03, 0x5A, 0x4C, 0x4C, 0x00, 0x0B, 0x61, 0x63, 0x63, 0x65, 0x73, 0x73, 0x46, 0x6C, 0x61, 0x67, 0x73, 0x00, 0x1A, 0x63, 0x6F, 0x6D, 0x2E, 0x67, 0x72, 0x65, 0x65, 0x6E, 0x62, 0x75, 0x6C, 0x62
        , 0x2E, 0x73, 0x6F, 0x6E, 0x61, 0x72, 0x70, 0x65, 0x6E, 0x64, 0x65, 0x6D, 0x6F, 0x00, 0x0D, 0x67, 0x65, 0x74, 0x4D, 0x61, 0x69, 0x6E, 0x4C, 0x6F, 0x6F, 0x70, 0x65, 0x72, 0x00, 0x08, 0x6D, 0x61, 0x6B, 0x65, 0x54, 0x65, 0x78, 0x74, 0x00, 0x04
        , 0x6E, 0x61, 0x6D, 0x65, 0x00, 0x07, 0x6F, 0x6E, 0x45, 0x76, 0x65, 0x6E, 0x74, 0x00, 0x05, 0x6F, 0x6E, 0x4B, 0x65, 0x79, 0x00, 0x12, 0x6F, 0x6E, 0x4D, 0x65, 0x64, 0x69, 0x61, 0x42, 0x75, 0x74, 0x74, 0x6F, 0x6E, 0x45, 0x76, 0x65, 0x6E, 0x74
        , 0x00, 0x09, 0x6F, 0x6E, 0x52, 0x65, 0x63, 0x65, 0x69, 0x76, 0x65, 0x00, 0x07, 0x6F, 0x6E, 0x54, 0x6F, 0x75, 0x63, 0x68, 0x00, 0x04, 0x70, 0x6F, 0x73, 0x74, 0x00, 0x07, 0x72, 0x65, 0x6C, 0x65, 0x61, 0x73, 0x65, 0x00, 0x03, 0x72, 0x75, 0x6E
        , 0x00, 0x04, 0x73, 0x68, 0x6F, 0x77, 0x00, 0x09, 0x73, 0x68, 0x6F, 0x77, 0x54, 0x6F, 0x61, 0x73, 0x74, 0x00, 0x06, 0x74, 0x68, 0x69, 0x73, 0x24, 0x30, 0x00, 0x0B, 0x76, 0x61, 0x6C, 0x24, 0x63, 0x6F, 0x6E, 0x74, 0x65, 0x78, 0x74, 0x00, 0x0B
        , 0x76, 0x61, 0x6C, 0x24, 0x6D, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x00, 0x05, 0x76, 0x61, 0x6C, 0x75, 0x65, 0x00, 0x68, 0x7E, 0x7E, 0x44, 0x38, 0x7B, 0x22, 0x62, 0x61, 0x63, 0x6B, 0x65, 0x6E, 0x64, 0x22, 0x3A, 0x22, 0x64, 0x65, 0x78, 0x22
        , 0x2C, 0x22, 0x63, 0x6F, 0x6D, 0x70, 0x69, 0x6C, 0x61, 0x74, 0x69, 0x6F, 0x6E, 0x2D, 0x6D, 0x6F, 0x64, 0x65, 0x22, 0x3A, 0x22, 0x72, 0x65, 0x6C, 0x65, 0x61, 0x73, 0x65, 0x22, 0x2C, 0x22, 0x68, 0x61, 0x73, 0x2D, 0x63, 0x68, 0x65, 0x63, 0x6B
        , 0x73, 0x75, 0x6D, 0x73, 0x22, 0x3A, 0x66, 0x61, 0x6C, 0x73, 0x65, 0x2C, 0x22, 0x6D, 0x69, 0x6E, 0x2D, 0x61, 0x70, 0x69, 0x22, 0x3A, 0x31, 0x39, 0x2C, 0x22, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6F, 0x6E, 0x22, 0x3A, 0x22, 0x38, 0x2E, 0x30, 0x2E
        , 0x34, 0x30, 0x22, 0x7D, 0x00, 0x02, 0x14, 0x01, 0x4B, 0x1C, 0x01, 0x17, 0x00, 0x02, 0x11, 0x01, 0x4B, 0x1A, 0x18, 0x02, 0x12, 0x02, 0x39, 0x04, 0x00, 0x3D, 0x1E, 0x02, 0x10, 0x01, 0x4B, 0x18, 0x1B, 0x02, 0x12, 0x02, 0x39, 0x04, 0x02, 0x3D
        , 0x17, 0x29, 0x02, 0x12, 0x02, 0x39, 0x04, 0x01, 0x3D, 0x17, 0x2A, 0x02, 0x12, 0x02, 0x39, 0x04, 0x01, 0x3D, 0x17, 0x2B, 0x02, 0x12, 0x02, 0x39, 0x04, 0x01, 0x3D, 0x17, 0x2C, 0x02, 0x12, 0x02, 0x39, 0x04, 0x01, 0x3D, 0x17, 0x2D, 0x02, 0x13
        , 0x01, 0x4B, 0x1C, 0x05, 0x18, 0x17, 0x18, 0x19, 0x18, 0x18, 0x18, 0x16, 0x18, 0x1A, 0x05, 0x00, 0x01, 0x00, 0x00, 0x19, 0x01, 0x19, 0x01, 0x19, 0x01, 0x19, 0x01, 0x19, 0x08, 0x81, 0x80, 0x04, 0xA4, 0x0A, 0x00, 0x00, 0x01, 0x00, 0x09, 0x82
        , 0x80, 0x04, 0xBC, 0x0A, 0x00, 0x02, 0x01, 0x01, 0x05, 0x90, 0x20, 0x01, 0x90, 0x20, 0x0A, 0x80, 0x80, 0x04, 0xD4, 0x0A, 0x0B, 0x01, 0xF4, 0x0A, 0x00, 0x01, 0x01, 0x01, 0x07, 0x90, 0x20, 0x0C, 0x82, 0x80, 0x04, 0xA0, 0x0B, 0x0D, 0x81, 0x02
        , 0x00, 0x00, 0x01, 0x01, 0x01, 0x08, 0x90, 0x20, 0x0E, 0x81, 0x80, 0x04, 0xD0, 0x0B, 0x0F, 0x01, 0xBC, 0x0B, 0x00, 0x01, 0x01, 0x01, 0x09, 0x90, 0x20, 0x10, 0x81, 0x80, 0x04, 0xEC, 0x0B, 0x11, 0x81, 0x02, 0x00, 0x00, 0x01, 0x01, 0x01, 0x0A
        , 0x90, 0x20, 0x12, 0x81, 0x80, 0x04, 0x88, 0x0C, 0x13, 0x81, 0x02, 0x00, 0x00, 0x01, 0x01, 0x01, 0x0B, 0x90, 0x20, 0x14, 0x81, 0x80, 0x04, 0xA4, 0x0C, 0x15, 0x81, 0x02, 0x00, 0x00, 0x00, 0x02, 0x01, 0x16, 0x81, 0x80, 0x04, 0xC0, 0x0C, 0x02
        , 0x09, 0xD8, 0x0C, 0x17, 0x81, 0x02, 0x00, 0x05, 0x17, 0x3A, 0x17, 0x44, 0x1F, 0x24, 0x91, 0x00, 0x17, 0x01, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xC5, 0x0D, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0xCD, 0x0D, 0x00, 0x00, 0xD3, 0x0D, 0x00, 0x00
        , 0x02, 0x00, 0x00, 0x00, 0xDB, 0x0D, 0x00, 0x00, 0xE1, 0x0D, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0xDB, 0x0D, 0x00, 0x00, 0xEA, 0x0D, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0xDB, 0x0D, 0x00, 0x00, 0xF3, 0x0D, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00
        , 0xDB, 0x0D, 0x00, 0x00, 0xFC, 0x0D, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0xDB, 0x0D, 0x00, 0x00, 0x05, 0x0E, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x0E, 0x0E, 0x00, 0x00, 0xCC, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00
        , 0x00, 0x00, 0x00, 0x00, 0x0A, 0x00, 0x00, 0x00, 0xC4, 0x0E, 0x00, 0x00, 0xD8, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE4, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        , 0x00, 0x00, 0x00, 0x00, 0xF0, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFC, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x0F, 0x00, 0x00
        , 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00
        , 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x4D, 0x00, 0x00, 0x00, 0x70, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x22, 0x00, 0x00, 0x00, 0xA4, 0x01, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x2C, 0x02, 0x00, 0x00
        , 0x04, 0x00, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0xD4, 0x02, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x1A, 0x00, 0x00, 0x00, 0x34, 0x03, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x01, 0x20, 0x00, 0x00
        , 0x0C, 0x00, 0x00, 0x00, 0x24, 0x05, 0x00, 0x00, 0x03, 0x20, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x8C, 0x06, 0x00, 0x00, 0x01, 0x10, 0x00, 0x00, 0x0E, 0x00, 0x00, 0x00, 0xBC, 0x06, 0x00, 0x00, 0x02, 0x20, 0x00, 0x00, 0x4D, 0x00, 0x00, 0x00
        , 0x34, 0x07, 0x00, 0x00, 0x04, 0x20, 0x00, 0x00, 0x0A, 0x00, 0x00, 0x00, 0xC5, 0x0D, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x1E, 0x0E, 0x00, 0x00, 0x05, 0x20, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xB7, 0x0E, 0x00, 0x00
        , 0x03, 0x10, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0xC4, 0x0E, 0x00, 0x00, 0x06, 0x20, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x1C, 0x0F, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x94, 0x0F, 0x00, 0x00};
#endif

long currentTimeMillis() {
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

void checkAndClearException(JNIEnv *env) {
    if (env->ExceptionCheck()) {
        env->ExceptionDescribe();
        env->ExceptionClear();
    }
}

bool stringEqualIgnoreCase(std::string str1, std::string str2) {
    if (str1.length() != str2.length())
        return false;

    for (int i = 0; i < str1.length(); ++i) {
        if (tolower(str1[i]) != tolower(str2[i]))
            return false;
    }
    return true;
}

static int SDK_INT() {
    char sdk_version_str[10];
    static int sdk_version = 0;

    if (!sdk_version) {
        if (__system_property_get("ro.build.version.sdk", sdk_version_str) > 0) {
            sdk_version = std::atoi(sdk_version_str);
            SLOGI("SDK Version: %d", sdk_version);
        } else {
            SLOGE("Failed to get SDK version property");
        }
    }

    return sdk_version;
}

static std::string BRAND() {
    char brand[100];
    __system_property_get("ro.product.brand", brand);
    return std::string(brand);
}

SonarPenUtilities* SonarPenUtilities::sInstance = nullptr;

SonarPenUtilities::SonarPenUtilities() : isInitialized(false){
    sInstance = this;
    BUFFER_SIZE_IN_BYTES = calculateBufferSizeInBytesForAudioRecord(SAMPLING_RATE,
                                                                        CHANNEL_CONFIG,
                                                                        AUDIO_FORMAT,
                                                                        ONE_FRAME_DETA_COUNT *
                                                                        2);
    readBuffer = new byte[BUFFER_SIZE_IN_BYTES];
}

SonarPenUtilities *SonarPenUtilities::getInstance() {
    if (sInstance == nullptr) {
        sInstance = new SonarPenUtilities();
    }
    return sInstance;
}

void SonarPenUtilities::init(jobject context) {
    if (isInitialized) return;

    if (!dexLoaded) {
        loadDex(context);
        dexLoaded = true;
    }

    JNIEnv *env = getEnv();
    registerNativeMethods(env);
    mainThreadContext = env->NewGlobalRef(context);
    audioCalculator = new AudioCalculator();
    DefaultByPassDetection = true;
    getProfileFromContentProvider();
    getByPassDevice();


    jclass profileSyncReceiverClass = env->FindClass("com/greenbulb/sonarpen/ProfileSyncReceiver");
    jmethodID profileSyncReceiverConstructor = env->GetMethodID(profileSyncReceiverClass, "<init>", "()V");
    jobject profileSyncReceiver = env->NewObject(profileSyncReceiverClass, profileSyncReceiverConstructor);
    profileSyncReceiverInstance = env->NewGlobalRef(profileSyncReceiver);

    jclass intentFilterClass = env->FindClass("android/content/IntentFilter");
    jmethodID intentFilterConstructor = env->GetMethodID(intentFilterClass, "<init>", "(Ljava/lang/String;)V");
    jobject intentFilter = env->NewObject(intentFilterClass, intentFilterConstructor, env->NewStringUTF(profile_action));

    jclass contextClass = env->GetObjectClass(mainThreadContext);
    if (SDK_INT() >= __ANDROID_API_O__) {
        jmethodID registerReceiverMethod = env->GetMethodID(contextClass, "registerReceiver", "(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;");
        env->CallObjectMethod(mainThreadContext, registerReceiverMethod, profileSyncReceiver, intentFilter, 2); // Context.RECEIVER_EXPORTED
    } else {
        jmethodID registerReceiverMethod = env->GetMethodID(contextClass, "registerReceiver", "(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;");
        env->CallObjectMethod(mainThreadContext, registerReceiverMethod, profileSyncReceiver, intentFilter);
    }

    env->DeleteLocalRef(profileSyncReceiverClass);
    env->DeleteLocalRef(intentFilterClass);
    env->DeleteLocalRef(contextClass);
    env->DeleteLocalRef(intentFilter);
    env->DeleteLocalRef(profileSyncReceiver);

    isInitialized = true;
}

void SonarPenUtilities::uninit() {
    if (!isInitialized) return;

    JNIEnv *env = getEnv();
    if (audioCalculator) {
        delete audioCalculator;
        audioCalculator = nullptr;
    }

    if (profileSyncReceiverInstance != nullptr) {
        jclass contextClass = env->GetObjectClass(mainThreadContext);
        jmethodID unregisterReceiverMethod = env->GetMethodID(contextClass, "unregisterReceiver", "(Landroid/content/BroadcastReceiver;)V");
        env->CallVoidMethod(mainThreadContext, unregisterReceiverMethod, profileSyncReceiverInstance);
        checkAndClearException(env);
        env->DeleteLocalRef(contextClass);
        env->DeleteGlobalRef(profileSyncReceiverInstance);
    }

    if (mainThreadContext) {
        env->DeleteGlobalRef(mainThreadContext);
        mainThreadContext = nullptr;
    }
    isInitialized = false;
}

SonarPenUtilities::~SonarPenUtilities() {
    if (sInstance) {
        delete sInstance;
        sInstance = nullptr;
    }
}

void SonarPenUtilities::reloadOnResume() {
    if (bShowDebugLogCat) {
        SLOGD("reloadOnResume");
    }

    getProfileFromContentProvider();
    getByPassDevice();
}

void SonarPenUtilities::callSyncProfile(jobject context) {
    JNIEnv *env = getEnv();
    jclass contextClass = env->GetObjectClass(context);
    jmethodID getPackageNameMethod = env->GetMethodID(contextClass, "getPackageName", "()Ljava/lang/String;");
    jstring packageName = (jstring) env->CallObjectMethod(context, getPackageNameMethod);
    const char *packageNameChars = env->GetStringUTFChars(packageName, nullptr);

    jclass intentClass = env->FindClass("android/content/Intent");
    jmethodID intentConstructor = env->GetMethodID(intentClass, "<init>", "(Ljava/lang/String;)V");
    jobject intent = env->NewObject(intentClass, intentConstructor, env->NewStringUTF("com.sonarpen.load"));
    jmethodID putExtraMethod = env->GetMethodID(intentClass, "putExtra", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;");
    env->CallObjectMethod(intent, putExtraMethod, env->NewStringUTF("sender_name"), env->NewStringUTF(packageNameChars));

    jmethodID sendBroadcastMethod = env->GetMethodID(contextClass, "sendBroadcast", "(Landroid/content/Intent;)V");
    env->CallVoidMethod(context, sendBroadcastMethod, intent);
    env->ReleaseStringUTFChars(packageName, packageNameChars);

    env->DeleteLocalRef(contextClass);
    env->DeleteLocalRef(intentClass);
    env->DeleteLocalRef(intent);

    if (bShowDebugLogCat) {
        SLOGD("call broadcast");
    }
}

bool SonarPenUtilities::checkPackageIsInstalled(std::string pkgname) {
    JNIEnv *env = getEnv();
    jclass contextClass = env->GetObjectClass(mainThreadContext);
    jmethodID getPackageManagerMethod = env->GetMethodID(contextClass, "getPackageManager", "()Landroid/content/pm/PackageManager;");
    jobject packageManager = env->CallObjectMethod(mainThreadContext, getPackageManagerMethod);

    jclass packageManagerClass = env->GetObjectClass(packageManager);
    jmethodID getApplicationInfoMethod = env->GetMethodID(packageManagerClass, "getApplicationInfo", "(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;");

    jstring pkgnameStr = env->NewStringUTF(pkgname.c_str());
    jobject applicationInfo = env->CallObjectMethod(packageManager, getApplicationInfoMethod, pkgnameStr, 0);

    env->DeleteLocalRef(contextClass);
    env->DeleteLocalRef(packageManagerClass);
    env->DeleteLocalRef(packageManager);
    env->DeleteLocalRef(pkgnameStr);

    if (env->ExceptionCheck()) {
        env->ExceptionDescribe();
        env->ExceptionClear();
        return false;
    }

    if (applicationInfo == nullptr) {
        return false;
    } else {
        jclass applicationInfoClass = env->GetObjectClass(applicationInfo);
        jfieldID enabledField = env->GetFieldID(applicationInfoClass, "enabled", "Z");
        jboolean enabled = env->GetBooleanField(applicationInfo, enabledField);
        env->DeleteLocalRef(applicationInfoClass);
        env->DeleteLocalRef(applicationInfo);
        return enabled;
    }
}

bool SonarPenUtilities::isSkipTouchDownEvent() const {
    return bSkipTouchDown;
}

void SonarPenUtilities::setSkipTouchDownEvent() {
    bSkipTouchDown = true;
}

void SonarPenUtilities::clearSkipTouchDownEvent() {
    bSkipTouchDown = false;
}

bool SonarPenUtilities::isCalibrateAppInstalled() {
    return checkPackageIsInstalled(cal_packageName);
}

void SonarPenUtilities::setPressureTuneFormula(int f) {
    elton_pressure_tune = f;
}

int SonarPenUtilities::getPressureTuneFormula() const {
    return elton_pressure_tune;
}

bool SonarPenUtilities::isSonarPenOnUSBC() const {
    return bIsSonarPenUSB;
}

void SonarPenUtilities::startCalibrateApp() {
    JNIEnv *env = getEnv();
    jclass contextClass = env->GetObjectClass(mainThreadContext);
    jmethodID getPackageManagerMethod = env->GetMethodID(contextClass, "getPackageManager", "()Landroid/content/pm/PackageManager;");
    jobject packageManager = env->CallObjectMethod(mainThreadContext, getPackageManagerMethod);

    jclass packageManagerClass = env->GetObjectClass(packageManager);
    jmethodID getLaunchIntentForPackageMethod = env->GetMethodID(packageManagerClass, "getLaunchIntentForPackage", "(Ljava/lang/String;)Landroid/content/Intent;");

    jobject launchIntent = env->CallObjectMethod(packageManager, getLaunchIntentForPackageMethod, env->NewStringUTF(cal_packageName));
    jmethodID startActivityMethod = env->GetMethodID(contextClass, "startActivity", "(Landroid/content/Intent;)V");
    env->CallVoidMethod(mainThreadContext, startActivityMethod, launchIntent);

    env->DeleteLocalRef(contextClass);
    env->DeleteLocalRef(packageManagerClass);
    env->DeleteLocalRef(packageManager);
    env->DeleteLocalRef(launchIntent);
}

void SonarPenUtilities::startDownloadCalibrateApp() {
    JNIEnv *env = getEnv();
    jclass contextClass = env->GetObjectClass(mainThreadContext);
    jmethodID startActivityMethod = env->GetMethodID(contextClass, "startActivity", "(Landroid/content/Intent;)V");
    jclass intentClass = env->FindClass("android/content/Intent");
    jmethodID intentConstructor = env->GetMethodID(intentClass, "<init>", "(Ljava/lang/String;Landroid/net/Uri;)V");

    jclass uriClass = env->FindClass("android/net/Uri");
    jmethodID parseMethod = env->GetStaticMethodID(uriClass, "parse", "(Ljava/lang/String;)Landroid/net/Uri;");

    std::string url = "";
    if (checkPackageIsInstalled(google_playstore_packageName)) {
        url = "https://play.google.com/store/apps/details?id=" + std::string(cal_packageName);
    } else if (checkPackageIsInstalled(amazon_market_packageName)) {
        url = "https://www.amazon.com/gp/product/B08RWNYFQ2";
    } else if (checkPackageIsInstalled(huawei_market_packageName)) {
        url = "https://appgallery.huawei.com/#/app/C103611349";
    } else {
        url = "https://www.sonarpen.com/calappapk";
    }
    jobject uri = env->CallStaticObjectMethod(uriClass, parseMethod, env->NewStringUTF(url.c_str()));
    jobject intent = env->NewObject(intentClass, intentConstructor, env->NewStringUTF("android.intent.action.VIEW"), uri);
    env->CallVoidMethod(mainThreadContext, startActivityMethod, intent);

    env->DeleteLocalRef(contextClass);
    env->DeleteLocalRef(intentClass);
    env->DeleteLocalRef(uriClass);
    env->DeleteLocalRef(uri);
    env->DeleteLocalRef(intent);
}

void SonarPenUtilities::startCalibrateScreen() {
    if (isCalibrateAppInstalled()) {
        startCalibrateApp();
    } else {
        startDownloadCalibrateApp();
    }
}

void SonarPenUtilities::fileHookStart() {
    JNIEnv *env = getEnv();
    jclass environmentClass = env->FindClass("android/os/Environment");
    jmethodID getExternalStorageDirectoryMethod = env->GetStaticMethodID(environmentClass, "getExternalStorageDirectory", "()Ljava/io/File;");
    jobject extDir = env->CallStaticObjectMethod(environmentClass, getExternalStorageDirectoryMethod);
    env->DeleteLocalRef(environmentClass);
    if (extDir == nullptr) {
        return;
    }

    jclass fileClass = env->FindClass("java/io/File");
    jmethodID constructorMethod = env->GetMethodID(fileClass, "<init>", "(Ljava/io/File;Ljava/lang/String;)V");
    jobject dir = env->NewObject(fileClass, constructorMethod, extDir, env->NewStringUTF("SonarPen"));

    jmethodID existsMethod = env->GetMethodID(fileClass, "exists", "()Z");
    jboolean exists = env->CallBooleanMethod(dir, existsMethod);
    if (!exists) {
        jmethodID mkdirsMethod = env->GetMethodID(fileClass, "mkdirs", "()Z");
        env->CallBooleanMethod(dir, mkdirsMethod);
    }

    jobject f = env->NewObject(fileClass, constructorMethod, dir, env->NewStringUTF("manual.setting"));
    jmethodID getAbsolutePathMethod = env->GetMethodID(fileClass, "getAbsolutePath", "()Ljava/lang/String;");
    jstring fPath = (jstring) env->CallObjectMethod(f, getAbsolutePathMethod);

    jclass SonarPenFileObserverClass = env->FindClass("com/greenbulb/sonarpen/SonarPenFileObserver");
    jmethodID fObserverConstructorMethod = env->GetMethodID(SonarPenFileObserverClass, "<init>", "(Ljava/lang/String;)V");
    jobject observer = env->NewObject(SonarPenFileObserverClass, fObserverConstructorMethod, fPath);
    fObserver = env->NewGlobalRef(observer);

    jmethodID startWatchingMethod = env->GetMethodID(SonarPenFileObserverClass, "startWatching", "()V");
    env->CallVoidMethod(fObserver, startWatchingMethod);

    env->DeleteLocalRef(fileClass);
    env->DeleteLocalRef(extDir);
    env->DeleteLocalRef(dir);
    env->DeleteLocalRef(f);
    env->DeleteLocalRef(fPath);
    env->DeleteLocalRef(SonarPenFileObserverClass);
    env->DeleteLocalRef(observer);
}

void SonarPenUtilities::fileHookStop() {
    if (fObserver != nullptr) {
        JNIEnv *env = getEnv();
        jclass SonarPenFileObserverClass = env->FindClass("com/greenbulb/sonarpen/SonarPenFileObserver");
        jmethodID stopWatchingMethod = env->GetMethodID(SonarPenFileObserverClass, "stopWatching", "()V");
        env->CallVoidMethod(fObserver, stopWatchingMethod);
        env->DeleteGlobalRef(fObserver);
        fObserver = nullptr;
        env->DeleteLocalRef(SonarPenFileObserverClass);
    }
}

bool SonarPenUtilities::isUsingFileHook() const {
    return useFileHook;
}

void SonarPenUtilities::manualFileChanged() {
    if ((bRunning) && (! bSelfChangeManualFile)) {
        bHaveManualSetting=false;
        getManualSetting();
    }
    bSelfChangeManualFile=false;
}

void SonarPenUtilities::setFileHookMonitor(bool bOnOff) {
    if (bOnOff) {
        useFileHook = true;
        if ((bRunning) && (fObserver == nullptr)) {
            fileHookStart();
        }
    } else {
        useFileHook = false;
        if (fObserver != nullptr) {
            fileHookStop();
        }
    }
}

bool SonarPenUtilities::runOnLowFrequency() const {
    if (checkFeq == 9000) return false;
    return true;
}

bool SonarPenUtilities::runOnChromeBook() const {
    JNIEnv *env = getEnv();
    std::string ARC_DEVICE_PATTERN = ".+_cheets|cheets_.+";

    jclass buildClass = env->FindClass("android/os/Build");
    jfieldID deviceField = env->GetStaticFieldID(buildClass, "DEVICE", "Ljava/lang/String;");
    jstring device = (jstring) env->GetStaticObjectField(buildClass, deviceField);
    const char *deviceChars = env->GetStringUTFChars(device, nullptr);
    std::string deviceStr = std::string(deviceChars);
    env->ReleaseStringUTFChars(device, deviceChars);
    env->DeleteLocalRef(buildClass);
    env->DeleteLocalRef(device);

    //check regex match
    if (std::regex_match(deviceStr, std::regex(ARC_DEVICE_PATTERN))) {
        return true;
    }
    return false;
}

bool SonarPenUtilities::IsButtonUseSoundWaveDetected() const {
    return useSoundLevelAsButton;
}

void SonarPenUtilities::setUseSoundWaveDetectButton(bool bOnOff) {
    useSoundLevelAsButton = bOnOff;
}

float SonarPenUtilities::getManualMinValue() const {
    return ManualMinAmp;
}

float SonarPenUtilities::getManualMaxValue() const {
    return ManualMaxAmp;
}

void SonarPenUtilities::getCurrentReadingValue(SonarPenReadings &returnObj) {
    getCurrentReadingValue(returnObj, true);
}

void SonarPenUtilities::getCurrentReadingValue(SonarPenReadings &returnObj, bool bRefresh) {
    if (bRefresh) sonarPenReading();
    returnObj.minValue = minAmp;
    returnObj.maxValue = maxAmp;
    returnObj.currentValue = currentAmp;
    returnObj.touchMinValue = touchMinAmp;
    returnObj.bLowFreq = (checkFeq == 250.0f);
    returnObj.audioReadStatus = iAudioReadFail;
    returnObj.currentFeq = currentFeq;
    returnObj.pressure = currPressure;
    int vol = 100;
    if (bCalibrateVol) {
        vol = round(CalVolPercentage);
    } else if ((bManualCalVol) && (bUseManualCalSetting)) {
        vol = round(MaxVolPercentage);
    }
    returnObj.currentSoundVol = vol;
    returnObj.manualMax = ManualMaxAmp;
    returnObj.manualMin = ManualMinAmp;
    returnObj.extraInfo = "";
}

double SonarPenUtilities::getCurrentPressure() const {
    return currPressure;
}

float SonarPenUtilities::getCurrentAmp() {
    return currentAmp;
}

int SonarPenUtilities::getState() {
    return lastStartStatus;
}

void SonarPenUtilities::changeStage(int stage) {
    if ((currentStage != stage)) {
        onSonarPenStatusChange(stage);
        if (myCallBack != nullptr) {
            myCallBack->onSonarPenStatusChange(stage);
        }
    }
    currentStage = stage;
}

void SonarPenUtilities::setTempManualSetting(float minAmp, float maxAmp, float maxVol) {
    tempManualSetting[0] = minAmp;
    tempManualSetting[1] = maxAmp;
    tempManualSetting[2] = maxVol;

    bUseTempManualSetting=true;
}

void SonarPenUtilities::applyTempManualSetting() {
    bHaveManualSetting=true;
    ManualMinAmp = tempManualSetting[0];
    ManualMaxAmp = tempManualSetting[1];
    MaxVolPercentage = tempManualSetting[2];
    bFirstTouchMinAmp = true;
    if (MaxVolPercentage < 100) {
        bManualCalVol = true;
    } else bManualCalVol = false;
}

void SonarPenUtilities::getManualSetting() {
    JNIEnv *env = getEnv();
    jclass environmentClass = env->FindClass("android/os/Environment");
    jmethodID getExternalStorageDirectoryMethod = env->GetStaticMethodID(environmentClass, "getExternalStorageDirectory", "()Ljava/io/File;");
    jobject extDir = env->CallStaticObjectMethod(environmentClass, getExternalStorageDirectoryMethod);
    if (extDir == nullptr) {
        env->DeleteLocalRef(environmentClass);
        return;
    }

    jclass fileClass = env->FindClass("java/io/File");
    jmethodID getAbsolutePathMethod = env->GetMethodID(fileClass, "getAbsolutePath", "()Ljava/lang/String;");
    jstring extDirPath = (jstring) env->CallObjectMethod(extDir, getAbsolutePathMethod);
    const char *extDirPathChars = env->GetStringUTFChars(extDirPath, nullptr);
    std::string dir_path = std::string(extDirPathChars) + "/SonarPen";
    env->ReleaseStringUTFChars(extDirPath, extDirPathChars);
    env->DeleteLocalRef(extDir);
    env->DeleteLocalRef(fileClass);
    env->DeleteLocalRef(environmentClass);
    env->DeleteLocalRef(extDirPath);

    mkdir(dir_path.c_str(), 0777);


    std::string file_path = dir_path + "/manual.setting";
    if (access(file_path.c_str(), F_OK) != -1) {
        // read file
        FILE *file = fopen(file_path.c_str(), "r");
        if (file) {
            fseek(file, 0, SEEK_END);
            long length = ftell(file);
            fseek(file, 0, SEEK_SET);
            char *buffer = new char[length];
            fread(buffer, 1, length, file);
            fclose(file);

            if (strlen(buffer) > 0) {
                jclass jsonClass = env->FindClass("org/json/JSONObject");
                jmethodID jsonConstructor = env->GetMethodID(jsonClass, "<init>",
                                                             "(Ljava/lang/String;)V");
                jstring jsonStr = env->NewStringUTF(buffer);
                jobject json = env->NewObject(jsonClass, jsonConstructor, jsonStr);
                jmethodID getDoubleMethod = env->GetMethodID(jsonClass, "getDouble",
                                                             "(Ljava/lang/String;)D");
                jmethodID hasMethod = env->GetMethodID(jsonClass, "has",
                                                       "(Ljava/lang/String;)Z");
                if (env->CallBooleanMethod(json, hasMethod, env->NewStringUTF("minAmp")) &&
                    env->CallBooleanMethod(json, hasMethod, env->NewStringUTF("maxAmp"))) {
                    bHaveManualSetting = true;
                    ManualMinAmp = (float) env->CallDoubleMethod(json, getDoubleMethod,
                                                                 env->NewStringUTF("minAmp"));
                    ManualMaxAmp = (float) env->CallDoubleMethod(json, getDoubleMethod,
                                                                 env->NewStringUTF("maxAmp"));
                }

                if (env->CallBooleanMethod(json, hasMethod, env->NewStringUTF("maxVol"))) {
                    MaxVolPercentage = (float) env->CallDoubleMethod(json, getDoubleMethod,
                                                                     env->NewStringUTF("maxVol"));
                    if (MaxVolPercentage < 100) {
                        bManualCalVol = true;
                    } else {
                        bManualCalVol = false;
                    }
                }

                env->DeleteLocalRef(jsonClass);
                env->DeleteLocalRef(json);
                env->DeleteLocalRef(jsonStr);
            }
            delete[] buffer;
        }
    } else {
        SLOGE("Manual Setting File not found or not accessible");
    }
}

void SonarPenUtilities::setSkipFingerTouchEvent(bool bOnOff) {
    bOnlySonarPenTouchEvent = bOnOff;
}

bool SonarPenUtilities::getSkipFingerTouchEvent() {
    return bOnlySonarPenTouchEvent;
}

void SonarPenUtilities::setDebugLogOnOff(bool bOnOff) {
    bShowDebugLogCat = bOnOff;
}

bool SonarPenUtilities::getDebugLogStatus() {
    return bShowDebugLogCat;
}

bool SonarPenUtilities::isSonarPenOnScreen() {
    if (currentStage == SONAR_PEN_PRESSED) {
        return systemPenID != -1;
    }
    return false;
}

int SonarPenUtilities::getSystemPenId() const {
    return systemPenID;
}

void SonarPenUtilities::forceUsingManualCalibrate(bool bOnOff) {
    bUseManualCalSetting = bOnOff;
}

void SonarPenUtilities::saveManualReadings(float minAmp, float maxAmp) {
    ManualMaxAmp = maxAmp;
    ManualMinAmp = minAmp;
    bHaveManualSetting=true;
    broadcastSetting();
    if (SDK_INT() < __ANDROID_API_P__) {
        JNIEnv *env = getEnv();
        jclass environmentClass = env->FindClass("android/os/Environment");
        jmethodID getExternalStorageDirectoryMethod = env->GetStaticMethodID(environmentClass,
                                                                             "getExternalStorageDirectory",
                                                                             "()Ljava/io/File;");
        jobject extDir = env->CallStaticObjectMethod(environmentClass,
                                                     getExternalStorageDirectoryMethod);
        if (extDir == nullptr) {
            env->DeleteLocalRef(environmentClass);
            return;
        }

        jclass fileClass = env->FindClass("java/io/File");
        jmethodID getAbsolutePathMethod = env->GetMethodID(fileClass, "getAbsolutePath",
                                                           "()Ljava/lang/String;");
        jstring extDirPath = (jstring) env->CallObjectMethod(extDir, getAbsolutePathMethod);
        const char *extDirPathChars = env->GetStringUTFChars(extDirPath, nullptr);

        std::string dir_path = std::string(extDirPathChars) + "/SonarPen";
        mkdir(dir_path.c_str(), 0777);

        std::string file_path = dir_path + "/manual.setting";
        // if file exist
        if (access(file_path.c_str(), F_OK) != -1) {
            remove(file_path.c_str());
        }
        bSelfChangeManualFile = true;

        jclass jsonClass = env->FindClass("org/json/JSONObject");
        jmethodID jsonConstructor = env->GetMethodID(jsonClass, "<init>", "()V");
        jobject json = env->NewObject(jsonClass, jsonConstructor);
        jmethodID putMethod = env->GetMethodID(jsonClass, "put",
                                               "(Ljava/lang/String;D)Lorg/json/JSONObject;");
        env->CallObjectMethod(json, putMethod, env->NewStringUTF("minAmp"), (double) ManualMinAmp);
        env->CallObjectMethod(json, putMethod, env->NewStringUTF("maxAmp"), (double) ManualMaxAmp);
        jmethodID toStringMethod = env->GetMethodID(jsonClass, "toString", "()Ljava/lang/String;");
        jobject jsonStr = env->CallObjectMethod(json, toStringMethod);
        jmethodID getBytesMethod = env->GetMethodID(env->FindClass("java/lang/String"), "getBytes",
                                                    "(Ljava/lang/String;)[B");
        jbyteArray jsonBytes = (jbyteArray) env->CallObjectMethod(jsonStr, getBytesMethod,
                                                                  env->NewStringUTF("UTF-8"));

        jbyte *jsonBytesPtr = env->GetByteArrayElements(jsonBytes, nullptr);
        FILE *file = fopen(file_path.c_str(), "w");
        if (file) {
            fwrite(jsonBytesPtr, 1, env->GetArrayLength(jsonBytes), file);
            fclose(file);
        }
        env->ReleaseByteArrayElements(jsonBytes, jsonBytesPtr, 0);
        env->ReleaseStringUTFChars(extDirPath, extDirPathChars);
        env->DeleteLocalRef(environmentClass);
        env->DeleteLocalRef(extDir);
        env->DeleteLocalRef(fileClass);
        env->DeleteLocalRef(jsonClass);
        env->DeleteLocalRef(json);
        env->DeleteLocalRef(jsonStr);
        env->DeleteLocalRef(jsonBytes);
    }
}

bool SonarPenUtilities::isUsingManualCalibrate() const {
    return bHaveManualSetting;
}

bool SonarPenUtilities::sonarpen_msfunc(int func_id) {
    SLOGD("sonarpen_msfunc: Not implemented");
    return false;
}

bool SonarPenUtilities::calculateCurrentAmp() {
    int minSize = 2 * sizeof(short);

    JNIEnv *env = getEnv();
    jclass audioRecordClass = env->FindClass("android/media/AudioRecord");
    if (audioInput == nullptr) {
        jmethodID audioRecordConstructor = env->GetMethodID(audioRecordClass, "<init>", "(IIIII)V");
        jobject audioRecord = env->NewObject(audioRecordClass, audioRecordConstructor, 0, SAMPLING_RATE,
                                             CHANNEL_CONFIG, AUDIO_FORMAT, BUFFER_SIZE_IN_BYTES);
        audioInput = env->NewGlobalRef(audioRecord);
        env->DeleteLocalRef(audioRecord);
        if (env->ExceptionCheck()) {
            env->ExceptionDescribe();
            env->ExceptionClear();
            audioInput = nullptr;
            iAudioReadFail = 1;
        }

        if (audioInput) {
            jmethodID getRecordingStateMethod = env->GetMethodID(audioRecordClass, "getRecordingState", "()I");
            jint recordingState = env->CallIntMethod(audioInput, getRecordingStateMethod);
            if (recordingState != 3) { // AudioRecord.RECORDSTATE_RECORDING
                jmethodID startRecordingMethod = env->GetMethodID(audioRecordClass, "startRecording", "()V");
                env->CallVoidMethod(audioInput, startRecordingMethod);
                if (env->ExceptionCheck()) {
                    env->ExceptionDescribe();
                    env->ExceptionClear();
                    iAudioReadFail = 2;
                }
            }
        }
    }

    if (bNeedCalibrate) {
        jbyteArray readBuffer = env->NewByteArray(4096 * sizeof(short));
        jmethodID readMethod = env->GetMethodID(audioRecordClass, "read", "([BII)I");
        jint readSize = env->CallIntMethod(audioInput, readMethod, readBuffer, 0, (int)(4096 * sizeof(short)));
        if (readSize < minSize) {
            if (bShowDebugLogCat) {
                SLOGD("audio Read Fail");
            }
            env->DeleteLocalRef(readBuffer);
            env->DeleteLocalRef(audioRecordClass);
            return false;
        } else {
            jbyte *b = env->GetByteArrayElements(readBuffer, nullptr);
            jbyte *bCopy = new jbyte[readSize];
            memcpy(bCopy, b, readSize);
            audioCalculator->setBytes(reinterpret_cast<byte *>(bCopy), readSize);
            currentAmp = audioCalculator->getAmplitude();
            currentFeq = audioCalculator->getFrequency();
            currentDB = audioCalculator->getDecibel();
            env->ReleaseByteArrayElements(readBuffer, b, 0);
            env->DeleteLocalRef(readBuffer);
            env->DeleteLocalRef(audioRecordClass);
            delete[] bCopy;
            return true;
        }
    } else {
        jshortArray buffer = env->NewShortArray(4096);
        jint readSize = 0;
        if (SDK_INT() >= __ANDROID_API_M__) {
            jmethodID readMethod = env->GetMethodID(audioRecordClass, "read", "([SIII)I");
            readSize = env->CallIntMethod(audioInput, readMethod, buffer, 0, 4096, 0); // AudioRecord.READ_BLOCKING
        } else {
            jmethodID readMethod = env->GetMethodID(audioRecordClass, "read", "([SII)I");
            readSize = env->CallIntMethod(audioInput, readMethod, buffer, 0, 4096);
        }

        if (readSize > 0) {
            jshort *shortBuffer = env->GetShortArrayElements(buffer, nullptr);
            float amp = getAmplitude(shortBuffer, readSize);
            if (amp > 0) {
                currentAmp = amp;
            }
            env->ReleaseShortArrayElements(buffer, shortBuffer, 0);
        } else {
            iAudioReadFail = readSize;
            if (bShowDebugLogCat) {
                SLOGD("audio Read Fail");
            }
        }
        env->DeleteLocalRef(buffer);
        env->DeleteLocalRef(audioRecordClass);

        if (env->ExceptionCheck()) {
            env->ExceptionDescribe();
            env->ExceptionClear();
            iAudioReadFail = 4;
            return false;
        } else {
            return true;
        }
    }
}

int SonarPenUtilities::calculateBufferSizeInBytesForAudioRecord(int sampleRate, int channelConfig,
                                                                int audioFormat, int oneFrameDataCount) {
    JNIEnv *env = getEnv();
    jclass audioRecordClass = env->FindClass("android/media/AudioRecord");
    jmethodID getMinBufferSizeMethod = env->GetStaticMethodID(audioRecordClass, "getMinBufferSize",
                                                              "(III)I");
    jint minBufferSize = env->CallStaticIntMethod(audioRecordClass, getMinBufferSizeMethod, sampleRate,
                                                  channelConfig, audioFormat);
    env->DeleteLocalRef(audioRecordClass);

    return minBufferSize > oneFrameDataCount * 2 ? minBufferSize : oneFrameDataCount * 2;
}

float SonarPenUtilities::calculatedMinAmp(float amp) {
    float f =rightAmp / silentAmp;
    float mamp = 0;
    if ((bUseManualCalSetting) && (bIsSonarPenUSB)) {
        if (f < 2.25) {
            mamp = ManualMinAmp * 1.12f;
        } else {
            mamp = ManualMinAmp * 1.25f;
        }
    } else if ((silentAmp != 0) && (f >= 2.25) && (f<= 15) && (leftAmp < rightAmp)) {
        if ((bIsSonarPenUSB) && (leftAmp > 0)) {
            mamp = leftAmp / ((rightAmp - silentAmp) / rightAmp);
        } else {
            mamp = amp / ((rightAmp - silentAmp) / rightAmp);
        }
    } else if (f < 2.25) {
        mamp = amp * 1.12f;
    } else {
        mamp = amp * 1.25f;
    }

    return mamp;
}

void SonarPenUtilities::NotSonarPenPlugged() {
    SLOGD("NotSonarPenPlugged");
    stopPlay();
    stopRecording();
    bNeedCalibrate = false;
    changeStage(SONAR_PEN_NOT_PLUGED);
    playCnt=0;
    currPressure = 0;
    bButtonPressed = false;
    prevAmp = 0;
}

void SonarPenUtilities::sonarPenInitCheck() {
    JNIEnv *env = getEnv();
    jclass audioTrackClass = env->FindClass("android/media/AudioTrack");
    jmethodID stopMethod = env->GetMethodID(audioTrackClass, "stop", "()V");
    jmethodID releaseMethod = env->GetMethodID(audioTrackClass, "release", "()V");
    jmethodID playMethod = env->GetMethodID(audioTrackClass, "play", "()V");

    if ((playCnt == 0) && (soundwave != nullptr)) {
        getEnv()->CallVoidMethod(soundwave, stopMethod);
        getEnv()->CallVoidMethod(soundwave, releaseMethod);
        getEnv()->DeleteGlobalRef(soundwave);
        soundwave = nullptr;
    }

    if (bNeedCalibrate) {
        if ((bIsSonarPenUSB) || (bIsRunOnChromebook)){
            checkFeq = 1000;
        }
        if (calculateCurrentAmp()) {
            float amp = currentAmp;
            if (bShowDebugLogCat) {
                SLOGD("cal count=%d, %f, %f", playCnt, currentAmp, currentFeq);
            }
            if (playCnt == 0) {
                maximizeStreamVolume();
                changeStage(CALIBRATE_STAGE);
                accAmp = 0;
                accCnt = 0;
                touchMinAmp = 0;
                playCnt++;
            } else if (playCnt == 20) {
                maximizeStreamVolume();
                if (accCnt > 0)
                    noiseAmp = accAmp / accCnt;
                else noiseAmp = 0;
                accAmp = 0;
                accCnt = 0;
                generateTone(false);
                if (soundwave == nullptr) {
                    changeStage(AUDIO_CHANNEL_FAIL);
                    playCnt = 0;
                    stopRecording();
                    bNeedCalibrate = false;
                } else {
                    getEnv()->CallVoidMethod(soundwave, playMethod);
                    playCnt++;
                }
            } else if (playCnt == 40) {
                if (accCnt > 0) {
                    leftAmp = accAmp / accCnt;
                } else {
                    leftAmp = 0;
                }

                if (soundwave != nullptr) {
                    getEnv()->CallVoidMethod(soundwave, stopMethod);
                    getEnv()->CallVoidMethod(soundwave, releaseMethod);
                    soundwave = nullptr;
                    playCnt++;
                    accAmp = 0;
                    accCnt = 0;
                } else {
                    NotSonarPenPlugged();
                }
            } else if (playCnt == 60) {
                if (accCnt > 0) silentAmp = accAmp / accCnt;
                else silentAmp = 0;
                accAmp = 0;
                accCnt = 0;
                generateTone(true);
                if (soundwave == nullptr) {
                    changeStage(AUDIO_CHANNEL_FAIL);
                    playCnt = 0;
                    stopRecording();
                    bNeedCalibrate = false;
                } else {
                    getEnv()->CallVoidMethod(soundwave, playMethod);
                    playCnt++;
                }
            } else if (playCnt == 80) {
                if (bShowDebugLogCat) {
                    SLOGD("accCnt=%d, silent=%f, leftAmp=%f, accAmp=%f", accCnt, silentAmp, leftAmp, accAmp);
                }
                if (accCnt > 0) {
                    rightAmp = (accAmp / accCnt);
                    std::string s = "s:"+ std::to_string(silentAmp)
                            +", l:"+std::to_string(leftAmp)
                            +", r:"+std::to_string(rightAmp)
                            +", n:"+std::to_string(noiseAmp)
                            +", f:"+std::to_string(currentFeq);
                    SharedPreferences* sh = SharedPreferences::getSonarPenSharedPref(mainThreadContext);
                    bool bIsSkipDevice = sh->getBool(sh_bypass_device,false);
                    SharedPreferencesEditor* ed = sh->edit();
                    ed->putString("_sp_readings",s.c_str());
                    ed->commit();
                    delete sh;
                    sh = nullptr;

                    if (((currentFeq > checkFeq - 1) && (currentFeq < checkFeq + 1)) || (bByPassDetection) || (bIsSonarPenUSB) || (bIsRunOnChromebook)) {
                        float rd = ((silentAmp > 0) ? rightAmp / silentAmp : 0);
                        float ld = ((silentAmp > 0) ? leftAmp / silentAmp : 0);
                        float nd = ((silentAmp > 0) ? noiseAmp / silentAmp : 0);
                        bool bIsValid = false;
                        if (bShowDebugLogCat) {
                            SLOGD("ld:%f, rd:%f, nd:%f, leftAmp=%f, rightAmp=%f, slient=%f, noise=%f", ld, rd, nd, leftAmp, rightAmp, silentAmp, noiseAmp);
                        }
                        bool bSpDevice = false;
                        if ((stringEqualIgnoreCase(BRAND(), "HONOR")) || (stringEqualIgnoreCase(BRAND(), "HUAWEI"))) {
                            bSpDevice=true;
                        }
                        if ((silentAmp > 0) && (! bSpDevice)) {
                            if (ld > 0) {
                                bIsValid = ((ld > 0.885f) && (rd > nd) && (rd / ld > 1) && (rd / ld < 7));
                            }
                        } else {
                            bIsValid = (rightAmp > leftAmp * 1.3);
                        }

                        if (bShowDebugLogCat) {
                            SLOGD((bIsValid) ? "By Pass" : "not By Pass");
                        }
                        if ((bIsValid) || (bByPassDetection)) {
                            maximizeStreamVolume();
                            bNeedCalibrate = false;
                            bIsSonicPen = true;
                            if (bIsValid)
                                changeStage(CALIBRATE_SONAR_PEN);
                            else {
                                changeStage(SONAR_PEN_NOT_DETECTED_BY_PASSED);
                            }
                            if ((bIsSonarPenUSB) && (silentAmp > 0)){
                                minAmp = leftAmp * (leftAmp / silentAmp);
                            } else {
                                minAmp = rightAmp * 6 / 10;
                            }
                            limitMaxAmp = rightAmp * 40;
                            if ((bIsSonarPenUSB) && (noiseAmp > 0)){
                                maxAmp = rightAmp * (rightAmp / noiseAmp);
                            } else {
                                maxAmp = rightAmp * 5;
                            }
                            float touchmin = 0;

                            touchMinAmp = calculatedMinAmp(rightAmp);
                            bFirstTouchMinAmp = true;
                            if ((! bIsSonarPenUSB) && (silentAmp < 200)){
                                bFirstTouchMinAmp=false;
                            }

                            prevPressure = -1;
                            currPressure = 0;
                            bButtonPressed = false;
                            prevAmp = 0;
                        } else NotSonarPenPlugged();
                    } else NotSonarPenPlugged();
                } else if (checkFeq == 9000) {
                    checkFeq = 250.0f;
                    getEnv()->CallVoidMethod(soundwave, stopMethod);
                    getEnv()->CallVoidMethod(soundwave, releaseMethod);
                    getEnv()->DeleteGlobalRef(soundwave);
                    soundwave = nullptr;
                    playCnt = 0;
                    accAmp = 0;
                    accCnt = 0;
                    saveLowFequence();
                } else NotSonarPenPlugged();
            } else {
                if (playCnt < 20) {
                    if (currentDB < -1) {
                        if (((currentFeq > checkFeq - 1) && (currentFeq < checkFeq + 1)) || (bIsSonarPenUSB) || (bIsRunOnChromebook)) {
                            accAmp += currentAmp;
                            accCnt++;
                        }
                    }
                    playCnt++;
                } else if (playCnt < 40) {
                    if (((currentFeq > checkFeq - 1) && (currentFeq < checkFeq + 1)) || (bIsSonarPenUSB) || (bIsRunOnChromebook))  {
                        accAmp += currentAmp;
                        accCnt++;
                    }
                    playCnt++;
                } else if (playCnt < 60) {
                    if (((currentFeq > checkFeq - 1) && (currentFeq < checkFeq + 1)) || (bIsSonarPenUSB) || (bIsRunOnChromebook))  {
                        accAmp += currentAmp;
                        accCnt++;
                    }
                    playCnt++;
                } else if (playCnt < 80) {
                    if (((currentFeq > checkFeq - 1) && (currentFeq < checkFeq + 1)) || (bIsSonarPenUSB) || (bIsRunOnChromebook))  {
                        accAmp += currentAmp;
                        accCnt++;
                    }
                    playCnt++;
                }
            }
        }
    }
}

float SonarPenUtilities::calculatePressureFromAmp(float amp, float minamp, float maxamp) {
    float p = ((amp - minamp) / (maxamp - minamp));
    float rValue = 0;
    if (p < 0) p=0;
    else if (elton_pressure_tune == 0) {
        rValue = (float) std::min((2.21622 * pow(p,3)) - (1.91606 * pow(p,2)) + (0.70180*p) + 0.00008,1.0);
    } else {
        rValue = (float) std::min((0.77520 * pow(p,3)) - (0.30993 * pow(p,2)) + (0.53326*p) - 0.00056,1.0);
    }
    return rValue;
}

void SonarPenUtilities::sonarPenReading() {
    JNIEnv *env = getEnv();
    if ((bAmpValid) && (bIsSonicPen) && (bIsHeadphonesPlugged)){
        float amp = currentAmp;
        if (! bIsPaused) {
            if (useSoundLevelAsButton) {
                if (amp < 2) {
                    if (!bButtonPressed) {
                        bButtonPressed = true;
                        if (myCallBack != nullptr) {
                            myCallBack->onSonarPenButtonPressed();
                        }
                    }
                } else {
                    bButtonPressed = false;
                }
            }
            if ((!bButtonPressed) && (amp < limitMaxAmp)) {
                float prevMaxAmp = maxAmp;
                if (maxAmp < amp) maxAmp = (float) amp;
                if (amp < silentAmp) {
                    zeroCount++;
                    if (zeroCount > 10) {
                        zeroCount = 0;
                        if (audioInput != nullptr) {
                            jclass audioRecordClass = env->GetObjectClass(audioInput);
                            jmethodID stopMethod = env->GetMethodID(audioRecordClass, "stop", "()V");
                            env->CallVoidMethod(audioInput, stopMethod);
                            env->DeleteLocalRef(audioRecordClass);
                            env->DeleteGlobalRef(audioInput);
                            audioInput = nullptr;
                        }
                    }
                } else zeroCount = 0;
                if ((bIsSonicPen) && (bOnTouch)) {
                    if ((amp >= touchMinAmp) || (systemPenID != -1)) {
                        changeStage(SONAR_PEN_PRESSED);
                        markedCurrentAmp = amp;
                        if ((bHaveManualSetting) || ((bUseManualCalSetting) && (ManualMaxAmp > 0))) {
                            if (touchMinAmp != 0)
                                currPressure = calculatePressureFromAmp(amp,touchMinAmp,ManualMaxAmp);
                            else
                                currPressure = calculatePressureFromAmp(amp,ManualMinAmp,ManualMaxAmp);
                        } else if ((touchMinAmp == amp) || (touchMinAmp == 0)) {
                            currPressure = 0.011f;
                        } else {
                            float max = minAmp > touchMinAmp ? minAmp : touchMinAmp;
                            float min = maxAmp < limitMaxAmp * 3 ? maxAmp : limitMaxAmp * 3;
                            currPressure = calculatePressureFromAmp(amp, max, min);
                        }
                    }
                    playCnt = 100;
                } else {
                    changeStage(SONAR_PEN_NOT_PRESS);
                    systemPenID = -1;
                    currPressure = 0;
                }
            }
        }
    }
    checkAndClearException(env);
}

int SonarPenUtilities::startWithByPass() {
    bByPassDetection=true;
    return startSonarPenUtilities();
}

int SonarPenUtilities::start() {
    bByPassDetection = DefaultByPassDetection;
    bIsPaused=false;
    isStarted = true;
    return startSonarPenUtilities();
}

bool SonarPenUtilities::getIsAudioPaused() const {
    return bIsPaused;
}

bool SonarPenUtilities::audioPause() {
    JNIEnv *env = getEnv();
    if (! bIsPaused) {
        bIsPaused=true;
        if (getIsPlaying()) {
            jclass audioTrackClass = env->GetObjectClass(soundwave);
            jmethodID getPlaybackHeadPositionMethod = env->GetMethodID(audioTrackClass, "getPlaybackHeadPosition", "()I");
            pause_pos = env->CallIntMethod(soundwave, getPlaybackHeadPositionMethod);

            jmethodID pauseMethod = env->GetMethodID(audioTrackClass, "pause", "()V");
            env->CallVoidMethod(soundwave, pauseMethod);
            env->DeleteLocalRef(audioTrackClass);

            restoreDefaultVolume();
        }
    }

    return bIsPaused;
}

bool SonarPenUtilities::audioResume() {
    JNIEnv *env = getEnv();
    if (bIsPaused) {
        bIsPaused=false;
        if (! getIsPlaying()) {
            if (soundwave == nullptr) startPlay();
            else {
                maximizeStreamVolume();
                jclass audioTrackClass = env->GetObjectClass(soundwave);
                jmethodID setPlaybackHeadPositionMethod = env->GetMethodID(audioTrackClass, "setPlaybackHeadPosition", "(I)I");
                env->CallIntMethod(soundwave, setPlaybackHeadPositionMethod, pause_pos);

                jmethodID playMethod = env->GetMethodID(audioTrackClass, "play", "()V");
                env->DeleteLocalRef(audioTrackClass);
            }
        }
    }

    return bIsPaused;
}

void SonarPenUtilities::saveLowFequence() {
    JNIEnv *env = getEnv();
    jclass contextClass = env->GetObjectClass(mainThreadContext);
    jmethodID getSharedPreferencesMethod = env->GetMethodID(contextClass, "getSharedPreferences", "(Ljava/lang/String;I)Landroid/content/SharedPreferences;");
    jmethodID getPackageNameMethod = env->GetMethodID(contextClass, "getPackageName", "()Ljava/lang/String;");
    jstring pkgName = (jstring) env->CallObjectMethod(mainThreadContext, getPackageNameMethod);
    jobject sh = env->CallObjectMethod(mainThreadContext, getSharedPreferencesMethod, pkgName, 0); //Context.MODE_PRIVATE
    jclass shClass = env->GetObjectClass(sh);
    jmethodID editMethod = env->GetMethodID(shClass, "edit", "()Landroid/content/SharedPreferences$Editor;");
    jobject ed = env->CallObjectMethod(sh, editMethod);
    jclass edClass = env->GetObjectClass(ed);
    jmethodID putBooleanMethod = env->GetMethodID(edClass, "putBoolean", "(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;");
    env->CallObjectMethod(ed, putBooleanMethod, env->NewStringUTF("lowFequence"), true);

    jmethodID commitMethod = env->GetMethodID(edClass, "commit", "()Z");
    env->CallBooleanMethod(ed, commitMethod);

    env->DeleteLocalRef(contextClass);
    env->DeleteLocalRef(pkgName);
    env->DeleteLocalRef(sh);
    env->DeleteLocalRef(shClass);
    env->DeleteLocalRef(ed);
    env->DeleteLocalRef(edClass);
}

void SonarPenUtilities::startSonarPenCalibrate() {
    SLOGD("startSonarPenCalibrate");
    playCnt=0;
    currPressure=0;
    bButtonPressed=false;
    bIsSonicPen=false;
    bNeedCalibrate=true;
    iPenTouchDistance=20;
    bIsPaused=false;
    bRunning=true;

    currThreadID++;
    currentThread.reset();
    currentThread = std::make_shared<std::thread>([this] {
        SLOGD("SonarPenLoop: %ld", currThreadID);
        JNIEnv *env = getEnv();
        getJavaVM()->AttachCurrentThread(&env, NULL);

        while (bRunning) {
            if (bIsPaused) {
                usleep(100000); // sleep 100ms
            } else if (bIsHeadphonesPlugged) {
                if (bNeedCalibrate) {
                    sonarPenInitCheck();
                } else {
                    bAmpValid = calculateCurrentAmp();
                    if (bAmpValid) sonarPenReading();
                }
            } else {
                changeStage(WAITING_HEADSET);
                stopRecording();
                stopPlay();
                playCnt = 0;
                currPressure = 0;
                bButtonPressed = false;
                bIsSonicPen = false;
                bNeedCalibrate = true;
                iPenTouchDistance = 20;

                usleep(50000); // 50ms
            }
        }

        getJavaVM()->DetachCurrentThread();
    });
}

int SonarPenUtilities::startSonarPenUtilities() {
    JNIEnv *env = getEnv();
    bIsRunOnChromebook = runOnChromeBook();
    bool bSyncProfiled=false;

    if (bShowDebugLogCat) {
        SLOGD("SONARPEN: %s ", VERSION_NO);
    }

    SharedPreferences* sh = SharedPreferences::getSonarPenSharedPref(mainThreadContext);\
    if (sh->getBool(sh_sync_loaded,false)) {
        std::string profile = sh->getString(sh_profile_name,"");
        if (sh->getBool("lowFequence",false)) {
            checkFeq=250.0f;
            bByPassDetection=true;
        }
        bSyncProfiled = true;
        if (profile.length() != 0) {
            JSONObject* o = new JSONObject(profile.c_str());
            if (o->isNull()) {
                if ((o->has("minAmp")) && (o->has("maxAmp"))) {
                    bHaveManualSetting = true;
                    ManualMinAmp = (float) o->getDouble("minAmp");
                    ManualMaxAmp = (float) o->getDouble("maxAmp");
                } else {
                    bHaveManualSetting=false;
                }
                if (o->has("maxVol")) {
                    MaxVolPercentage = (float) o->getDouble("maxVol");
                    if (MaxVolPercentage < 100) {
                        bManualCalVol = true;
                    } else bManualCalVol = false;
                } else bManualCalVol = false;
                if (o->has("lowFeq")) {
                    checkFeq = 250.0f;
                }
                if (bShowDebugLogCat) {
                    SLOGD("min: %0.2f ,max: %0.2f, vol: %0.2f, bHaveManualSetting: %d", ManualMinAmp, ManualMaxAmp, MaxVolPercentage, bHaveManualSetting);
                }
            }
        } else {
            bHaveManualSetting=false;
            bManualCalVol=false;
        }
    }
    delete sh;

    if ((useFileHook) && (! bSyncProfiled) && (SDK_INT() < __ANDROID_API_P__)) {
        fileHookStart();
    }
    changeStage(INIT_STAGE);
    registerHeadsetPlugReceiver();
    if (!updateIsHeadphonesPlugged()) { // if failed to get whether or not headset is plugged in,
        lastStartStatus = ERROR_NO_AUDIO_MANGER;
        changeStage(INIT_FAIL);
        return lastStartStatus;
    }

    if (SDK_INT() >= 21) {
        if (!Permissions::hasPermission(mainThreadContext, "android.permission.RECORD_AUDIO")) {
            lastStartStatus = ERROR_NO_PERMISSION;
            changeStage(INIT_FAIL);
            return ERROR_NO_PERMISSION;
        }

        if (audioSession == nullptr) {
            jclass mediaSessionClass = env->FindClass("android/media/session/MediaSession");
            jmethodID mediaSessionConstructor = env->GetMethodID(mediaSessionClass, "<init>", "(Landroid/content/Context;Ljava/lang/String;)V");
            jstring tag = env->NewStringUTF("SONICPEN");
            jobject mediaSession = env->NewObject(mediaSessionClass, mediaSessionConstructor, mainThreadContext, tag);
            audioSession = env->NewGlobalRef(mediaSession);
            getEnv()->DeleteLocalRef(mediaSession);
            getEnv()->DeleteLocalRef(tag);

            jmethodID setFlagsMethod = env->GetMethodID(mediaSessionClass, "setFlags", "(I)V");
            env->CallVoidMethod(audioSession, setFlagsMethod, 0x00000003); // MediaSession.FLAG_HANDLES_MEDIA_BUTTONS | MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS

            jclass SPMediaSessionCallbackClass = env->FindClass("com/greenbulb/sonarpen/SPMediaSessionCallback");
            jmethodID SPMediaSessionCallbackConstructor = env->GetMethodID(SPMediaSessionCallbackClass, "<init>", "()V");
            jobject SPMediaSessionCallback = env->NewObject(SPMediaSessionCallbackClass, SPMediaSessionCallbackConstructor);
            jmethodID setCallbackMethod = env->GetMethodID(mediaSessionClass, "setCallback", "(Landroid/media/session/MediaSession$Callback;Landroid/os/Handler;)V");
            jclass handlerClass = env->FindClass("android/os/Handler");
            jmethodID handlerConstructor = env->GetMethodID(handlerClass, "<init>", "(Landroid/os/Looper;)V");
            jclass looperClass = env->FindClass("android/os/Looper");
            jmethodID getMainLooperMethod = env->GetStaticMethodID(looperClass, "getMainLooper", "()Landroid/os/Looper;");
            jobject mainLooper = env->CallStaticObjectMethod(looperClass, getMainLooperMethod);
            jobject handler = env->NewObject(handlerClass, handlerConstructor, mainLooper);
            env->CallVoidMethod(audioSession, setCallbackMethod, SPMediaSessionCallback, handler);
            getEnv()->DeleteLocalRef(mainLooper);
            getEnv()->DeleteLocalRef(looperClass);
            getEnv()->DeleteLocalRef(handler);
            getEnv()->DeleteLocalRef(handlerClass);
            getEnv()->DeleteLocalRef(SPMediaSessionCallback);
            getEnv()->DeleteLocalRef(SPMediaSessionCallbackClass);

            jclass playbackStateClassBuilder = env->FindClass("android/media/session/PlaybackState$Builder");
            jmethodID playbackStateBuilderConstructor = env->GetMethodID(playbackStateClassBuilder, "<init>", "()V");
            jobject playbackStateBuilder = env->NewObject(playbackStateClassBuilder, playbackStateBuilderConstructor);
            jmethodID setActionsMethod = env->GetMethodID(playbackStateClassBuilder, "setActions", "(J)Landroid/media/session/PlaybackState$Builder;");
            env->CallObjectMethod(playbackStateBuilder, setActionsMethod, 516L); // PlaybackState.ACTION_PLAY | PlaybackState.ACTION_PAUSE | PlaybackState.ACTION_PLAY_PAUSE = 516
            jmethodID setStateMethod = env->GetMethodID(playbackStateClassBuilder, "setState", "(IJFJ)Landroid/media/session/PlaybackState$Builder;");
            env->CallObjectMethod(playbackStateBuilder, setStateMethod, 3, (long)0, (float)0.0, (long)1); // PlaybackState.STATE_PLAYING = 3
            jmethodID buildMethod = env->GetMethodID(playbackStateClassBuilder, "build", "()Landroid/media/session/PlaybackState;");
            jobject playbackState = env->CallObjectMethod(playbackStateBuilder, buildMethod);

            jmethodID setPlaybackStateMethod = env->GetMethodID(mediaSessionClass, "setPlaybackState", "(Landroid/media/session/PlaybackState;)V");
            env->CallVoidMethod(audioSession, setPlaybackStateMethod, playbackState);
            env->CallVoidMethod(audioSession, setFlagsMethod, 0x00000003); // MediaSession.FLAG_HANDLES_MEDIA_BUTTONS | MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS

            getEnv()->DeleteLocalRef(playbackState);
            getEnv()->DeleteLocalRef(playbackStateBuilder);
            getEnv()->DeleteLocalRef(playbackStateClassBuilder);
        }

        jmethodID setActiveMethod = env->GetMethodID(env->GetObjectClass(audioSession), "setActive", "(Z)V");
        env->CallVoidMethod(audioSession, setActiveMethod, true);
    }

    if (bUseTempManualSetting) {
        applyTempManualSetting();
    } else if ((! bSyncProfiled) && (SDK_INT() < __ANDROID_API_P__)){
        getManualSetting();
    }
    bRunning=true;
    bIsSonicPen=false;
    playCnt=0;
    changeStage(CALIBRATE_STAGE);

    jclass contextClass = env->GetObjectClass(mainThreadContext);
    jclass audioManagerClass = env->FindClass("android/media/AudioManager");
    jmethodID getSystemServiceMethod = env->GetMethodID(contextClass, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");
    jstring audioService = env->NewStringUTF("audio");
    jobject audioManager = env->CallObjectMethod(mainThreadContext, getSystemServiceMethod, audioService);
    if (audioManager != nullptr) {
        jmethodID getStreamVolumeMethod = env->GetMethodID(audioManagerClass, "getStreamVolume", "(I)I");
        defaultVolume = env->CallIntMethod(audioManager, getStreamVolumeMethod, 3); // AudioManager.STREAM_MUSIC
    } else {
        defaultVolume = 0;
    }
    getEnv()->DeleteLocalRef(contextClass);
    getEnv()->DeleteLocalRef(audioManagerClass);
    getEnv()->DeleteLocalRef(audioService);
    getEnv()->DeleteLocalRef(audioManager);

    startSonarPenCalibrate();

    lastStartStatus =NO_ERROR;

    if ((! bSyncProfiled) && (SDK_INT() < __ANDROID_API_P__)) fileHookStop();

    return NO_ERROR;
}

void SonarPenUtilities::stopRecording() {
    JNIEnv *env = getEnv();
    if (audioInput != nullptr) {
        jclass audioInputClass = env->GetObjectClass(audioInput);
        jmethodID stopMethod = env->GetMethodID(audioInputClass, "stop", "()V");
        jmethodID releaseMethod = env->GetMethodID(audioInputClass, "release", "()V");
        env->CallVoidMethod(audioInput, stopMethod);
        env->CallVoidMethod(audioInput, releaseMethod);

        checkAndClearException(env);

        env->DeleteGlobalRef(audioInput);
        audioInput = nullptr;
    }
    touchMinAmp=0;
}

void SonarPenUtilities::stop() {
    JNIEnv *env = getEnv();
    fileHookStop();
    bRunning=false;
    isStarted = false;
    if (currentThread) {
        if (currentThread->joinable()) {
            currentThread->join(); // ATTENTION: This will block the main thread and cannot interrupt the thread
        }
        currentThread.reset();
    }
    stopRecording();
    if (audioSession != nullptr) {
        if (SDK_INT() >= 21) {        // Android LOLLIPOP = 21
            jclass mediaSessionClass = env->GetObjectClass(audioSession);
            jmethodID setActiveMethod = env->GetMethodID(mediaSessionClass, "setActive", "(Z)V");
            env->CallVoidMethod(audioSession, setActiveMethod, false);
            env->DeleteLocalRef(mediaSessionClass);
        }
    }
    restoreDefaultVolume();
    stopPlay();
    unregisterHeadsetPlugReceiver();
    bIsPaused=false;
    playCnt=0;
    currPressure=0;
    bButtonPressed=false;
    bIsSonicPen=false;
    changeStage(CLOSED_STAGE);
    lastStartStatus=NOT_RUNNING;
}

void SonarPenUtilities::broadcastSetting() {
    JNIEnv *env = getEnv();
    jclass jsonClass = env->FindClass("org/json/JSONObject");
    jmethodID jsonConstructor = env->GetMethodID(jsonClass, "<init>", "()V");
    jobject json = env->NewObject(jsonClass, jsonConstructor);

    if (bHaveManualSetting) {
        jmethodID putMethod = env->GetMethodID(jsonClass, "put", "(Ljava/lang/String;D)Lorg/json/JSONObject;");
        env->CallObjectMethod(json, putMethod, env->NewStringUTF("minAmp"), (double)ManualMinAmp);
        env->CallObjectMethod(json, putMethod, env->NewStringUTF("maxAmp"), (double)ManualMaxAmp);
        if (bManualCalVol) {
            env->CallObjectMethod(json, putMethod, env->NewStringUTF("volume"), (double)MaxVolPercentage);
        } else {
            env->CallObjectMethod(json, putMethod, env->NewStringUTF("volume"), (double)100.0);
        }
        if (checkFeq != 9000) {
            putMethod = env->GetMethodID(jsonClass, "put", "(Ljava/lang/String;Z)Lorg/json/JSONObject;");
            env->CallObjectMethod(json, putMethod, env->NewStringUTF("lowFeq"), true);
        }
    } else {
        jmethodID putMethod = env->GetMethodID(jsonClass, "put", "(Ljava/lang/String;I)Lorg/json/JSONObject;");
        env->CallObjectMethod(json, putMethod, env->NewStringUTF("minAmp"), 0);
        env->CallObjectMethod(json, putMethod, env->NewStringUTF("maxAmp"), 0);

        putMethod = env->GetMethodID(jsonClass, "put", "(Ljava/lang/String;D)Lorg/json/JSONObject;");
        env->CallObjectMethod(json, putMethod, env->NewStringUTF("volume"), (double)100.0);
    }

    jmethodID toStringMethod = env->GetMethodID(jsonClass, "toString", "()Ljava/lang/String;");
    jstring profile = (jstring) env->CallObjectMethod(json, toStringMethod);

    jclass contextClass = env->GetObjectClass(mainThreadContext);
    jmethodID getSharedPreferencesMethod = env->GetMethodID(contextClass, "getSharedPreferences", "(Ljava/lang/String;I)Landroid/content/SharedPreferences;");
    jmethodID getPackageNameMethod = env->GetMethodID(contextClass, "getPackageName", "()Ljava/lang/String;");
    jstring pkgName = (jstring) env->CallObjectMethod(mainThreadContext, getPackageNameMethod);
    jobject sh = env->CallObjectMethod(mainThreadContext, getSharedPreferencesMethod, pkgName, 0); //Context.MODE_PRIVATE
    jclass shClass = env->GetObjectClass(sh);
    jmethodID editMethod = env->GetMethodID(shClass, "edit", "()Landroid/content/SharedPreferences$Editor;");
    jobject ed = env->CallObjectMethod(sh, editMethod);
    jclass edClass = env->GetObjectClass(ed);
    jmethodID putStringMethod = env->GetMethodID(edClass, "putString", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;");
    jmethodID putBooleanMethod = env->GetMethodID(edClass, "putBoolean", "(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;");
    jmethodID commitMethod = env->GetMethodID(edClass, "commit", "()Z");

    env->CallObjectMethod(ed, putStringMethod, env->NewStringUTF(sh_profile_name), profile);
    env->CallObjectMethod(ed, putBooleanMethod, env->NewStringUTF(sh_sync_loaded), true);
    env->CallBooleanMethod(ed, commitMethod);

    jclass intentClass = env->FindClass("android/content/Intent");
    jmethodID intentConstructor = env->GetMethodID(intentClass, "<init>", "(Ljava/lang/String;)V");
    jobject it = env->NewObject(intentClass, intentConstructor, env->NewStringUTF(profile_action));
    jmethodID putExtraMethod = env->GetMethodID(intentClass, "putExtra", "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;");
    env->CallObjectMethod(it, putExtraMethod, env->NewStringUTF("sender_name"), pkgName);
    env->CallObjectMethod(it, putExtraMethod, env->NewStringUTF("to_name"), env->NewStringUTF("all"));
    env->CallObjectMethod(it, putExtraMethod, env->NewStringUTF("profile"), profile);
    jmethodID sendBroadcastMethod = env->GetMethodID(contextClass, "sendBroadcast", "(Landroid/content/Intent;)V");
    env->CallVoidMethod(mainThreadContext, sendBroadcastMethod, it);
    checkAndClearException(env);

    env->DeleteLocalRef(jsonClass);
    env->DeleteLocalRef(json);
    env->DeleteLocalRef(contextClass);
    env->DeleteLocalRef(pkgName);
    env->DeleteLocalRef(sh);
    env->DeleteLocalRef(shClass);
    env->DeleteLocalRef(ed);
    env->DeleteLocalRef(edClass);
    env->DeleteLocalRef(it);
    env->DeleteLocalRef(intentClass);
}

bool SonarPenUtilities::markSettingValue() {
    startCalibrateScreen();
    return false;
}

void SonarPenUtilities::generateTone() {
    generateTone(true);
}

void SonarPenUtilities::generateTone(bool bRightOnly) {
    JNIEnv *env = getEnv();
    jclass audioTrackClass = env->FindClass("android/media/AudioTrack");
    if (soundwave != nullptr) {
        jmethodID getPlayStateMethod = env->GetMethodID(audioTrackClass, "getPlayState", "()I");
        if (env->CallIntMethod(soundwave, getPlayStateMethod) == 3) { // AudioTrack.PLAYSTATE_PLAYING
            jmethodID stopMethod = env->GetMethodID(audioTrackClass, "stop", "()V");
            env->CallVoidMethod(soundwave, stopMethod);
        }
        jmethodID releaseMethod = env->GetMethodID(audioTrackClass, "release", "()V");
        env->CallVoidMethod(soundwave, releaseMethod);
        env->DeleteGlobalRef(soundwave);
        soundwave = nullptr;
    }

    const double freqHz = checkFeq;
    if (bShowDebugLogCat) {
        SLOGD("feq: %f", checkFeq);
    }
    const int durationMs = 1000;
    int count = (int)(SAMPLING_RATE * 2.0 * (durationMs / 1000.0)) & ~1;
    short *samples = new short[count];
    for (int i = 0; i < count; i+=2) {
        short sample = (short)(sin(2 * M_PI * i / (SAMPLING_RATE / freqHz)) * 0x7FFF);
        if (bRightOnly) {
            samples[i + 0] = 0;
            samples[i + 1] = sample; // right channel
        } else {
            samples[i + 0] = sample; // left channel
            samples[i + 1] = 0;
        }
    }

    if (SDK_INT() >= 26) {
        jclass audioFormatBuilderClass = env->FindClass("android/media/AudioFormat$Builder");
        jmethodID audioFormatBuilderConstructor = env->GetMethodID(audioFormatBuilderClass,
                                                                   "<init>", "()V");
        jobject audioFormatBuilder = env->NewObject(audioFormatBuilderClass,
                                                    audioFormatBuilderConstructor);
        jmethodID setSampleRateMethod = env->GetMethodID(audioFormatBuilderClass, "setSampleRate",
                                                         "(I)Landroid/media/AudioFormat$Builder;");
        jmethodID setEncodingMethod = env->GetMethodID(audioFormatBuilderClass, "setEncoding",
                                                       "(I)Landroid/media/AudioFormat$Builder;");
        jmethodID setChannelMaskMethod = env->GetMethodID(audioFormatBuilderClass, "setChannelMask",
                                                          "(I)Landroid/media/AudioFormat$Builder;");
        jmethodID buildMethod = env->GetMethodID(audioFormatBuilderClass, "build",
                                                 "()Landroid/media/AudioFormat;");
        audioFormatBuilder = env->CallObjectMethod(audioFormatBuilder, setSampleRateMethod,
                                                   SAMPLING_RATE);
        audioFormatBuilder = env->CallObjectMethod(audioFormatBuilder, setEncodingMethod,
                                                   2); // AudioFormat.ENCODING_PCM_16BIT
        audioFormatBuilder = env->CallObjectMethod(audioFormatBuilder, setChannelMaskMethod,
                                                   12); // AudioFormat.CHANNEL_OUT_STEREO
        jobject audioFormat = env->CallObjectMethod(audioFormatBuilder, buildMethod);

        jclass audioAttributesBuilderClass = env->FindClass(
                "android/media/AudioAttributes$Builder");
        jmethodID audioAttributesBuilderConstructor = env->GetMethodID(audioAttributesBuilderClass,
                                                                       "<init>", "()V");
        jobject audioAttributesBuilder = env->NewObject(audioAttributesBuilderClass,
                                                        audioAttributesBuilderConstructor);
        jmethodID setContentTypeMethod = env->GetMethodID(audioAttributesBuilderClass,
                                                          "setContentType",
                                                          "(I)Landroid/media/AudioAttributes$Builder;");
        jmethodID setLegacyStreamTypeMethod = env->GetMethodID(audioAttributesBuilderClass,
                                                               "setLegacyStreamType",
                                                               "(I)Landroid/media/AudioAttributes$Builder;");
        jmethodID setUsageMethod = env->GetMethodID(audioAttributesBuilderClass, "setUsage",
                                                    "(I)Landroid/media/AudioAttributes$Builder;");
        jmethodID buildAudioAttributesMethod = env->GetMethodID(audioAttributesBuilderClass,
                                                                "build",
                                                                "()Landroid/media/AudioAttributes;");
        audioAttributesBuilder = env->CallObjectMethod(audioAttributesBuilder, setContentTypeMethod,
                                                       2); // AudioAttributes.CONTENT_TYPE_MUSIC
        audioAttributesBuilder = env->CallObjectMethod(audioAttributesBuilder,
                                                       setLegacyStreamTypeMethod,
                                                       3); // AudioManager.STREAM_MUSIC
        audioAttributesBuilder = env->CallObjectMethod(audioAttributesBuilder, setUsageMethod,
                                                       1); // AudioAttributes.USAGE_MEDIA
        jobject audioAttributes = env->CallObjectMethod(audioAttributesBuilder,
                                                        buildAudioAttributesMethod);

        jclass contextClass = env->GetObjectClass(mainThreadContext);
        jmethodID getSystemServiceMethod = env->GetMethodID(contextClass, "getSystemService",
                                                            "(Ljava/lang/String;)Ljava/lang/Object;");
        jobject audioManager = env->CallObjectMethod(mainThreadContext, getSystemServiceMethod,
                                                     env->NewStringUTF("audio"));
        jclass audioManagerClass = env->GetObjectClass(audioManager);
        jmethodID generateAudioSessionIdMethod = env->GetMethodID(audioManagerClass,
                                                                  "generateAudioSessionId", "()I");
        jint audioSessionId = env->CallIntMethod(audioManager, generateAudioSessionIdMethod);

        jmethodID audioTrackConstructor = env->GetMethodID(audioTrackClass, "<init>",
                                                           "(Landroid/media/AudioAttributes;Landroid/media/AudioFormat;III)V");
        jobject audioTrack = env->NewObject(audioTrackClass, audioTrackConstructor, audioAttributes,
                                            audioFormat, count * 2, 0,
                                            audioSessionId); // count * (Short.SIZE / Byte.SIZE) = count * 2, AudioTrack.MODE_STATIC = 0
        soundwave = env->NewGlobalRef(audioTrack);

        env->DeleteLocalRef(audioFormatBuilderClass);
        env->DeleteLocalRef(audioFormatBuilder);
        env->DeleteLocalRef(audioFormat);

        env->DeleteLocalRef(audioAttributesBuilderClass);
        env->DeleteLocalRef(audioAttributesBuilder);
        env->DeleteLocalRef(audioAttributes);

        env->DeleteLocalRef(contextClass);
        env->DeleteLocalRef(audioManager);
        env->DeleteLocalRef(audioManagerClass);
        env->DeleteLocalRef(audioTrack);
    } else {
        jmethodID audioTrackConstructor = env->GetMethodID(audioTrackClass, "<init>",
                                                           "(IIIIII)V");
        jobject audioTrack = env->NewObject(audioTrackClass, audioTrackConstructor, 3, SAMPLING_RATE, 12, 2, count * 2, 0);
        soundwave = env->NewGlobalRef(audioTrack);
        env->DeleteLocalRef(audioTrack);
    }

    jmethodID getPlayStateMethod = env->GetMethodID(audioTrackClass, "getPlayState", "()I");
    jmethodID releaseMethod = env->GetMethodID(audioTrackClass, "release", "()V");
    if (env->CallIntMethod(soundwave, getPlayStateMethod) == 0) { // AudioTrack.STATE_UNINITIALIZED
        env->CallVoidMethod(soundwave, releaseMethod);
        env->DeleteGlobalRef(soundwave);
        soundwave = nullptr;
        return;
    }

    jmethodID writeMethod = env->GetMethodID(audioTrackClass, "write", "([SII)I");
    jshortArray shortArray = env->NewShortArray(count);
    env->SetShortArrayRegion(shortArray, 0, count, samples);
    env->CallIntMethod(soundwave, writeMethod, shortArray, 0, count);
    env->DeleteLocalRef(shortArray);

    if (env->CallIntMethod(soundwave, getPlayStateMethod) != 1) { // AudioTrack.STATE_INITIALIZED
        env->CallVoidMethod(soundwave, releaseMethod);
        env->DeleteGlobalRef(soundwave);
        soundwave = nullptr;
        return;
    }

    jmethodID setLoopPointsMethod = env->GetMethodID(audioTrackClass, "setLoopPoints", "(III)I");
    env->CallIntMethod(soundwave, setLoopPointsMethod, 0, count / 4, -1);
    if (SDK_INT() >= 21) {
        jmethodID setVolumeMethod = env->GetMethodID(audioTrackClass, "setVolume", "(F)I");
        jmethodID getMaxVolumeMethod = env->GetStaticMethodID(audioTrackClass, "getMaxVolume",
                                                              "()F");
        env->CallIntMethod(soundwave, setVolumeMethod,
                           env->CallStaticFloatMethod(audioTrackClass, getMaxVolumeMethod));
    }

    env->DeleteLocalRef(audioTrackClass);
}

float SonarPenUtilities::getAmplitude(short *buffer, int length) {
    long sum = 0;
    for (int i=0;i<length;i++) {
        sum += buffer[i] * buffer[i];
    }
    double amplitude = sum / (length * 2.0f);
    return (float) sqrt(amplitude);
}

bool SonarPenUtilities::isSonicPenButton(jobject keyEvent) {
    JNIEnv *env = getEnv();
    jclass keyEventClass = env->GetObjectClass(keyEvent);
    jmethodID getKeyCodeMethod = env->GetMethodID(keyEventClass, "getKeyCode", "()I");
    jmethodID getActionMethod = env->GetMethodID(keyEventClass, "getAction", "()I");
    jint keyCode = env->CallIntMethod(keyEvent, getKeyCodeMethod);
    jint action = env->CallIntMethod(keyEvent, getActionMethod);
    env->DeleteLocalRef(keyEventClass);
    return isSonicPenButton(keyCode, action);
}

bool SonarPenUtilities::isSonicPenButton(int keyCode, int action) {
    if ((keyCode == KEYCODE_HEADSETHOOK) || (keyCode == KEYCODE_MEDIA_PLAY_PAUSE)) {
        if (bIsSonicPen) {
            bButtonPressed = (action == KEY_ACTION_DOWN);
            if (bButtonPressed) {
                onSonarPenButtonPressed();
                if (myCallBack != nullptr) {
                    myCallBack->onSonarPenButtonPressed();
                }
            }
            return true;
        }
    }
    return false;
}

bool SonarPenUtilities::getSonicPenButtonPressed() {
    return bButtonPressed;
}

void SonarPenUtilities::addSonarPenCallback(SonarPenCallback *callback) {
    myCallBack = callback;
}

void SonarPenUtilities::onReceive(jobject context, jobject intent) {
    JNIEnv *env = getEnv();
    jclass intentClass = env->GetObjectClass(intent);
    jmethodID getActionMethod = env->GetMethodID(intentClass, "getAction", "()Ljava/lang/String;");
    jstring action = (jstring) env->CallObjectMethod(intent, getActionMethod);
    const char *actionChars = env->GetStringUTFChars(action, nullptr);

    if (strcmp("android.intent.action.HEADSET_PLUG", actionChars) == 0) {
        bool prevPlugged = bIsHeadphonesPlugged;
        jmethodID getIntExtraMethod = env->GetMethodID(intentClass, "getIntExtra", "(Ljava/lang/String;I)I");
        jint intVal = env->CallIntMethod(intent, getIntExtraMethod, env->NewStringUTF("state"), -1);
        bIsHeadphonesPlugged = intVal > 0;

        if (prevPlugged != bIsHeadphonesPlugged) {
            if (bIsHeadphonesPlugged) {
                onHeadsetPlugged();
            } else {
                onHeadsetUnplugged();
            }
        }
    }
    env->ReleaseStringUTFChars(action, actionChars);
    env->DeleteLocalRef(intentClass);
}

void SonarPenUtilities::startPlay() {
    JNIEnv *env = getEnv();
    if (bIsHeadphonesPlugged && !getIsPlaying()) {
        maximizeStreamVolume();
        generateTone();
        if (soundwave == nullptr) { // if the initialization of AudioTrack in generateTone() failed,
            changeStage(AUDIO_CHANNEL_FAIL);
        } else {
            jclass soundwaveClass = env->GetObjectClass(soundwave);
            jmethodID playMethod = env->GetMethodID(soundwaveClass, "play", "()V");
            env->CallVoidMethod(soundwave, playMethod);
            env->DeleteLocalRef(soundwaveClass);
        }
        playCnt = 0;
    }
}

void SonarPenUtilities::setManualCalVol(int ptg) {
    bCalibrateVol = true;
    CalVolPercentage = ptg;
}

void SonarPenUtilities::clearManualCalVol() {
    bCalibrateVol = false;
}

void SonarPenUtilities::maximizeStreamVolume() {
    if (no_set_max_value) return;
    JNIEnv *env = getEnv();
    jclass contextClass = env->GetObjectClass(mainThreadContext);
    jmethodID getSystemServiceMethod = env->GetMethodID(contextClass, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");
    jobject audioManager = env->CallObjectMethod(mainThreadContext, getSystemServiceMethod, env->NewStringUTF("audio"));

    if (audioManager != nullptr) {
        jclass audioManagerClass = env->GetObjectClass(audioManager);
        jmethodID getStreamMaxVolumeMethod = env->GetMethodID(audioManagerClass, "getStreamMaxVolume", "(I)I");
        jmethodID getStreamVolumeMethod = env->GetMethodID(audioManagerClass, "getStreamVolume", "(I)I");
        jmethodID setStreamVolumeMethod = env->GetMethodID(audioManagerClass, "setStreamVolume", "(III)V");
        jmethodID adjustStreamVolumeMethod = env->GetMethodID(audioManagerClass, "adjustStreamVolume", "(III)V");

        jint idx = env->CallIntMethod(audioManager, getStreamMaxVolumeMethod, 3); // AudioManager.STREAM_MUSIC
        if (bCalibrateVol) {
            idx = round(idx * CalVolPercentage / 100.0f);
        } else if ((bManualCalVol) && (bUseManualCalSetting)) {
            idx = round(idx * MaxVolPercentage / 100.0f);
        }

        if (bShowDebugLogCat) {
            SLOGD("set volume: %d", idx);
        }

        env->CallVoidMethod(audioManager, setStreamVolumeMethod, 3, idx, 0); // AudioManager.STREAM_MUSIC
        checkAndClearException(env);

        jint currentVolume = env->CallIntMethod(audioManager, getStreamVolumeMethod, 3); // AudioManager.STREAM_MUSIC
        if (currentVolume < idx) {
            env->CallVoidMethod(audioManager, adjustStreamVolumeMethod, 3, 1, 0); // AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE
            changeStage(AUDIO_VOL_CANT_CHANGE);
        }

        env->DeleteLocalRef(audioManagerClass);
    }
    env->DeleteLocalRef(contextClass);
    env->DeleteLocalRef(audioManager);
}

void SonarPenUtilities::restoreDefaultVolume() {
    JNIEnv *env = getEnv();
    jclass contextClass = env->GetObjectClass(mainThreadContext);
    jmethodID getSystemServiceMethod = env->GetMethodID(contextClass, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");
    jobject audioManager = env->CallObjectMethod(mainThreadContext, getSystemServiceMethod, env->NewStringUTF("audio"));

    if (audioManager != nullptr) {
        jclass audioManagerClass = env->GetObjectClass(audioManager);
        jmethodID setStreamVolumeMethod = env->GetMethodID(audioManagerClass, "setStreamVolume", "(III)V");
        env->CallVoidMethod(audioManager, setStreamVolumeMethod, 3, defaultVolume, 0); // AudioManager.STREAM_MUSIC
        env->DeleteLocalRef(audioManagerClass);
        env->DeleteLocalRef(audioManager);
    }
    env->DeleteLocalRef(contextClass);
}

bool SonarPenUtilities::getIsPlaying() {
    if (soundwave == nullptr) return false;
    JNIEnv *env = getEnv();
    jclass soundwaveClass = env->GetObjectClass(soundwave);
    jmethodID getPlayStateMethod = env->GetMethodID(soundwaveClass, "getPlayState", "()I");
    jint playState = env->CallIntMethod(soundwave, getPlayStateMethod);
    env->DeleteLocalRef(soundwaveClass);
    return playState == 3; // AudioTrack.PLAYSTATE_PLAYING
}

void SonarPenUtilities::stopPlay() {
    if (getIsPlaying()) {
        JNIEnv *env = getEnv();
        jclass soundwaveClass = env->GetObjectClass(soundwave);
        jmethodID stopMethod = env->GetMethodID(soundwaveClass, "stop", "()V");
        jmethodID releaseMethod = env->GetMethodID(soundwaveClass, "release", "()V");
        env->CallVoidMethod(soundwave, stopMethod);
        env->CallVoidMethod(soundwave, releaseMethod);
        env->DeleteLocalRef(soundwaveClass);

        env->DeleteGlobalRef(soundwave);
        soundwave = nullptr;
        playCnt = 0;
    }
}

void SonarPenUtilities::onHeadsetPlugged() {
    JNIEnv *env = getEnv();
    if (SDK_INT() >= 23) {
        jclass contextClass = env->GetObjectClass(mainThreadContext);
        jmethodID getSystemServiceMethod = env->GetMethodID(contextClass, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");
        jobject audioManager = env->CallObjectMethod(mainThreadContext, getSystemServiceMethod, env->NewStringUTF("audio"));

        if (audioManager != nullptr) {
            bool bChecked = false;
            jclass audioManagerClass = env->GetObjectClass(audioManager);
            jmethodID getDevicesMethod = env->GetMethodID(audioManagerClass, "getDevices", "(I)[Landroid/media/AudioDeviceInfo;");
            jobjectArray audioDevices = (jobjectArray) env->CallObjectMethod(audioManager, getDevicesMethod, 1); // AudioManager.GET_DEVICES_INPUTS
            jsize length = env->GetArrayLength(audioDevices);
            for (int i = 0; i < length; i++) {
                jobject deviceInfo = env->GetObjectArrayElement(audioDevices, i);
                jclass deviceInfoClass = env->GetObjectClass(deviceInfo);
                jmethodID getTypeMethod = env->GetMethodID(deviceInfoClass, "getType", "()I");
                jint type = env->CallIntMethod(deviceInfo, getTypeMethod);
                if (type == 22) { // AudioDeviceInfo.TYPE_USB_HEADSET
                    jmethodID getProductNameMethod = env->GetMethodID(deviceInfoClass,
                                                                      "getProductName",
                                                                      "()Ljava/lang/CharSequence;");
                    jobject productName = env->CallObjectMethod(deviceInfo, getProductNameMethod);
                    jclass productNameClass = env->GetObjectClass(productName);
                    jmethodID toStringMethod = env->GetMethodID(productNameClass, "toString",
                                                                "()Ljava/lang/String;");
                    jstring productNameStr = (jstring) env->CallObjectMethod(productName,
                                                                             toStringMethod);

                    const char *productNameChars = env->GetStringUTFChars((jstring) productNameStr,
                                                                          nullptr);
                    if (strstr(productNameChars, "SP1") != nullptr) {
                        bIsSonarPenUSB = true;
                        checkFeq = 1000;
                    } else {
                        bIsHeadphonesPlugged = false;
                    }
                    bChecked = true;
                    env->ReleaseStringUTFChars((jstring) productName, productNameChars);
                    env->DeleteLocalRef(productNameStr);
                    env->DeleteLocalRef(productNameClass);
                    env->DeleteLocalRef(productName);
                }
                env->DeleteLocalRef(deviceInfoClass);
                env->DeleteLocalRef(deviceInfo);
            }

            if (!bChecked) {
                jclass usbManagerClass = env->FindClass("android/hardware/usb/UsbManager");
                jmethodID getSystemServiceMethod = env->GetMethodID(contextClass, "getSystemService",
                                                                   "(Ljava/lang/String;)Ljava/lang/Object;");
                jobject usbManager = env->CallObjectMethod(mainThreadContext, getSystemServiceMethod,
                                                           env->NewStringUTF("usb"));
                jmethodID getDeviceListMethod = env->GetMethodID(usbManagerClass, "getDeviceList",
                                                                "()Ljava/util/HashMap;");
                jobject usbDevices = env->CallObjectMethod(usbManager, getDeviceListMethod);
                jclass hashMapClass = env->FindClass("java/util/HashMap");
                jmethodID valuesMethod = env->GetMethodID(hashMapClass, "values", "()Ljava/util/Collection;");
                jobject values = env->CallObjectMethod(usbDevices, valuesMethod);
                jclass collectionClass = env->FindClass("java/util/Collection");
                jmethodID toArrayMethod = env->GetMethodID(collectionClass, "toArray", "()[Ljava/lang/Object;");
                jobjectArray usbArray = (jobjectArray) env->CallObjectMethod(values, toArrayMethod);
                jsize usbLength = env->GetArrayLength(usbArray);
                for (int i = 0; i < usbLength; i++) {
                    jobject usb = env->GetObjectArrayElement(usbArray, i);
                    jclass usbClass = env->GetObjectClass(usb);
                    if (SDK_INT() >= __ANDROID_API_L__) {
                        jmethodID getProductNameMethod = env->GetMethodID(usbClass, "getProductName",
                                                                          "()Ljava/lang/String;");
                        jstring productName = (jstring) env->CallObjectMethod(usb, getProductNameMethod);
                        const char *productNameChars = env->GetStringUTFChars(productName, nullptr);
                        if (strstr(productNameChars, "SP1") != nullptr) {
                            bIsSonarPenUSB = true;
                            env->ReleaseStringUTFChars(productName, productNameChars);
                            env->DeleteLocalRef(productName);
                            env->DeleteLocalRef(usb);
                            env->DeleteLocalRef(usbClass);
                            break;
                        }
                        env->ReleaseStringUTFChars(productName, productNameChars);
                        env->DeleteLocalRef(productName);
                    } else {
                        jmethodID toStringMethod = env->GetMethodID(usbClass, "toString", "()Ljava/lang/String;");
                        jstring usbtostr = (jstring) env->CallObjectMethod(usb, toStringMethod);
                        const char *usbtostrChars = env->GetStringUTFChars(usbtostr, nullptr);
                        int idx = std::string(usbtostrChars).find("mProductName=");
                        if (idx != std::string::npos) {
                            idx += 13;
                            std::string pname = std::string(usbtostrChars).substr(idx);
                            idx = pname.find(',');
                            if (idx != std::string::npos) {
                                pname = pname.substr(0, idx);
                            }
                            if (pname.find("SP1") != std::string::npos) {
                                bIsSonarPenUSB = true;
                                env->ReleaseStringUTFChars(usbtostr, usbtostrChars);
                                env->DeleteLocalRef(usbtostr);
                                env->DeleteLocalRef(usb);
                                env->DeleteLocalRef(usbClass);
                                break;
                            }
                        }
                        env->ReleaseStringUTFChars(usbtostr, usbtostrChars);
                        env->DeleteLocalRef(usbtostr);
                    }
                }
                env->DeleteLocalRef(collectionClass);
                env->DeleteLocalRef(values);
                env->DeleteLocalRef(hashMapClass);
                env->DeleteLocalRef(usbManagerClass);
                env->DeleteLocalRef(usbManager);
            }

            env->DeleteLocalRef(audioManager);
        }
        env->DeleteLocalRef(contextClass);
    }
}

void SonarPenUtilities::onHeadsetUnplugged() {
    stopPlay();
    restoreDefaultVolume();
}

bool SonarPenUtilities::getUseTouchSize() {
    return bTouchSizeChecking;
}

void SonarPenUtilities::setUseTouchSize(bool bOnOff) {
    bTouchSizeChecking = bOnOff;
}

jobject SonarPenUtilities::skipMotionEvent(jobject motionEvent) {
    JNIEnv *env = getEnv();
    jclass motionEventClass = env->GetObjectClass(motionEvent);
    jmethodID getPointerCountMethod = env->GetMethodID(motionEventClass, "getPointerCount", "()I");
    jint ptCount = env->CallIntMethod(motionEvent, getPointerCountMethod);

    jclass pointerPropertiesClass = env->FindClass("android/view/MotionEvent$PointerProperties");
    jobjectArray props = env->NewObjectArray(ptCount, pointerPropertiesClass, nullptr);
    jclass pointerCoordsClass = env->FindClass("android/view/MotionEvent$PointerCoords");
    jobjectArray coords = env->NewObjectArray(ptCount, pointerCoordsClass, nullptr);

    for (int i = 0; i < ptCount; i++) {
        jobject currProp = env->NewObject(pointerPropertiesClass, env->GetMethodID(pointerPropertiesClass, "<init>", "()V"));
        jobject currCoords = env->NewObject(pointerCoordsClass, env->GetMethodID(pointerCoordsClass, "<init>", "()V"));
        jmethodID getPointerCoordsMethod = env->GetMethodID(motionEventClass, "getPointerCoords", "(ILandroid/view/MotionEvent$PointerCoords;)V");
        env->CallVoidMethod(motionEvent, getPointerCoordsMethod, i, currCoords);
        checkAndClearException(env);

        jfieldID idField = env->GetFieldID(pointerPropertiesClass, "id", "I");
        jmethodID getPointerIdMethod = env->GetMethodID(motionEventClass, "getPointerId", "(I)I");
        jint pointerId = env->CallIntMethod(motionEvent, getPointerIdMethod, i);
        env->SetIntField(currProp, idField, pointerId);

        jfieldID toolTypeField = env->GetFieldID(pointerPropertiesClass, "toolType", "I");
        env->SetIntField(currProp, toolTypeField, 0);

        jfieldID pressureField = env->GetFieldID(pointerCoordsClass, "pressure", "F");
        env->SetFloatField(currCoords, pressureField, 0.0f);

        env->SetObjectArrayElement(props, i, currProp);
        env->SetObjectArrayElement(coords, i, currCoords);

        env->DeleteLocalRef(currProp);
        env->DeleteLocalRef(currCoords);
    }

    jmethodID obtainMethod = env->GetStaticMethodID(motionEventClass, "obtain", "(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;");
    jobject newMotionEvent = env->CallStaticObjectMethod(motionEventClass, obtainMethod,
                                                         env->CallLongMethod(motionEvent, env->GetMethodID(motionEventClass, "getDownTime", "()J")),
                                                         env->CallLongMethod(motionEvent, env->GetMethodID(motionEventClass, "getEventTime", "()J")),
                                                         2,
                                                         ptCount,
                                                         props,
                                                         coords,
                                                         env->CallIntMethod(motionEvent, env->GetMethodID(motionEventClass, "getMetaState", "()I")),
                                                         env->CallIntMethod(motionEvent, env->GetMethodID(motionEventClass, "getButtonState", "()I")),
                                                         env->CallFloatMethod(motionEvent, env->GetMethodID(motionEventClass, "getXPrecision", "()F")),
                                                         env->CallFloatMethod(motionEvent, env->GetMethodID(motionEventClass, "getYPrecision", "()F")),
                                                         0, env->CallIntMethod(motionEvent, env->GetMethodID(motionEventClass, "getEdgeFlags", "()I")),
                                                         env->CallIntMethod(motionEvent, env->GetMethodID(motionEventClass, "getSource", "()I")),
                                                         env->CallIntMethod(motionEvent, env->GetMethodID(motionEventClass, "getFlags", "()I")));
    return newMotionEvent;
}

jobject SonarPenUtilities::translateTouchEvent(jobject event) {
    if ((! bIsSonicPen) || (bNeedCalibrate) || (bIsPaused) || (! bIsHeadphonesPlugged)) return event;

    MotionEvent motionEvent(event);
    int ptCount = motionEvent.getPointerCount();
    int action = motionEvent.getActionMasked();

    if ((action == MotionEvent::ACTION_DOWN) || (action == MotionEvent::ACTION_POINTER_DOWN) || (action == MotionEvent::ACTION_MOVE)) {
        bOnTouch=true;
        if ((bTouchSizeChecking) && (! bSystemDriverChecked)) {
            if (ptCount > 1) {
                float sm_size=motionEvent.getSize(0);
                float sm_pressure=motionEvent.getPressure(0);
                float mk_size=-1;
                float mk_pressure=-1;
                if (motionEvent.getPointerId(0) == lastTouchedID) {
                    mk_size = sm_size;
                    mk_pressure = sm_pressure;
                }

                bool bHaveSize=false;
                bool bHavePressure=false;

                for (int i=1;i<ptCount;i++) {
                    if (motionEvent.getSize(i) != sm_size) {
                        bHaveSize = true;
                        if (motionEvent.getPointerId(i) == lastTouchedID)
                            mk_size = motionEvent.getSize(i);
                    }
                    if (motionEvent.getPressure(i) != sm_pressure) {
                        bHavePressure = true;
                        if (motionEvent.getPointerId(i) == lastTouchedID)
                            mk_pressure = motionEvent.getPressure(i);
                    }
                }
                bSystemDriverChecked=true;
                if (bHaveSize) {
                    if (lastTouchedSize == 999) {
                        lastTouchedSize = mk_size;
                    }
                    bSystemDriverHaveSize=true;
                } else if (bHavePressure) {
                    if (lastTouchedSize == 999) {
                        lastTouchedSize = mk_pressure;
                    }
                    bSystemDriverHaveSize=true;
                    bSystemUseSize=false;
                }
            } else if (size_check_point < 4) {
                size_check_point_size[size_check_point] = motionEvent.getSize(0);
                size_check_point_pressure[size_check_point] = motionEvent.getPressure(0);
                size_check_point++;
                if (size_check_point >= 4) {
                    bSystemDriverChecked=true;

                    if ((size_check_point_size[0] == size_check_point_size[1]) &&
                        (size_check_point_size[1] == size_check_point_size[2]) &&
                        (size_check_point_size[2] == size_check_point_size[3])) {
                        if ((size_check_point_pressure[0] == size_check_point_pressure[1]) &&
                            (size_check_point_pressure[1] == size_check_point_pressure[2]) &&
                            (size_check_point_pressure[2] == size_check_point_pressure[3])) {
                            bSystemDriverHaveSize=false;
                        } else {
                            bSystemUseSize=false;
                            bSystemDriverHaveSize=true;
                        }
                    } else {
                        bSystemUseSize=true;
                        bSystemDriverHaveSize=true;
                    }
                }
            }
        }

        sonarPenReading();
        float lastAmp = currentAmp;
        if (prevAmp == 0) prevAmp = currentAmp;
        float ptsize=0;
        int lastStage = currentStage;
        if (bShowDebugLogCat) {
            SLOGD("before touchID=%d touchmin=%f, current=%f, %d, size=%s", lastTouchedID, touchMinAmp, currentAmp, currentStage, bSystemUseSize ? "SIZE" : "PRESSURE");
        }
        if ((bTouchSizeChecking) && (bSystemDriverHaveSize)) {
            if (bSystemUseSize)
                ptsize = motionEvent.getSize(motionEvent.getActionIndex());
            else ptsize = motionEvent.getPressure(motionEvent.getActionIndex());
        }
        if ((action == MotionEvent::ACTION_DOWN) || (action == MotionEvent::ACTION_POINTER_DOWN)) {
            lastTouchedID = motionEvent.getPointerId(motionEvent.getActionIndex());
            markedPenDownTime = currentTimeMillis();
            iPenTouchDistance=0;
        }
        if (systemPenID == -1) {
            if ((currentStage != SONAR_PEN_PRESSED) && ((action == MotionEvent::ACTION_DOWN) || (action == MotionEvent::ACTION_POINTER_DOWN))) {
                iWaitTimeMark = currentTimeMillis();
                while ((currentTimeMillis() - iWaitTimeMark) < 100) {
                    if ((bIsPaused) || (bNeedCalibrate) || (!bRunning) || (!bIsHeadphonesPlugged))
                        break;
                }
                sonarPenReading();
                markedPenDownTime = currentTimeMillis();
            }
            int tID = motionEvent.getPointerId(motionEvent.getActionIndex());
            long currentTime = currentTimeMillis();
            if (bShowDebugLogCat) {
                SLOGD("after tID=%d current=%f, %d, %f/%f:%ld", tID, currentAmp, currentStage, lastAmp, prevAmp, (currentTime - markedPenDownTime));
            }
            if (currentStage == SONAR_PEN_PRESSED) {
                if (systemPenID == -1) {
                    if ((lastTouchedID == tID) && (((action == MotionEvent::ACTION_MOVE) && (currentTime - markedPenDownTime < skip_touch_down_count)) || (action != MotionEvent::ACTION_MOVE))) {
                        prevPressure = -1;
                        systemPenID = tID;
                        lastTouchedID = -1;
                        if ((bSkipTouchDown) && (action == MotionEvent::ACTION_MOVE)) {
                            motionEvent.setAction((motionEvent.getPointerCount()>1)?MotionEvent::ACTION_POINTER_DOWN:MotionEvent::ACTION_DOWN);
                        }
                        if ((bTouchSizeChecking) && (bSystemDriverHaveSize) && (ptsize > 0) && (markedPenSize == 999)) {
                            markedPenSize = ptsize;
                        }
                        if (action == MotionEvent::ACTION_MOVE) {
                            lastAmp = (float) prevAmp;
                        }

                        if (bFirstTouchMinAmp) {
                            float mamp = calculatedMinAmp(lastAmp);
                            if (mamp < touchMinAmp) {
                                touchMinAmp = mamp;
                                minAmp = mamp;
                                bFirstTouchMinAmp=false;
                            }
                        }

                    } else if ((action == MotionEvent::ACTION_MOVE) && (lastTouchedID == tID) && (currentTime - markedPenDownTime >= skip_touch_down_count)) {
                        motionEvent.setAction((motionEvent.getPointerCount()>1)?MotionEvent::ACTION_POINTER_DOWN:MotionEvent::ACTION_DOWN);
                        lastTouchedID = -1;
                    }
                }
            } else if (action == MotionEvent::ACTION_MOVE) {
                if (currentTime - markedPenDownTime < skip_touch_down_count*2) {
                    if (bShowDebugLogCat) {
                        SLOGD("move %d, %d, %d, %f, %f, %f", iPenTouchDistance, currentTime - markedPenDownTime, tID, currentAmp, prevAmp, touchMinAmp);
                    }
                    iPenTouchDistance++;
                    if (currentAmp < prevAmp) prevAmp = currentAmp;
                    if ((currentTime - markedPenDownTime >= skip_touch_down_count) && (tID == lastTouchedID) && (bSkipTouchDown)) {
                        motionEvent.setAction((motionEvent.getPointerCount()>1)?MotionEvent::ACTION_POINTER_DOWN:MotionEvent::ACTION_DOWN);
                        lastTouchedID=-1;
                    }
                } else if ((bTouchSizeChecking) && (bSystemDriverHaveSize) && (markedPenSize != 999) && (ptsize != 0) && ((markedPenSize > 0.01) && (ptsize > markedPenSize*penSizeAllows) || ((markedPenSize < 0.01) && (ptsize > markedPenSize*penSizeAllowsSp)))) {
                    if (bShowDebugLogCat) {
                        SLOGD("size %d, %d, %d, %f, %f, %f", iPenTouchDistance, currentTime - markedPenDownTime, tID, currentAmp, prevAmp, touchMinAmp);
                    }
                    lastTouchedID = -1;
                    if (bOnlySonarPenTouchEvent) {
                        motionEvent.setAction(MotionEvent::ACTION_CANCEL);
                        return skipMotionEvent(motionEvent.getMotionEvent());
                    }
                    return motionEvent.getMotionEvent();
                }
            }
        }
    } else if (((action == MotionEvent::ACTION_POINTER_UP) || (action == MotionEvent::ACTION_UP))) {
        if (bIsSonicPen) {
            int prevSystemPenID = systemPenID;
            if ((action == MotionEvent::ACTION_POINTER_UP) && ((systemPenID == motionEvent.getPointerId(motionEvent.getActionIndex())) || (systemPenID == -1))) {
                if (bShowDebugLogCat) {
                    SLOGD("SONAR Pointer UP %d, system=%d", motionEvent.getPointerId(motionEvent.getActionIndex()), systemPenID);
                }
                systemPenID = -1;
                lastTouchedID = -1;
                lastTouchedSize=999;
                bOnTouch = false;
            } else if (action == MotionEvent::ACTION_UP) {
                if (bShowDebugLogCat) {
                    SLOGD("SONAR Touch Up");
                }
                systemPenID = -1;
                lastTouchedID = -1;
                lastTouchedSize=999;
                bOnTouch = false;
            }
            if ((systemPenID == -1) && (prevSystemPenID != -1)) {
                changeStage(SONAR_PEN_NOT_PRESS);
                PointerProperties* props =new PointerProperties[ptCount];
                PointerCoords* coords = new PointerCoords[ptCount];
                for (int i = 0; i < ptCount; i++) {
                    PointerProperties currProp;
                    PointerCoords currCoords;
                    motionEvent.getPointerCoords(i, &currCoords);


                    currProp.id = motionEvent.getPointerId(i);
                    currProp.toolType = motionEvent.getToolType(i);
                    if (prevSystemPenID == motionEvent.getPointerId(i)) {
                        currProp.toolType = MotionEvent::TOOL_TYPE_STYLUS;
                        currCoords.pressure = 0;
                    } else if ((bOnlySonarPenTouchEvent) && (prevSystemPenID != -1)){
                        currProp.toolType = MotionEvent::TOOL_TYPE_UNKNOWN;
                    }
                    props[i] = currProp;
                    coords[i] = currCoords;
                }
                jobject ret = MotionEvent::obtain(motionEvent.getDownTime(), motionEvent.getEventTime(), motionEvent.getAction(), ptCount, props, ptCount, coords, ptCount, motionEvent.getMetaState(), motionEvent.getButtonState(), motionEvent.getXPrecision(), motionEvent.getYPrecision(), motionEvent.getDeviceId(), motionEvent.getEdgeFlags(), motionEvent.getSource(), motionEvent.getFlags());
                delete[] props;
                delete[] coords;
                return ret;
            } else if (bOnlySonarPenTouchEvent) {
                motionEvent.setAction(MotionEvent::ACTION_CANCEL);
                return skipMotionEvent(motionEvent.getMotionEvent());
            }
        }

        return motionEvent.getMotionEvent();
    }

    if (bShowDebugLogCat) {
        SLOGD("SONAR Event Action=%s, %s, ptid=%d, lastId=%d, sysid=%d, SonicPen=%s, onTouch=%s, pressure=%f, size=%f, currAmp=%f, prevAmp=%f, touchAmp=%f, penCount=%d, Readed=%s, lastsize=%f, useTouch=%s, systemchecked=%s, Readed=%s", MotionEvent::actionToString(action).c_str(), ((currentStage == SONAR_PEN_EXIST) ? "Pen EXIST" : "-"), motionEvent.getPointerId(motionEvent.getActionIndex()), lastTouchedID, systemPenID, ((bIsSonicPen) ? "TRUE" : "FALSE"), ((bOnTouch) ? "TRUE" : "FALSE"), currPressure, motionEvent.getSize(motionEvent.getActionIndex()), currentAmp, markAmp, touchMinAmp, iPenTouchDistance, ((bReadAfterTouch) ? "true" : "false"), lastTouchedSize, (bTouchSizeChecking ? "true" : "false"), (bSystemDriverChecked ? "true" : "false"), ((bReadAfterTouch) ? "true" : "false"));
    }
    if (((action == MotionEvent::ACTION_DOWN) || (action == MotionEvent::ACTION_POINTER_DOWN)) && (systemPenID==-1) && (bSkipTouchDown)) {
        if ((action == MotionEvent::ACTION_DOWN) || (bOnlySonarPenTouchEvent)) {
            if (bOnlySonarPenTouchEvent) {
                motionEvent.setAction(MotionEvent::ACTION_CANCEL);
            }
            return skipMotionEvent(motionEvent.getMotionEvent());
        } else {
            PointerProperties* props = new PointerProperties[ptCount-1];
            PointerCoords* coords = new PointerCoords[ptCount-1];
            int j = 0;
            for (int i=0;i<ptCount;i++) {
                PointerProperties currProp;
                PointerCoords currCoords;
                motionEvent.getPointerCoords(i,&currCoords);

                currProp.id = motionEvent.getPointerId(i);
                currProp.toolType = motionEvent.getToolType(i);
                if (motionEvent.getActionIndex() != i) {
                    props[j] = currProp;
                    coords[j] = currCoords;
                    j++;
                }
            }

            jobject ret = MotionEvent::obtain(motionEvent.getDownTime(),motionEvent.getEventTime(),MotionEvent::ACTION_MOVE,ptCount-1,props,ptCount-1, coords,ptCount-1, motionEvent.getMetaState(),motionEvent.getButtonState(),motionEvent.getXPrecision(),motionEvent.getYPrecision(),0,motionEvent.getEdgeFlags(),motionEvent.getSource(),motionEvent.getFlags());
            delete[] props;
            delete[] coords;
            return ret;
        }
    }
    PointerProperties* props = new PointerProperties[ptCount];
    PointerCoords* coords = new PointerCoords[ptCount];
    PointerProperties firstProps;
    PointerCoords firstCoords;
    bool bSetted=false;

    float ePressure = currPressure;
    if (prevPressure > 0) {
        float diff = prevPressure - currPressure;
        if (pressure_smooth_type == 0) {
            diff /= 2;
        }
        else if (pressure_smooth_type == 1) {
            diff = diff * 2 / 3;
        }
        else {
            diff = diff * 3 / 4;
        }
        ePressure += diff;
        if (ePressure < 0) ePressure = 0;
        if (ePressure > 1) ePressure = 1;
    }
    if (systemPenID != -1) prevPressure = ePressure;
    if (bShowDebugLogCat) {
        SLOGD("ePressure=%f, Prev:%f, curr:%f", ePressure, prevPressure, currPressure);
    }
    for (int i=0;i<ptCount;i++) {
        PointerProperties currProp;
        PointerCoords currCoords;
        motionEvent.getPointerCoords(i,&currCoords);

        currProp.id = motionEvent.getPointerId(i);
        currProp.toolType = motionEvent.getToolType(i);

        if (currentStage == SONAR_PEN_PRESSED) {
            if ((bOnTouch) && (currProp.toolType == MotionEvent::TOOL_TYPE_FINGER)) {
                if (systemPenID == motionEvent.getPointerId(i)) {
                    if ((bTouchSizeChecking) && (bSystemDriverHaveSize)) {
                        if (bSystemUseSize) {
                            if (markedPenSize > motionEvent.getSize(i)) {
                                markedPenSize = motionEvent.getSize(i);
                            }
                        } else {
                            if (markedPenSize > motionEvent.getPressure(i)) {
                                markedPenSize = motionEvent.getPressure(i);
                            }
                        }
                    }
                    currProp.toolType = MotionEvent::TOOL_TYPE_STYLUS;
                    currCoords.pressure = ePressure > 0.011f? ePressure : 0.011f;
                    bSetted=true;
                    if (bIsInCalibrating) {
                        currCoords.size = markedCurrentAmp;
                    }
                } else if (bOnlySonarPenTouchEvent) {
                    currProp.toolType = MotionEvent::TOOL_TYPE_UNKNOWN;
                    currCoords.pressure = 0;
                    currCoords.size=0;
                }
            }
        }
        if (i==0) {
            firstProps = currProp;
            firstCoords = currCoords;
        }

        if (bOnlySonarPenTouchEvent) {
            if (currProp.toolType == MotionEvent::TOOL_TYPE_STYLUS) {
                props[0] = currProp;
                coords[0] = currCoords;
            }
        } else {
            props[i] = currProp;
            coords[i] = currCoords;
        }
    }
    jobject ret;
    if (bOnlySonarPenTouchEvent) {
        if (! bSetted) {
            props[0] = firstProps;
            coords[0] = firstCoords;
            motionEvent.setAction(MotionEvent::ACTION_CANCEL);
        }
        ret = MotionEvent::obtain(motionEvent.getDownTime(),motionEvent.getEventTime(),motionEvent.getAction(),1,props,ptCount,coords,ptCount,motionEvent.getMetaState(),motionEvent.getButtonState(),motionEvent.getXPrecision(),motionEvent.getYPrecision(),motionEvent.getDeviceId(),motionEvent.getEdgeFlags(),motionEvent.getSource(),motionEvent.getFlags());
    } else {
        ret = MotionEvent::obtain(motionEvent.getDownTime(), motionEvent.getEventTime(),
                                  motionEvent.getAction(), ptCount, props, ptCount, coords, ptCount,
                                  motionEvent.getMetaState(), motionEvent.getButtonState(),
                                  motionEvent.getXPrecision(), motionEvent.getYPrecision(),
                                  motionEvent.getDeviceId(), motionEvent.getEdgeFlags(),
                                  motionEvent.getSource(), motionEvent.getFlags());
    }
    delete[] props;
    delete[] coords;
    return ret;
}

bool SonarPenUtilities::ChangePressureSmoothOption(int slevel) {
    if ((slevel >= 0) && (slevel <= 2)) {
        pressure_smooth_type = slevel;

        return true;
    }

    return false;
}

void SonarPenUtilities::registerHeadsetPlugReceiver() {
    if (mainThreadContext != nullptr && !isRegisteredReceiver) {
        JNIEnv *env = getEnv();
        jclass contextClass = env->GetObjectClass(mainThreadContext);
        jclass intentFilterClass = env->FindClass("android/content/IntentFilter");
        jmethodID intentFilterConstructor = env->GetMethodID(intentFilterClass, "<init>", "(Ljava/lang/String;)V");
        jobject intentFilter = env->NewObject(intentFilterClass, intentFilterConstructor, env->NewStringUTF("android.intent.action.HEADSET_PLUG"));

        jclass broadcastReceiverClass = env->FindClass("com/greenbulb/sonarpen/SonarPenListener");
        jobject broadcastReceiver = env->NewObject(broadcastReceiverClass, env->GetMethodID(broadcastReceiverClass, "<init>", "()V"));
        sonarPenListenerInstance = env->NewGlobalRef(broadcastReceiver);

        if (SDK_INT() >= __ANDROID_API_O__) {
            jmethodID registerReceiverMethod = env->GetMethodID(contextClass, "registerReceiver", "(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;");
            env->CallObjectMethod(mainThreadContext, registerReceiverMethod, broadcastReceiver, intentFilter, 2); // Context.RECEIVER_EXPORTED
        } else {
            jmethodID registerReceiverMethod = env->GetMethodID(contextClass, "registerReceiver", "(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;");
            env->CallObjectMethod(mainThreadContext, registerReceiverMethod, broadcastReceiver, intentFilter);
        }
        checkAndClearException(env);
        isRegisteredReceiver = true;
    }
}

void SonarPenUtilities::unregisterHeadsetPlugReceiver() {
    if (mainThreadContext != nullptr && isRegisteredReceiver) {
        JNIEnv *env = getEnv();
        jclass contextClass = env->GetObjectClass(mainThreadContext);
        jmethodID unregisterReceiverMethod = env->GetMethodID(contextClass, "unregisterReceiver", "(Landroid/content/BroadcastReceiver;)V");
        env->CallVoidMethod(mainThreadContext, unregisterReceiverMethod, sonarPenListenerInstance);
        checkAndClearException(env);
        isRegisteredReceiver = false;
        env->DeleteGlobalRef(sonarPenListenerInstance);
        sonarPenListenerInstance = nullptr;
    }
}

bool SonarPenUtilities::updateIsHeadphonesPlugged() {
    JNIEnv *env = getEnv();
    jclass contextClass = env->GetObjectClass(mainThreadContext);
    jclass audioManagerClass = env->FindClass("android/media/AudioManager");
    jmethodID getSystemServiceMethod = env->GetMethodID(contextClass, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");
    jobject audioManager = env->CallObjectMethod(mainThreadContext, getSystemServiceMethod, env->NewStringUTF("audio"));

    if (audioManager == nullptr) return false;

    bIsHeadphonesPlugged = false;
    if (SDK_INT() >= 23) {
        jmethodID getDevicesMethod = env->GetMethodID(audioManagerClass, "getDevices", "(I)[Landroid/media/AudioDeviceInfo;");
        jobjectArray audioDevices = (jobjectArray) env->CallObjectMethod(audioManager, getDevicesMethod, 3); // AudioManager.GET_DEVICES_INPUTS
        jsize length = env->GetArrayLength(audioDevices);
        for (int i = 0; i < length; i++) {
            jobject deviceInfo = env->GetObjectArrayElement(audioDevices, i);
            jclass deviceInfoClass = env->GetObjectClass(deviceInfo);
            jmethodID getTypeMethod = env->GetMethodID(deviceInfoClass, "getType", "()I");
            jint type = env->CallIntMethod(deviceInfo, getTypeMethod);
            if (type == 22 | type == 4 || type == 3) { // AudioDeviceInfo.TYPE_USB_HEADSET | AudioDeviceInfo.TYPE_WIRED_HEADPHONES | AudioDeviceInfo.TYPE_WIRED_HEADSET
                if (type == 22) { // AudioDeviceInfo.TYPE_USB_HEADSET
                    jmethodID getProductNameMethod = env->GetMethodID(deviceInfoClass,
                                                                      "getProductName",
                                                                      "()Ljava/lang/CharSequence;");
                    jobject productName = env->CallObjectMethod(deviceInfo, getProductNameMethod);
                    jclass productNameClass = env->GetObjectClass(productName);
                    jmethodID toStringMethod = env->GetMethodID(productNameClass, "toString",
                                                                "()Ljava/lang/String;");
                    jstring productNameStr = (jstring) env->CallObjectMethod(productName,
                                                                             toStringMethod);

                    const char *productNameChars = env->GetStringUTFChars((jstring) productNameStr,
                                                                          nullptr);
                    if (strstr(productNameChars, "SP1") != nullptr) {
                        bIsHeadphonesPlugged = true;
                        bIsSonarPenUSB = true;
                    }
                    env->ReleaseStringUTFChars((jstring) productName, productNameChars);
                    env->DeleteLocalRef(productNameStr);
                    env->DeleteLocalRef(productNameClass);
                    env->DeleteLocalRef(productName);
                } else {
                    jclass usbManagerClass = env->FindClass("android/hardware/usb/UsbManager");
                    jmethodID getSystemServiceMethod = env->GetMethodID(contextClass,
                                                                        "getSystemService",
                                                                        "(Ljava/lang/String;)Ljava/lang/Object;");
                    jobject usbManager = env->CallObjectMethod(mainThreadContext,
                                                               getSystemServiceMethod,
                                                               env->NewStringUTF("usb"));
                    jmethodID getDeviceListMethod = env->GetMethodID(usbManagerClass,
                                                                     "getDeviceList",
                                                                     "()Ljava/util/HashMap;");
                    jobject usbDevices = env->CallObjectMethod(usbManager, getDeviceListMethod);
                    jclass hashMapClass = env->FindClass("java/util/HashMap");
                    jmethodID valuesMethod = env->GetMethodID(hashMapClass, "values",
                                                              "()Ljava/util/Collection;");
                    jobject values = env->CallObjectMethod(usbDevices, valuesMethod);
                    jclass collectionClass = env->FindClass("java/util/Collection");
                    jmethodID toArrayMethod = env->GetMethodID(collectionClass, "toArray",
                                                               "()[Ljava/lang/Object;");
                    jobjectArray usbArray = (jobjectArray) env->CallObjectMethod(values,
                                                                                 toArrayMethod);
                    jsize usbLength = env->GetArrayLength(usbArray);
                    for (int i = 0; i < usbLength; i++) {
                        jobject usb = env->GetObjectArrayElement(usbArray, i);
                        jclass usbClass = env->GetObjectClass(usb);
                        if (SDK_INT() >= __ANDROID_API_L__) {
                            jmethodID getProductNameMethod = env->GetMethodID(usbClass,
                                                                              "getProductName",
                                                                              "()Ljava/lang/String;");
                            jstring productName = (jstring) env->CallObjectMethod(usb,
                                                                                  getProductNameMethod);
                            const char *productNameChars = env->GetStringUTFChars(productName,
                                                                                  nullptr);
                            if (strstr(productNameChars, "SP1") != nullptr) {
                                bIsSonarPenUSB = true;
                                env->ReleaseStringUTFChars(productName, productNameChars);
                                env->DeleteLocalRef(productName);
                                env->DeleteLocalRef(usb);
                                env->DeleteLocalRef(usbClass);
                                break;
                            }
                            env->ReleaseStringUTFChars(productName, productNameChars);
                            env->DeleteLocalRef(productName);
                        } else {
                            jmethodID toStringMethod = env->GetMethodID(usbClass, "toString",
                                                                        "()Ljava/lang/String;");
                            jstring usbtostr = (jstring) env->CallObjectMethod(usb, toStringMethod);
                            const char *usbtostrChars = env->GetStringUTFChars(usbtostr, nullptr);
                            int idx = std::string(usbtostrChars).find("mProductName=");
                            if (idx != std::string::npos) {
                                idx += 13;
                                std::string pname = std::string(usbtostrChars).substr(idx);
                                idx = pname.find(',');
                                if (idx != std::string::npos) {
                                    pname = pname.substr(0, idx);
                                }
                                if (pname.find("SP1") != std::string::npos) {
                                    bIsSonarPenUSB = true;
                                    env->ReleaseStringUTFChars(usbtostr, usbtostrChars);
                                    env->DeleteLocalRef(usbtostr);
                                    env->DeleteLocalRef(usb);
                                    env->DeleteLocalRef(usbClass);
                                    break;
                                }
                            }
                            env->ReleaseStringUTFChars(usbtostr, usbtostrChars);
                            env->DeleteLocalRef(usbtostr);
                        }
                    }
                    bIsHeadphonesPlugged = true;
                }
            }
        }

    } else {
        jmethodID isWiredHeadsetOnMethod = env->GetMethodID(audioManagerClass, "isWiredHeadsetOn", "()Z");
        bIsHeadphonesPlugged = env->CallBooleanMethod(audioManager, isWiredHeadsetOnMethod);
    }
    return true;
}

void SonarPenUtilities::addSonarPenToView(jobject v) {
    SLOGI("addSonarPenToView");
    JNIEnv *env = getEnv();
    jclass viewClass = env->GetObjectClass(v);
    jmethodID getIDMethod = env->GetMethodID(viewClass, "getId", "()I");
    jint viewID = env->CallIntMethod(v, getIDMethod);

    if (views.viewIDExist(viewID)) {
        return;
    } else {
        views.addViewID(viewID);
    }

#if 0
    jclass SPOnKeyListenerClass = env->FindClass("com/greenbulb/sonarpen/SPOnKeyListener");
    jmethodID constructor = env->GetMethodID(SPOnKeyListenerClass, "<init>", "()V");
    jobject spOnKeyListener = env->NewObject(SPOnKeyListenerClass, constructor);
    env->CallVoidMethod(v, env->GetMethodID(viewClass, "setOnKeyListener", "(Landroid/view/View$OnKeyListener;)V"), spOnKeyListener);
    env->DeleteLocalRef(SPOnKeyListenerClass);
    env->DeleteLocalRef(spOnKeyListener);
#endif

    jclass SPOnTouchListenerClass = env->FindClass("com/greenbulb/sonarpen/SPOnTouchListener");
    jmethodID constructor = env->GetMethodID(SPOnTouchListenerClass, "<init>", "()V");
    jobject spOnTouchListener = env->NewObject(SPOnTouchListenerClass, constructor);
    env->CallVoidMethod(v, env->GetMethodID(viewClass, "setOnTouchListener", "(Landroid/view/View$OnTouchListener;)V"), spOnTouchListener);
    env->DeleteLocalRef(SPOnTouchListenerClass);
    env->DeleteLocalRef(spOnTouchListener);

    env->DeleteLocalRef(viewClass);
}

int SonarPenUtilities::registerNativeMethods(JNIEnv *env) {
    // SonarPenFileObserver registration
    jclass SonarPenFileObserverClass = env->FindClass("com/greenbulb/sonarpen/SonarPenFileObserver");
    static JNINativeMethod  SonarPenFileObserverMethods[] = {
            {"onEvent", "(ILjava/lang/String;)V", (void *) &SonarPenFileObserver_onEvent}
    };
    if (env->RegisterNatives(SonarPenFileObserverClass, SonarPenFileObserverMethods, sizeof(SonarPenFileObserverMethods) / sizeof(SonarPenFileObserverMethods[0])) < 0) {
        return JNI_ERR;
    }

    // ProfileSyncReceiver registration
    jclass ProfileSyncReceiverClass = env->FindClass("com/greenbulb/sonarpen/ProfileSyncReceiver");
    static JNINativeMethod ProfileSyncReceiverMethods[] = {
            {"onReceive", "(Landroid/content/Context;Landroid/content/Intent;)V", (void *) &ProfileSyncReceiver_onReceive}
    };
    if (env->RegisterNatives(ProfileSyncReceiverClass, ProfileSyncReceiverMethods, sizeof(ProfileSyncReceiverMethods) / sizeof(ProfileSyncReceiverMethods[0])) < 0) {
        return JNI_ERR;
    }

    // SonarPenListener registration
    jclass SonarPenListenerClass = env->FindClass("com/greenbulb/sonarpen/SonarPenListener");
    static JNINativeMethod SonarPenListenerMethods[] = {
            {"onReceive", "(Landroid/content/Context;Landroid/content/Intent;)V", (void *) &SonarPenListener_onReceive}
    };
    if (env->RegisterNatives(SonarPenListenerClass, SonarPenListenerMethods, sizeof(SonarPenListenerMethods) / sizeof(SonarPenListenerMethods[0])) < 0) {
        return JNI_ERR;
    }

    // SPOnKeyListener registration
#if 0
    jclass SPOnKeyListenerClass = env->FindClass("com/greenbulb/sonarpen/SPOnKeyListener");
    static JNINativeMethod SPOnKeyListenerMethods[] = {
            {"onKey", "(Landroid/view/View;ILandroid/view/KeyEvent;)Z", (void *) &SPOnKeyListener_onKey}
    };
    if (env->RegisterNatives(SPOnKeyListenerClass, SPOnKeyListenerMethods, sizeof(SPOnKeyListenerMethods) / sizeof(SPOnKeyListenerMethods[0])) < 0) {
        return JNI_ERR;
    }
#endif

    // SPOnTouchListener registration
    jclass SPOnTouchListenerClass = env->FindClass("com/greenbulb/sonarpen/SPOnTouchListener");
    static JNINativeMethod SPOnTouchListenerMethods[] = {
            {"onTouch", "(Landroid/view/View;Landroid/view/MotionEvent;)Z", (void *) &SPOnTouchListener_onTouch}
    };
    if (env->RegisterNatives(SPOnTouchListenerClass, SPOnTouchListenerMethods, sizeof(SPOnTouchListenerMethods) / sizeof(SPOnTouchListenerMethods[0])) < 0) {
        return JNI_ERR;
    }
    return 0;
}

void SonarPenUtilities::ProfileSyncReceiver_onReceive(JNIEnv *env, jobject thiz, jobject context,
                                                      jobject intent) {
    if (SonarPenUtilities::getInstance()->isInitialized) {
        jclass intentClass = env->GetObjectClass(intent);
        jmethodID getStringExtraMethod = env->GetMethodID(intentClass, "getStringExtra", "(Ljava/lang/String;)Ljava/lang/String;");
        jstring toName = (jstring) env->CallObjectMethod(intent, getStringExtraMethod, env->NewStringUTF("to_name"));
        jstring fromName = (jstring) env->CallObjectMethod(intent, getStringExtraMethod, env->NewStringUTF("sender_name"));
        const char *toNameChars = env->GetStringUTFChars(toName, nullptr);
        const char *fromNameChars = env->GetStringUTFChars(fromName, nullptr);

        jclass mainThreadContextClass = env->GetObjectClass(SonarPenUtilities::getInstance()->mainThreadContext);
        jmethodID getPackageNameMethod = env->GetMethodID(mainThreadContextClass, "getPackageName", "()Ljava/lang/String;");
        jstring pkgName = (jstring) env->CallObjectMethod(SonarPenUtilities::getInstance()->mainThreadContext, getPackageNameMethod);
        const char *pkgNameChars = env->GetStringUTFChars(pkgName, nullptr);

        if ((stringEqualIgnoreCase(toNameChars, "all") && !stringEqualIgnoreCase(fromNameChars, pkgNameChars)) ||
            stringEqualIgnoreCase(toNameChars, pkgNameChars)) {
            jstring profile = (jstring) env->CallObjectMethod(intent, getStringExtraMethod, env->NewStringUTF("profile"));
            const char *profileChars = env->GetStringUTFChars(profile, nullptr);
            jmethodID getSharedPreferencesMethod = env->GetMethodID(mainThreadContextClass, "getSharedPreferences", "(Ljava/lang/String;I)Landroid/content/SharedPreferences;");
            jobject sh = env->CallObjectMethod(SonarPenUtilities::getInstance()->mainThreadContext, getSharedPreferencesMethod, pkgName, 0); //Context.MODE_PRIVATE
            jclass shClass = env->GetObjectClass(sh);
            jmethodID editMethod = env->GetMethodID(shClass, "edit", "()Landroid/content/SharedPreferences$Editor;");
            jobject ed = env->CallObjectMethod(sh, editMethod);
            jclass edClass = env->GetObjectClass(ed);
            if (strlen(profileChars) > 0) {
                jmethodID putStringMethod = env->GetMethodID(edClass, "putString",
                                                             "(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;");
                jstring shProfileName = env->NewStringUTF(
                        SonarPenUtilities::getInstance()->sh_profile_name);
                env->CallObjectMethod(ed, putStringMethod, shProfileName, profile);
            } else {
                jmethodID removeMethod = env->GetMethodID(edClass, "remove",
                                                          "(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;");
                jstring shProfileName = env->NewStringUTF(
                        SonarPenUtilities::getInstance()->sh_profile_name);
                env->CallObjectMethod(ed, removeMethod, shProfileName);
            }

            jmethodID putBooleanMethod = env->GetMethodID(edClass, "putBoolean",
                                                         "(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;");
            jstring shSyncLoaded = env->NewStringUTF(SonarPenUtilities::getInstance()->sh_sync_loaded);
            env->CallObjectMethod(ed, putBooleanMethod, shSyncLoaded, true);

            jmethodID commitMethod = env->GetMethodID(edClass, "commit", "()Z");
            env->CallBooleanMethod(ed, commitMethod);
            SonarPenUtilities::getInstance()->readInSyncProfile(profileChars);

            env->DeleteLocalRef(shClass);
            env->DeleteLocalRef(sh);
            env->DeleteLocalRef(edClass);
            env->DeleteLocalRef(ed);
            env->ReleaseStringUTFChars(profile, profileChars);
        }
        env->ReleaseStringUTFChars(toName, toNameChars);
        env->ReleaseStringUTFChars(fromName, fromNameChars);
        env->ReleaseStringUTFChars(pkgName, pkgNameChars);
        env->DeleteLocalRef(mainThreadContextClass);
        env->DeleteLocalRef(intentClass);
    }
}

void SonarPenUtilities::SonarPenListener_onReceive(JNIEnv *env, jobject thiz, jobject context,
                                                   jobject intent) {
    if (SonarPenUtilities::getInstance()->isInitialized) {
        jclass intentClass = env->GetObjectClass(intent);
        jmethodID getActionMethod = env->GetMethodID(intentClass, "getAction", "()Ljava/lang/String;");
        jstring action = (jstring) env->CallObjectMethod(intent, getActionMethod);
        const char *actionChars = env->GetStringUTFChars(action, nullptr);
        if (strcmp("android.intent.action.HEADSET_PLUG", actionChars) == 0) {
            bool prevPlugged = SonarPenUtilities::getInstance()->bIsHeadphonesPlugged;
            jmethodID getIntExtraMethod = env->GetMethodID(intentClass, "getIntExtra", "(Ljava/lang/String;I)I");
            jint intVal = env->CallIntMethod(intent, getIntExtraMethod, env->NewStringUTF("state"), -1);
            SonarPenUtilities::getInstance()->bIsHeadphonesPlugged = intVal > 0;
            if (prevPlugged != SonarPenUtilities::getInstance()->bIsHeadphonesPlugged) {
                if (SonarPenUtilities::getInstance()->bIsHeadphonesPlugged) {
                    SonarPenUtilities::getInstance()->onHeadsetPlugged();
                } else {
                    SonarPenUtilities::getInstance()->onHeadsetUnplugged();
                }
            }
        }
        env->ReleaseStringUTFChars(action, actionChars);
        env->DeleteLocalRef(intentClass);
    }
}

void SonarPenUtilities::readInSyncProfile(const char *s) {
    bHaveManualSetting=false;
    bManualCalVol = false;

    if (strlen(s) > 0) {
        JNIEnv *env = getEnv();
        jclass JSONObjectClass = env->FindClass("org/json/JSONObject");
        jmethodID JSONObjectConstructor = env->GetMethodID(JSONObjectClass, "<init>", "(Ljava/lang/String;)V");
        jobject o = env->NewObject(JSONObjectClass, JSONObjectConstructor, env->NewStringUTF(s));
        checkAndClearException(env);

        jmethodID hasMethod = env->GetMethodID(JSONObjectClass, "has", "(Ljava/lang/String;)Z");
        jmethodID getDoubleMethod = env->GetMethodID(JSONObjectClass, "getDouble", "(Ljava/lang/String;)D");

        if (env->CallBooleanMethod(o, hasMethod, env->NewStringUTF("minAmp")) &&
            env->CallBooleanMethod(o, hasMethod, env->NewStringUTF("maxAmp"))) {
            bHaveManualSetting = true;
            ManualMinAmp = (float) env->CallDoubleMethod(o, getDoubleMethod, env->NewStringUTF("minAmp"));
            ManualMaxAmp = (float) env->CallDoubleMethod(o, getDoubleMethod, env->NewStringUTF("maxAmp"));
            if ((ManualMinAmp == 0) && (ManualMaxAmp == 0)) {
                bHaveManualSetting = false;
            }
        } else {
            bHaveManualSetting = false;
        }

        if (env->CallBooleanMethod(o, hasMethod, env->NewStringUTF("maxVol"))) {
            MaxVolPercentage = (float) env->CallDoubleMethod(o, getDoubleMethod, env->NewStringUTF("maxVol"));
            if (MaxVolPercentage < 100) {
                bManualCalVol = true;
                maximizeStreamVolume();
            } else {
                bManualCalVol = false;
            }
        } else {
            bManualCalVol = false;
        }

        if (env->CallBooleanMethod(o, hasMethod, env->NewStringUTF("lowFeq"))) {
            checkFeq = 250.0f;
            bByPassDetection = true;
            DefaultByPassDetection = true;
        }

        env->DeleteLocalRef(JSONObjectClass);
        env->DeleteLocalRef(o);
        checkAndClearException(env);
    }
}

void SonarPenUtilities::getByPassDevice() {
    JNIEnv *env = getEnv();
    jobject cursor = nullptr;
    jclass mainThreadContextClass = env->GetObjectClass(mainThreadContext);
    jmethodID getContentResolverMethod = env->GetMethodID(mainThreadContextClass, "getContentResolver", "()Landroid/content/ContentResolver;");
    jobject contentResolver = env->CallObjectMethod(mainThreadContext, getContentResolverMethod);
    jclass contentResolverClass = env->FindClass("android/content/ContentResolver");
    jobject uri = env->CallStaticObjectMethod(env->FindClass("android/net/Uri"), env->GetStaticMethodID(env->FindClass("android/net/Uri"), "parse", "(Ljava/lang/String;)Landroid/net/Uri;"), env->NewStringUTF("content://sonarpen.devicecompat/data"));
    if (SDK_INT() >= __ANDROID_API_O__) {
        jmethodID queryMethod = env->GetMethodID(contentResolverClass, "query", "(Landroid/net/Uri;[Ljava/lang/String;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Landroid/database/Cursor;");
        cursor = env->CallObjectMethod(contentResolver, queryMethod, uri, nullptr, nullptr, nullptr);
    } else {
        jmethodID queryMethod = env->GetMethodID(contentResolverClass, "query", "(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;");
        cursor = env->CallObjectMethod(contentResolver, queryMethod, uri, nullptr, nullptr, nullptr, nullptr);
    }
    env->DeleteLocalRef(contentResolverClass);
    env->DeleteLocalRef(contentResolver);
    env->DeleteLocalRef(uri);

    if (cursor != nullptr) {
        jclass cursorClass = env->GetObjectClass(cursor);
        jmethodID getCountMethod = env->GetMethodID(cursorClass, "getCount", "()I");
        jint count = env->CallIntMethod(cursor, getCountMethod);
        if (count > 0) {
            jmethodID moveToFirstMethod = env->GetMethodID(cursorClass, "moveToFirst", "()Z");
            env->CallBooleanMethod(cursor, moveToFirstMethod);
            jmethodID getStringMethod = env->GetMethodID(cursorClass, "getString",
                                                         "(I)Ljava/lang/String;");
            jstring s = (jstring) env->CallObjectMethod(cursor, getStringMethod, 0);
            const char *sChars = env->GetStringUTFChars(s, nullptr);

            SharedPreferences* sh = SharedPreferences::getSonarPenSharedPref(mainThreadContext);
            SharedPreferencesEditor* ed = sh->edit();
            if (stringEqualIgnoreCase(sChars, "NO")) {
                ed->putBool(sh_bypass_device, false);
            } else {
                ed->putBool(sh_bypass_device, true);
            }
            ed->commit();
            delete sh;
        }

        jmethodID closeMethod = env->GetMethodID(cursorClass, "close", "()V");
        env->CallVoidMethod(cursor, closeMethod);

        env->DeleteLocalRef(cursorClass);
        env->DeleteLocalRef(cursor);
    }
    checkAndClearException(env);
}

void SonarPenUtilities::getProfileFromContentProvider() {
    JNIEnv *env = getEnv();
    jobject cursor = nullptr;
    jclass mainThreadContextClass = env->GetObjectClass(mainThreadContext);
    jmethodID getContentResolverMethod = env->GetMethodID(mainThreadContextClass, "getContentResolver", "()Landroid/content/ContentResolver;");
    jobject contentResolver = env->CallObjectMethod(mainThreadContext, getContentResolverMethod);
    jclass contentResolverClass = env->FindClass("android/content/ContentResolver");
    jobject uri = env->CallStaticObjectMethod(env->FindClass("android/net/Uri"), env->GetStaticMethodID(env->FindClass("android/net/Uri"), "parse", "(Ljava/lang/String;)Landroid/net/Uri;"), env->NewStringUTF("content://sonarpen.calibrate/data"));
    if (SDK_INT() >= __ANDROID_API_O__) {
        jmethodID queryMethod = env->GetMethodID(contentResolverClass, "query", "(Landroid/net/Uri;[Ljava/lang/String;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Landroid/database/Cursor;");
        cursor = env->CallObjectMethod(contentResolver, queryMethod, uri, nullptr, nullptr, nullptr, nullptr);
    } else {
        jmethodID queryMethod = env->GetMethodID(contentResolverClass, "query", "(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;");
        cursor = env->CallObjectMethod(contentResolver, queryMethod, uri, nullptr, nullptr, nullptr, nullptr);
    }
    env->DeleteLocalRef(contentResolverClass);
    env->DeleteLocalRef(contentResolver);
    env->DeleteLocalRef(uri);

    if (cursor != nullptr) {
        jclass cursorClass = env->GetObjectClass(cursor);
        jmethodID getCountMethod = env->GetMethodID(cursorClass, "getCount", "()I");
        jint count = env->CallIntMethod(cursor, getCountMethod);
        if (count > 0) {
            jmethodID moveToFirstMethod = env->GetMethodID(cursorClass, "moveToFirst", "()Z");
            env->CallBooleanMethod(cursor, moveToFirstMethod);
            jmethodID getStringMethod = env->GetMethodID(cursorClass, "getString",
                                                         "(I)Ljava/lang/String;");
            jstring s = (jstring) env->CallObjectMethod(cursor, getStringMethod, 0);
            const char *sChars = env->GetStringUTFChars(s, nullptr);

            SharedPreferences* sh = SharedPreferences::getSonarPenSharedPref(mainThreadContext);
            SharedPreferencesEditor* ed = sh->edit();
            if (strlen(sChars) > 0) ed->putString(sh_profile_name, sChars);
            else ed->remove(sh_profile_name);

            ed->putBool(sh_sync_loaded, true);
            ed->commit();
            delete sh;

            readInSyncProfile(sChars);

            env->ReleaseStringUTFChars(s, sChars);
            env->DeleteLocalRef(s);
        }

        jmethodID closeMethod = env->GetMethodID(cursorClass, "close", "()V");
        env->CallVoidMethod(cursor, closeMethod);

        env->DeleteLocalRef(cursorClass);
        env->DeleteLocalRef(cursor);
    }
    checkAndClearException(env);
}
#if 0
jboolean SonarPenUtilities::SPOnKeyListener_onKey(JNIEnv *env, jobject thiz, jobject v, jint keycode, jobject event) {
    jclass eventClass = env->GetObjectClass(event);
    jmethodID getActionMethod = env->GetMethodID(eventClass, "getAction", "()I");
    jint action = env->CallIntMethod(event, getActionMethod);
    SLOGI("SPOnKeyListener_onKey-> keycode: %d, action: %d", keycode, action);
    if (action == 0) { //KeyEvent.ACTION_DOWN
        SonarPenUtilities::getInstance()->onKeyDown(keycode);
       if (SonarPenUtilities::getInstance()->isSonicPenButton(event)) return true;
    } else if (action == 1) { //KeyEvent.ACTION_UP
        SonarPenUtilities::getInstance()->onKeyUp(keycode);
        if (SonarPenUtilities::getInstance()->isSonicPenButton(event)) return true;
    }
    env->DeleteLocalRef(eventClass);
    return false;
}
#endif

jboolean SonarPenUtilities::SPOnTouchListener_onTouch(JNIEnv *env, jobject thiz, jobject v, jobject event) {
    jobject translatedEvent = SonarPenUtilities::getInstance()->translateTouchEvent(event);
    jclass viewClass = env->GetObjectClass(v);
    jmethodID onTouchEventMethod = env->GetMethodID(viewClass, "onTouchEvent", "(Landroid/view/MotionEvent;)Z");
    bool result = env->CallBooleanMethod(v, onTouchEventMethod, translatedEvent);
    env->DeleteLocalRef(translatedEvent);
    env->DeleteLocalRef(viewClass);
    return result;
}

void
SonarPenUtilities::SonarPenFileObserver_onEvent(JNIEnv *env, jobject thiz, int event, jstring s) {
    SonarPenUtilities *sputil = SonarPenUtilities::getInstance();
    if (sputil->isUsingFileHook()) {
        if ((event == 256) || (event == 512) || (event == 2)) // FileObserver.CREATE | FileObserver.MODIFY | FileObserver.ATTRIB
            sputil->manualFileChanged();
    }
}

void SonarPenUtilities::loadDex(jobject context) {
    JNIEnv *env = getEnv();
    auto activityClass = env->GetObjectClass(context);
    jclass classClass = env->GetObjectClass(activityClass);
    auto getClassLoaderMethod = env->GetMethodID(classClass, "getClassLoader",
                                                 "()Ljava/lang/ClassLoader;");
    jobject classLoaderIns = env->CallObjectMethod(activityClass, getClassLoaderMethod);

    jfieldID pathListFieldId = env->GetFieldID(env->GetObjectClass(classLoaderIns),
                                               "pathList",
                                               "Ldalvik/system/DexPathList;");
    jobject  pathList = env->GetObjectField(classLoaderIns, pathListFieldId);

    jfieldID dexElementsField = env->GetFieldID(env->GetObjectClass(pathList),
                                                "dexElements",
                                                "[Ldalvik/system/DexPathList$Element;");
    jobjectArray originDexElements = static_cast<jobjectArray>(env->GetObjectField(pathList, dexElementsField));
    if(originDexElements == nullptr) {
        SLOGE("originDexElements is null");
        return;
    }

    int originDexElementsLenth = env->GetArrayLength(originDexElements);
    if(originDexElementsLenth <= 0) {
        SLOGE("originDexElementsLenth is 0");
        return;
    }

    jobjectArray addDexElements = nullptr;
    jclass DexPathListClass = env->FindClass("dalvik/system/DexPathList");
    jclass arrayListClass = env->FindClass("java/util/ArrayList");
    jmethodID arrayListConstructor = env->GetMethodID(arrayListClass, "<init>", "()V");
    jobject suppressedExceptions = env->NewObject(arrayListClass, arrayListConstructor);

    if (SDK_INT() > 25) {
#if 0
        jbyteArray byteArr = env->NewByteArray((jsize)sizeof(dex_bytes));
        env->SetByteArrayRegion(byteArr, 0, sizeof(dex_bytes), reinterpret_cast<const jbyte*>(dex_bytes));
        SLOGI("dex_size: %lu", sizeof(dex_bytes));
#else
        jbyteArray byteArr = env->NewByteArray((jsize)p_dex_binSize);
        env->SetByteArrayRegion(byteArr, 0, p_dex_binSize, reinterpret_cast<const jbyte*>(p_dex_binData));
        SLOGI("dex_size: %lu", p_dex_binSize);
#endif

        jclass ByteBufferClass = env->FindClass("java/nio/ByteBuffer");
        jmethodID wrapMethod = env->GetStaticMethodID(ByteBufferClass, "wrap", "([B)Ljava/nio/ByteBuffer;");
        jobject byteBuffer = env->CallStaticObjectMethod(ByteBufferClass, wrapMethod, byteArr);

        jobjectArray bufferArray = env->NewObjectArray(1, env->FindClass("java/nio/ByteBuffer"), byteBuffer);

        jmethodID makeInMemoryDexElementsMethod = env->GetStaticMethodID(DexPathListClass, "makeInMemoryDexElements", "([Ljava/nio/ByteBuffer;Ljava/util/List;)[Ldalvik/system/DexPathList$Element;");
        addDexElements = (jobjectArray) env->CallStaticObjectMethod(DexPathListClass, makeInMemoryDexElementsMethod, bufferArray, suppressedExceptions);

        env->DeleteLocalRef(byteArr);
        env->DeleteLocalRef(ByteBufferClass);
        env->DeleteLocalRef(byteBuffer);
        env->DeleteLocalRef(bufferArray);
    } else {
        const char* content = reinterpret_cast<const char*>(p_dex_binData);
        jmethodID getCachedirMethod = env->GetMethodID(activityClass, "getCacheDir", "()Ljava/io/File;");
        jobject cacheDir = env->CallObjectMethod(context, getCachedirMethod);
        jstring fileName = env->NewStringUTF("sonarpen.dex");
        jclass fileClass = env->FindClass("java/io/File");
        jmethodID fileConstructor = env->GetMethodID(fileClass, "<init>", "(Ljava/io/File;Ljava/lang/String;)V");
        jobject jFile = env->NewObject(fileClass, fileConstructor, cacheDir, fileName);

        //save content to file
        jmethodID getAbsolutePathMethod = env->GetMethodID(fileClass, "getAbsolutePath", "()Ljava/lang/String;");
        jstring path = (jstring) env->CallObjectMethod(jFile, getAbsolutePathMethod);
        const char* pathChars = env->GetStringUTFChars(path, nullptr);
        SLOGI("dex path: %s", pathChars);

        FILE* file = fopen(pathChars, "wb");
        if(file) {
            fwrite(content, 1, p_dex_binSize, file);
            fclose(file);
        }

        jobject files = env->NewObject(arrayListClass, arrayListConstructor);
        jmethodID addMethod = env->GetMethodID(arrayListClass, "add", "(Ljava/lang/Object;)Z");
        env->CallBooleanMethod(files, addMethod, jFile);

        jmethodID makePathElementsMethod = env->GetStaticMethodID(DexPathListClass, "makePathElements", "(Ljava/util/List;Ljava/io/File;Ljava/util/List;)[Ldalvik/system/DexPathList$Element;");
        jstring out = env->NewStringUTF("out");
        jobject optimizedDir = env->CallObjectMethod(context, env->GetMethodID(activityClass, "getDir", "(Ljava/lang/String;I)Ljava/io/File;"), out, 0);
        addDexElements = (jobjectArray) env->CallStaticObjectMethod(DexPathListClass, makePathElementsMethod, files, optimizedDir, suppressedExceptions);

        env->ReleaseStringUTFChars(path, pathChars);
        env->DeleteLocalRef(optimizedDir);
        env->DeleteLocalRef(out);
        env->DeleteLocalRef(jFile);
        env->DeleteLocalRef(fileClass);
        env->DeleteLocalRef(fileName);
        env->DeleteLocalRef(cacheDir);
    }

    if(addDexElements == nullptr || !env->GetArrayLength(addDexElements)) {
        SLOGE("addDexElements is empty");
        return;
    }

    int addDexElementsLenth = env->GetArrayLength(addDexElements);
    jobjectArray newDexElements = env->NewObjectArray(originDexElementsLenth + addDexElementsLenth,
                                                      env->FindClass("dalvik/system/DexPathList$Element"),
                                                      nullptr);
    jclass SystemClass = env->FindClass("java/lang/System");
    jmethodID arraycopyMethod = env->GetStaticMethodID(SystemClass, "arraycopy", "(Ljava/lang/Object;ILjava/lang/Object;II)V");
    env->CallStaticVoidMethod(SystemClass, arraycopyMethod, originDexElements, 0, newDexElements, 0, originDexElementsLenth);
    env->CallStaticVoidMethod(SystemClass, arraycopyMethod, addDexElements, 0, newDexElements, originDexElementsLenth, addDexElementsLenth);
    env->SetObjectField(pathList, dexElementsField, newDexElements);
    SLOGE("originDexElementsLenth: %d -- addDexElementsLenth: %d -- newDexElementsLength: %d", originDexElementsLenth, addDexElementsLenth, env->GetArrayLength(newDexElements));

    env->DeleteLocalRef(activityClass);
    env->DeleteLocalRef(classClass);
    env->DeleteLocalRef(classLoaderIns);
    env->DeleteLocalRef(pathList);
    env->DeleteLocalRef(originDexElements);
    env->DeleteLocalRef(arrayListClass);
    env->DeleteLocalRef(addDexElements);
    env->DeleteLocalRef(newDexElements);
    env->DeleteLocalRef(suppressedExceptions);
    env->DeleteLocalRef(DexPathListClass);
    env->DeleteLocalRef(SystemClass);
}

void SonarPenUtilities::onKeyUp(int keyCode) {
    SLOGD("onKeyUp -> keyCode: %d", keyCode);
    if (isSonicPenButton(keyCode, KEY_ACTION_UP)) {
        SLOGD("onKeyUp -> sonic pen button released");
    } else {
        JNIEnv *env = getEnv();
        if (isStarted && (keyCode == KEYCODE_HEADSETHOOK || keyCode == KEYCODE_MEDIA_PLAY_PAUSE)) {
            jclass activityClazz = env->FindClass("android/app/Activity");
            jmethodID checkSelfPermissionMethod = env->GetMethodID(activityClazz, "checkSelfPermission", "(Ljava/lang/String;)I");
            jmethodID requestPermissionsMethod = env->GetMethodID(activityClazz, "requestPermissions", "([Ljava/lang/String;I)V");
            jstring permission = env->NewStringUTF("android.permission.RECORD_AUDIO");
            jint permissionResult = env->CallIntMethod(mainThreadContext, checkSelfPermissionMethod, permission);
            if (permissionResult != 0) {
                jclass stringClazz = env->FindClass("java/lang/String");
                jobjectArray permissions = env->NewObjectArray(1, stringClazz, nullptr);
                env->SetObjectArrayElement(permissions, 0, permission);
                env->CallVoidMethod(mainThreadContext, requestPermissionsMethod, permissions, 12345);

                env->DeleteLocalRef(stringClazz);
                env->DeleteLocalRef(permissions);
            }
            env->DeleteLocalRef(activityClazz);
            env->DeleteLocalRef(permission);
        }
    }
}

void SonarPenUtilities::onKeyDown(int keyCode) {
    SLOGD("onKeyDown -> keyCode: %d", keyCode);
    if (isSonicPenButton(keyCode, KEY_ACTION_DOWN)) {
        SLOGD("onKeyDown -> sonic pen button pressed");
    }
}

void SonarPenUtilities::onSonarPenButtonPressed() {
    SLOGD("onSonarPenButtonPressed");
    JNIEnv *env = getEnv();
    jclass qtApplicationClazz = env->FindClass("org/qtproject/qt5/android/bindings/QtApplication");
    if (qtApplicationClazz == nullptr) {
        SLOGE("qtApplicationClazz is null");
        checkAndClearException(env);
        return;
    }

    jfieldID m_delegateObjectField = env->GetStaticFieldID(qtApplicationClazz, "m_delegateObject", "Ljava/lang/Object;");
    jobject delegateObject = env->GetStaticObjectField(qtApplicationClazz, m_delegateObjectField);
    if (delegateObject != nullptr) {
        jfieldID onKeyUpField = env->GetStaticFieldID(qtApplicationClazz, "onKeyUp", "Ljava/lang/reflect/Method;");
        jfieldID onKeyDownField = env->GetStaticFieldID(qtApplicationClazz, "onKeyDown", "Ljava/lang/reflect/Method;");

        jobject onKeyUp = env->GetStaticObjectField(qtApplicationClazz, onKeyUpField);
        jobject onKeyDown = env->GetStaticObjectField(qtApplicationClazz, onKeyDownField);

        if (onKeyUp && onKeyDown) {
            jclass keyEventClazz = env->FindClass("android/view/KeyEvent");
            jmethodID keyEventConstructor = env->GetMethodID(keyEventClazz, "<init>", "(II)V");
            jobject eventDown = env->NewObject(keyEventClazz, keyEventConstructor, KEY_ACTION_DOWN, KEYCODE_TAB);
            jobject eventUp = env->NewObject(keyEventClazz, keyEventConstructor, KEY_ACTION_UP, KEYCODE_TAB);

            jmethodID invokeDelegateMethod = env->GetStaticMethodID(qtApplicationClazz, "invokeDelegateMethod", "(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;");



            jclass objectClazz = env->FindClass("java/lang/Object");
            jclass integerClass = env->FindClass("java/lang/Integer");
            jmethodID integerConstructor = env->GetMethodID(integerClass, "<init>", "(I)V");


            jobjectArray args = env->NewObjectArray(2, objectClazz, nullptr);
            jobject keycodeTab = env->NewObject(integerClass, integerConstructor, KEYCODE_TAB);
            env->SetObjectArrayElement(args, 0, keycodeTab);
            env->SetObjectArrayElement(args, 1, eventDown);

            // call onKeyDown
            env->CallStaticObjectMethod(qtApplicationClazz, invokeDelegateMethod, onKeyDown, args);

            // reset args
            env->DeleteLocalRef(args);

            args = env->NewObjectArray(2, objectClazz, nullptr);
            env->SetObjectArrayElement(args, 0, keycodeTab);
            env->SetObjectArrayElement(args, 1, eventUp);

            // call onKeyUp
            env->CallStaticObjectMethod(qtApplicationClazz, invokeDelegateMethod, onKeyUp, args);

            // release args
            env->DeleteLocalRef(args);
            env->DeleteLocalRef(keycodeTab);


            env->DeleteLocalRef(keyEventClazz);
            env->DeleteLocalRef(eventDown);
            env->DeleteLocalRef(eventUp);
        }

        if (onKeyUp) env->DeleteLocalRef(onKeyUp);
        if (onKeyDown) env->DeleteLocalRef(onKeyDown);
        env->DeleteLocalRef(delegateObject);
    }
    env->DeleteLocalRef(qtApplicationClazz);
}

void SonarPenUtilities::onSonarPenStatusChange(jint status) {
    SLOGD("onSonarPenStatusChange: %d", status);
    JNIEnv *env = getEnv();
    switch (status) {
        case CALIBRATE_SONAR_PEN:
        case SONAR_PEN_NOT_DETECTED_BY_PASSED:
            if (!isConnected) {
                isConnected = true;
                SLOGD("SonarPen is connected");
                showToast(mainThreadContext, "SonarPen is connected");
            }
            break;
        case SONAR_PEN_NOT_PLUGED:
        case WAITING_HEADSET:
            if (isConnected) {
                isConnected = false;
                SLOGD("SonarPen is disconnected");
                showToast(mainThreadContext, "SonarPen is disconnected");
            }
            break;
        default:
            break;
    }
}

void SonarPenUtilities::showToast(jobject context, const char *message) {
    if (sonarPenListenerInstance) {
        JNIEnv *env = getEnv();
        jclass sonarPenUtilitiesNativeClz = env->GetObjectClass(sonarPenListenerInstance);
        jmethodID showToastMethod = env->GetStaticMethodID(sonarPenUtilitiesNativeClz, "showToast",
                                                           "(Landroid/content/Context;Ljava/lang/String;)V");
        jstring jMessage = env->NewStringUTF(message);
        env->CallStaticVoidMethod(sonarPenUtilitiesNativeClz, showToastMethod, context, jMessage);
        env->DeleteLocalRef(jMessage);
        env->DeleteLocalRef(sonarPenUtilitiesNativeClz);
    } else {
        SLOGE("sonarPenListenerInstance is null");
    }
}

void SonarPenUtilities::addSonarPenToQtView() {
    SLOGD("addSonarPenToQtView");
    if (mainThreadContext != nullptr) {
        JNIEnv *env = getEnv();

        jclass qtApplicationClazz = env->FindClass("org/qtproject/qt5/android/bindings/QtApplication");
        if (!qtApplicationClazz) {
            SLOGE("qtApplicationClazz is null");
            checkAndClearException(env);
            return;
        }

        jfieldID m_delegateObjectField = env->GetStaticFieldID(qtApplicationClazz, "m_delegateObject", "Ljava/lang/Object;");
        jobject delegateObject = env->GetStaticObjectField(qtApplicationClazz, m_delegateObjectField);
        if (delegateObject != nullptr) {
            jclass delegateObjectClazz = env->GetObjectClass(delegateObject);
            jfieldID m_layoutField = env->GetFieldID(delegateObjectClazz, "m_layout", "Lorg/qtproject/qt5/android/QtLayout;");
            jobject layout = env->GetObjectField(delegateObject, m_layoutField);
            if (layout != nullptr) {
                addSonarPenToView(layout);
            } else {
                SLOGE("layout is null");
            }
            env->DeleteLocalRef(delegateObjectClazz);
            env->DeleteLocalRef(layout);
        } else {
            SLOGE("delegateObject is null");
        }
        env->DeleteLocalRef(qtApplicationClazz);
        env->DeleteLocalRef(delegateObject);
    } else {
        SLOGE("sonarPenUtilitiesIns isn't initialized yet!");
    }
}
