# SonarPen SDK C++ Usage


## Load sonarpen library
    
```
#include <sonarpen-sdk-cpp/sonarpen.h>
#include <sonarpen-sdk-cpp/sonarpen_utilities.h>

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *_vm, void *reserved) {
    // Load the SonarPen library
    SonarPen::JNI_OnLoad(_vm, reserved);
    /*
     * Do something
     */

    return JNI_VERSION_1_6;
}

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM* vm, void* reserved) {
    SonarPen::JNI_OnUnload(vm, reserved);
}

```


## Init library with activity jni instance
```
// after Activity::OnCreate, should init SonarPenUtilities with activity jni instance
SonarPenUtilities::getInstance()->init(context);
```

## Add sonarpen to view
```
SonarPenUtilities::getInstance()->addSonarPenToView(view);
```
Or in case use Qt Android, use below (Sonarpen library will seek a correct view and add it itself)
```
SonarPenUtilities::getInstance()->addSonarPenToQtView();
```

## Start Sonarpen when app focus (i.e Activity::onResume)
```
SonarPenUtilities::getInstance()->reloadOnResume();
SonarPenUtilities::getInstance()->start();
```

## Share key event to Sonarpen Library (i.e Activity::onKeyDown)
```
\\ Activity::onKeyDown
SonarPenUtilities::getInstance()->onKeyDown(key);
\\ And Activity::onKeyUp
SonarPenUtilities::getInstance()->onKeyUp(key);
```

## Stop Sonarpen when app focus (i.e Activity::onPause or Activity::onStop)
```
SonarPenUtilities::getInstance()->stop();
```

## Uninit library when activity is destroyed
```
SonarPenUtilities::getInstance()->uninit();
```




