#!/bin/bash -e

set -e
set -x


export ANDROID_ROOT=/home/phongdang/Workspace
export KDECI_ANDROID_SDK_ROOT=$ANDROID_ROOT/sdk
export KDECI_ANDROID_NDK_ROOT=$ANDROID_ROOT/android-ndk-r22b/
export ANDROID_HOME=$ANDROID_ROOT/sdk
export PATH="$ANDROID_ROOT/sdk/platform-tools/:$ANDROID_ROOT/cmdline-tools/bin/:$PATH"

export SRCDIR=$ANDROID_ROOT/greenblub/host_build/sources
export WORKDIR=$ANDROID_ROOT/greenblub/host_build/envdir

mkdir -p $SRCDIR
mkdir -p $WORKDIR

# Clone source
cd $SRCDIR
git clone https://invent.kde.org/greenblub/krita.git -b krita/5.2.2.x
git clone https://invent.kde.org/greenblub/krita-deps-management.git krita/krita-deps-management
git clone https://invent.kde.org/greenblub/ci-utilities.git krita/krita-deps-management/ci-utilities

# set up venv
python3 -m venv --upgrade-deps $WORKDIR/PythonEnv
source $WORKDIR/PythonEnv/bin/activate

# install requirements
python -m pip install -r $SRCDIR/krita/krita-deps-management/requirements.txt

export KDECI_ANDROID_ABI=arm64-v8a
export KRITACI_ANDROID_PACKAGE_TYPE=release

#Set up working directory and environment:
cd $SRCDIR/krita
python krita-deps-management/tools/setup-env.py -v $WORKDIR/PythonEnv --android-abi $KDECI_ANDROID_ABI --root $WORKDIR

## activate the generated environment
source $WORKDIR/base-env

## generate deps file
python krita-deps-management/tools/generate-deps-file.py -s krita-deps-management/latest/krita-deps.yml -o .kde-ci.yml

## fetch the dependencies
python krita-deps-management/ci-utilities/run-ci-build.py --project krita --branch master --platform Android/$KDECI_ANDROID_ABI --only-env

# activate generated environment
source ./env

cd $SRCDIR/krita/_build
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo  \
    -DHIDE_SAFE_ASSERTS=OFF \
    -DBUILD_TESTING=OFF \
    -DANDROID_SDK_ROOT=$KDECI_ANDROID_SDK_ROOT \
    -DCMAKE_INSTALL_PREFIX=$SRCDIR/krita/_install \
    -DCMAKE_TOOLCHAIN_FILE=$SRCDIR/krita/krita-deps-management/tools/android-toolchain-krita.cmake \
    $SRCDIR/krita/

#Build
make -j$(nproc) install
cd $SRCDIR/krita
python build-tools/ci-scripts/build-android-package.py
